﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChartDirector;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace RegressionModelsTest
{
    class ChartDraw
    {
        static public void SurfacePlot(double[] X1, double[] X2, double[] Y,WinChartViewer Chart, int x1index, int x2index)//, double[,] X, RegressionData regData, int X1index, int X2index)
        {
          

            
            SurfaceChart c = new SurfaceChart(550, 420);
            // Create a SurfaceChart object of size 720 x 600 pixels
            //SurfaceChart c = new SurfaceChart(720, 600);

            // Add a title to the chart using 20 points Times New Roman Italic font
            c.addTitle("Regression Surface ", "Times New Roman Italic", 10);
            //.addTitle("Surface Energy Density   ", "Times New Roman Italic", 20);

            // Set the center of the plot region at (350, 280), and set width x depth x height to
            // 360 x 360 x 270 pixels
            c.setPlotRegion(180, 200, 160, 160, 250);

            // Set the data to use to plot the chart
            c.setData(X1,X2,Y);

            // Spline interpolate data to a 80 x 80 grid for a smooth surface
            c.setInterpolation(1, 1);
           

            // Add a color axis (the legend) in which the left center is anchored at (645, 270). Set
            // the length to 200 pixels and the labels on the right side.
            c.setColorAxis(400, 50, ChartDirector.Chart.TopLeft, 350, ChartDirector.Chart.Right);
            // Set the x, y and z axis titles using 10 points Arial Bold font
            c.xAxis().setTitle("X" + x1index.ToString(), "Arial Bold", 10);
            c.yAxis().setTitle("X" + x2index.ToString(), "Arial Bold", 10);
            c.zAxis().setTitle("Regression Surface", "Arial Bold", 10);

            Chart.Chart = c;

        }

        static public void Livellines(double[] X1, double[] X2, double[] Y, WinChartViewer DrawChart, int x1index, int x2index)
        {

            // Create a XYChart object of size 600 x 500 pixels
            XYChart c = new XYChart(550, 420);

            // Add a title to the chart using 15 points Arial Bold Italic font
            c.addTitle("Level lines", "Arial Bold Italic", 10);

            // Set the plotarea at (75, 40) and of size 400 x 400 pixels. Use semi-transparent black
            // (80000000) dotted lines for both horizontal and vertical grid lines
            c.setPlotArea(75, 40, 320, 320, -1, -1, -1, c.dashLineColor(unchecked((int)0x80000000),
                ChartDirector.Chart.DotLine), -1);

            // Set x-axis and y-axis title using 12 points Arial Bold Italic font
            c.xAxis().setTitle("X"+x1index.ToString(), "Arial Bold Italic", 12);
            c.yAxis().setTitle("X"+x2index.ToString(), "Arial Bold Italic", 12);

            // Set x-axis and y-axis labels to use Arial Bold font
            c.xAxis().setLabelStyle("Arial Bold");
            c.yAxis().setLabelStyle("Arial Bold");

            // When auto-scaling, use tick spacing of 40 pixels as a guideline
            c.yAxis().setTickDensity(40);
            c.xAxis().setTickDensity(40);

            // Add a contour layer using the given data
            ContourLayer layer = c.addContourLayer(X1, X2, Y);

            // Move the grid lines in front of the contour layer
            c.getPlotArea().moveGridBefore(layer);

            // Add a color axis (the legend) in which the top left corner is anchored at (505, 40).
            // Set the length to 400 pixels and the labels on the right side.
            ColorAxis cAxis = layer.setColorAxis(450, 40, ChartDirector.Chart.TopLeft, 300, ChartDirector.Chart.Right);

            // Add a title to the color axis using 12 points Arial Bold Italic font
            cAxis.setTitle("Color Legend", "Arial Bold Italic", 12);

            // Set color axis labels to use Arial Bold font
            cAxis.setLabelStyle("Arial Bold");

            // Output the chart
            DrawChart.Chart = c;
        }
        static public void draw2Dchart(System.Windows.Forms.DataVisualization.Charting.Chart chart,double[] baseX,double[] RegressionX, double[] BaseY, double[] RegressionY)
        {
            chart.ChartAreas[0].AxisX.Minimum = 0.8 * baseX.Min();
            chart.ChartAreas[0].AxisX.Maximum = 1.2 * baseX.Max();
            chart.ChartAreas[0].AxisY.Minimum = 0.8 * BaseY.Min();
            chart.ChartAreas[0].AxisY.Maximum = 1.2 * BaseY.Max();
            chart.ChartAreas[0].AxisX.MajorGrid.Interval = (baseX.Max()-baseX.Min())/10 ;
            chart.Series[0].Points.DataBindXY(baseX, BaseY);
            chart.Series[1].Points.DataBindXY(RegressionX, RegressionY);
        }
    }
}
