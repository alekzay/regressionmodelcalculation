﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;
using ExcelDataReader;

namespace RegressionModelsTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            polinomial_range.SelectedIndex = 1;
            Linear_standart_graph.Checked = true;
            Polinomial_standart_graph.Checked = true;
            Exponential_standart_graph.Checked = true;
            PolinomialAuto_standart_graph.Checked = true;
            Giperbola_standart_graph.Checked = true;
            Degree_standart_graph.Checked = true;
            Square_standart_graph.Checked = true;
        }

        DataSet resultExcel;
        RegressionData linear;
        RegressionData exponential;
        RegressionData polinomialAuto;
        RegressionData polinomial;
        RegressionData degree;
        RegressionData giperbola;
        RegressionData square;

        private void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                Data formdata = new Data();
                formdata.setDataFromForm(dataGridView1);

                DateTime t1 = System.DateTime.Now;
                if (Linear_CheckBox.Checked == true)
                {

                    linear = new RegressionData(formdata.X, formdata.Y);
                    t1 = System.DateTime.Now;
                    linear = RegressionModel.linearRegression(formdata.X, formdata.Y);
                    Linear_time.Text = (System.DateTime.Now.Subtract(t1)).ToString();
                    Result_Linear.Text = Result.linearRegressionResult(linear.Koef);
                    Aproxymation.Analysys(linear, formdata);
                    Result_Linear_korrelation.Text = linear.korrelation.ToString();
                    Result_Linear_R.Text = linear.R.ToString();
                    Result_Linear_Radj.Text = linear.Radj.ToString();
                    linear_detail_korrelation.Text = linear.korrelation.ToString();
                    linear_detail_R.Text = linear.R.ToString();
                    linear_X1_collection.Items.Clear();
                    linear_X2_collection.Items.Clear();
                    for (int i = 0; i < linear.Koef.Length - 1; i++)
                    {
                        linear_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                        linear_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                    }
                }



                if (Degree_CheckBox.Checked == true)
                {
                    degree = new RegressionData(formdata.X, formdata.Y);
                    t1 = System.DateTime.Now;
                    degree = RegressionModel.degreeRegression(formdata.X, formdata.Y);
                    Degree_time.Text = (System.DateTime.Now - t1).ToString();
                    Result_Degree.Text = Result.degreeRegressionResult(degree.Koef);
                    Aproxymation.AnalysysNonLinear(degree, formdata);
                    Result_Degrree_korelation.Text = degree.korrelation.ToString();
                    Result_Degrree_R.Text = degree.R.ToString();
                    Result_Degrree_Radj.Text = degree.Radj.ToString();
                    degree_detail_korrelation.Text = degree.korrelation.ToString();
                    degree_detail_R.Text = degree.R.ToString();
                    degree_X1_collection.Items.Clear();
                    degree_X2_collection.Items.Clear();
                    for (int i = 0; i < degree.Koef.Length - 1; i++)
                    {
                        degree_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                        degree_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                    }
                }



                if (Giperbola_CheckBox.Checked == true)
                {
                    giperbola = new RegressionData(formdata.X, formdata.Y);
                    t1 = System.DateTime.Now;
                    giperbola = RegressionModel.giperbolaRegression(formdata.X, formdata.Y);
                    Giperbola_time.Text = (System.DateTime.Now - t1).ToString();
                    Result_giperbola.Text = Result.giperbolaRegressionResult(giperbola.Koef);
                    Aproxymation.AnalysysNonLinear(giperbola, formdata);
                    Result_Giperbola_korrelation.Text = giperbola.korrelation.ToString();
                    Result_Giperbola_R.Text = giperbola.R.ToString();
                    Result_Giperbola_Radj.Text = giperbola.Radj.ToString();
                    giperbola_detail_korrelation.Text = giperbola.korrelation.ToString();
                    giperbola_detail_R.Text = giperbola.R.ToString();
                    giperbola_X1_collection.Items.Clear();
                    giperbola_X2_collection.Items.Clear();
                    for (int i = 0; i < giperbola.Koef.Length - 1; i++)
                    {
                        giperbola_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                        giperbola_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                    }
                }



                if (Sqrt_CheckBox.Checked == true)
                {
                    square = new RegressionData(formdata.X, formdata.Y);
                    t1 = System.DateTime.Now;
                    square = RegressionModel.sqrtRegression(formdata.X, formdata.Y);
                    Square_time.Text = (System.DateTime.Now - t1).ToString();
                    Result_sqrt.Text = Result.squareRegressionResult(square.Koef);
                    Aproxymation.AnalysysNonLinear(square, formdata);
                    Result_Square_korrelation.Text = square.korrelation.ToString();
                    Result_Square_R.Text = square.R.ToString();
                    Result_Square_Radj.Text = square.Radj.ToString();
                    square_detail_korrelation.Text = square.korrelation.ToString();
                    square_detail_R.Text = square.R.ToString();
                    square_X1_collection.Items.Clear();
                    square_X2_collection.Items.Clear();
                    for (int i = 0; i < square.Koef.Length - 1; i++)
                    {
                        square_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                        square_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                    }
                }



                if (Exponential_CheckBox.Checked == true)
                {
                    exponential = new RegressionData(formdata.X, formdata.Y);
                    t1 = System.DateTime.Now;
                    exponential = RegressionModel.exponentialRegression(formdata.X, formdata.Y);
                    Exponential_time.Text = (System.DateTime.Now - t1).ToString();
                    Result_exponential.Text = Result.exponentialRegressionResult(exponential.Koef);
                    Aproxymation.AnalysysNonLinear(exponential, formdata);
                    Result_Exponential_korrelation.Text = exponential.korrelation.ToString();
                    Result_Exponential_R.Text = exponential.R.ToString();
                    Result_Exponential_Radj.Text = exponential.Radj.ToString();
                    exponential_detail_korrelation.Text = exponential.korrelation.ToString();
                    exponential_detail_R.Text = exponential.R.ToString();
                    exponential_X1_collection.Items.Clear();
                    exponential_X2_collection.Items.Clear();
                    for (int i = 0; i < exponential.Koef.Length - 1; i++)
                    {
                        exponential_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                        exponential_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                    }
                }



                if (Polinomial_CheckBox.Checked == true)
                {
                    polinomial = new RegressionData(formdata.X, formdata.Y);
                    t1 = System.DateTime.Now;
                    polinomial = RegressionModel.polimonialRegression(formdata.X, formdata.Y, polinomial_range.SelectedIndex + 2);
                    Polinomial_time.Text = (System.DateTime.Now - t1).ToString();
                    Result_polinomial.Text = Result.polinomialRegressionResult(polinomial.Koef, polinomial_range.SelectedIndex + 2);
                    Aproxymation.AnalysysNonLinear(polinomial, formdata);
                    Result_Polinomial_korrelation.Text = polinomial.korrelation.ToString();
                    Result_Polinomial_R.Text = polinomial.R.ToString();
                    Result_Polinomial_Radj.Text = polinomial.Radj.ToString();
                    polinomial_detail_korrelation.Text = polinomial.korrelation.ToString();
                    polinomial_detail_R.Text = polinomial.R.ToString();
                    polinomial_X1_collection.Items.Clear();
                    polinomial_X2_collection.Items.Clear();
                    for (int i = 0; i < dataGridView1.ColumnCount - 1; i++)
                    {
                        polinomial_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                        polinomial_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                    }
                }

            }
            catch (DivideByZeroException)
            {
                MessageBox.Show("Please enter data");
            }
            catch (OverflowException)
            {
                MessageBox.Show("Select Table");
            }

        }



        private void addColumn_Click(object sender, EventArgs e)
        {
            if (dataGridView1.ColumnCount+1 > 15)
            {
                MessageBox.Show("NO");
            }
            else
            {
                dataGridView1.ColumnCount++;
                dataGridView1.Columns[dataGridView1.ColumnCount - 1].Name = "X" + (dataGridView1.ColumnCount - 1);
            }
        }

        

        private void removeColumn_Click(object sender, EventArgs e)
        {
            int i;
            if (dataGridView1.Columns.Count == 0)
            {
                MessageBox.Show("Нечего удалять");
            }
            else
            {
                if ((int.TryParse(removeIndex.Text, out i) == true)&&(Convert.ToInt32(removeIndex.Text)<=dataGridView1.Columns.Count))
                {
                    if (i > 0)
                    {
                        dataGridView1.Columns.Remove(dataGridView1.Columns[i - 1].Name);
                    }
                    else
                    {
                        dataGridView1.Columns.Remove(dataGridView1.Columns[i].Name);
                    }
                }
                else
                {
                    MessageBox.Show("Неправельно введён индекс столбца");
                }
            }
        }

         void setDataFromFile(DataGridView data)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = " txt|*.txt| Excel Workbook |*.xls; *.xlsx| CSV| *.csv";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //FileSourseText.Text = ofd.FileName;
                switch (ofd.FilterIndex)
                {
                    case 1: StreamReader fileRead = new StreamReader(ofd.FileName);
                            string file = fileRead.ReadToEnd();
                            string[] lines = file.Split('\n');
                            string[] symbols;
                            string[] test = lines[0].Split(' ');
                            double[,] result = new double[lines.Length, test.Length];
                            for (int i = 0; i < lines.Length; i++)
                            {
                                symbols = lines[i].Split(' ');
                                for (int k = 0; k < symbols.Length; k++)
                                {
                                    result[i, k] = double.Parse(symbols[k]);
                                }
                            }
                            data.ColumnCount = test.Length;
                            data.RowCount = lines.Length + 1;
                            for (int i = 0; i < data.RowCount - 1; i++)
                            {
                                for (int k = 0; k < data.ColumnCount; k++)
                                {
                                    data[k, i].Value = result[i, k];
                                }
                            }
                            fileRead.Close();
                        break;
                    case 2:
                        data.ColumnCount = 0;
                        comboExcelSheets.Visible = true;
                        addColumn.Enabled = false;
                        FileStream fs = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read);
                        if (Path.GetExtension(ofd.FileName) == ".xls")
                        {
                            IExcelDataReader reader = ExcelReaderFactory.CreateBinaryReader(fs);
                            resultExcel = reader.AsDataSet();
                            comboExcelSheets.Items.Clear();
                            foreach (DataTable dt in resultExcel.Tables)
                                comboExcelSheets.Items.Add(dt.TableName);
                            reader.Close();
                            break;
                        }
                        else
                        {
                            IExcelDataReader reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
                            resultExcel = reader.AsDataSet();
                            comboExcelSheets.Items.Clear();
                            foreach (DataTable dt in resultExcel.Tables)
                                comboExcelSheets.Items.Add(dt.TableName);
                            reader.Close();
                            break;
                        }

                    case 3:
                        comboExcelSheets.Visible = true;
                        addColumn.Enabled = false;
                        FileStream fscsv = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read);
                        IExcelDataReader readercsv = ExcelReaderFactory.CreateCsvReader(fscsv);
                        resultExcel = readercsv.AsDataSet();/*(new ExcelDataSetConfiguration() {
	                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration() {
		                        UseHeaderRow = true
	                    }
                        });*/
                        comboExcelSheets.Items.Clear();
                        foreach (DataTable dt in resultExcel.Tables)
                            comboExcelSheets.Items.Add(dt.TableName);
                        readercsv.Close();
                        break;
            }
            }
        }

        private void Linear_Details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Linear_Page);
        }

        private void Degree_Details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Degree_Page);
        }

        private void Giperbola_Details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Giperbola_Page);
        }


        private void Open_file_button_Click(object sender, EventArgs e)
        {
            //dataGridView1.ColumnCount = 0;

            dataGridView1.DataSource = null;
            comboExcelSheets.Items.Clear();
            
                while (dataGridView1.Columns.Count != 0)
                {
                    dataGridView1.Columns.RemoveAt(0);
                }

            setDataFromFile(dataGridView1);
            if (dataGridView1.Columns.Count > 0)
            {
                dataGridView1.Columns[0].Name = "Y";
                for (int i = 1; i < dataGridView1.Columns.Count; i++)
                {
                    dataGridView1.Columns[i].Name = "X" + i.ToString();
                }
            }
        }

        private void comboExcelSheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.DataSource = resultExcel.Tables[comboExcelSheets.SelectedIndex];
        }

        private void Auto_Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                Data formdata = new Data();
                formdata.setDataFromForm(dataGridView1);




                linear = new RegressionData(formdata.X, formdata.Y);
                DateTime tLinear = System.DateTime.Now;
                linear = RegressionModel.linearRegression(formdata.X, formdata.Y);
                TimeSpan linearTime = System.DateTime.Now - tLinear;
                Linear_Auto_time.Text = linearTime.ToString();
                Auto_linear_result.Text = Result.linearRegressionResult(linear.Koef);
                Aproxymation.Analysys(linear, formdata);
                Auto_linear_korrelation.Text = linear.korrelation.ToString();
                R_linear.Text = linear.R.ToString();
                Auto_linear_Radj.Text = linear.Radj.ToString();
                linear_detail_korrelation.Text = linear.korrelation.ToString();
                linear_detail_R.Text = linear.R.ToString();
                linear_X1_collection.Items.Clear();
                linear_X2_collection.Items.Clear();
                for (int i = 0; i < linear.Koef.Length - 1; i++)
                {
                    linear_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                    linear_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                }



                exponential = new RegressionData(formdata.X, formdata.Y);
                DateTime tExponential = System.DateTime.Now;
                exponential = RegressionModel.exponentialRegression(formdata.X, formdata.Y);
                TimeSpan exponentialTime = System.DateTime.Now - tExponential;
                Exponential_Auto_time.Text = exponentialTime.ToString();
                Auto_exponential_result.Text = Result.exponentialRegressionResult(exponential.Koef);
                Aproxymation.AnalysysNonLinear(exponential, formdata);
                Auto_exponential_korrelation.Text = exponential.korrelation.ToString();
                R_exponential.Text = exponential.R.ToString();
                Auto_exponential_Radj.Text = exponential.Radj.ToString();
                exponential_detail_korrelation.Text = exponential.korrelation.ToString();
                exponential_detail_R.Text = exponential.R.ToString();
                exponential_X1_collection.Items.Clear();
                exponential_X2_collection.Items.Clear();
                for (int i = 0; i < exponential.Koef.Length - 1; i++)
                {
                    exponential_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                    exponential_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                }



                polinomialAuto = new RegressionData(formdata.X, formdata.Y);
                DateTime tPolinomialAuto = System.DateTime.Now;
                polinomialAuto = RegressionModel.polinomialAutoRegression(formdata.X, formdata.Y);
                TimeSpan polinomialAutoTime = System.DateTime.Now - tPolinomialAuto;
                Polinomial_Auto_time.Text = polinomialAutoTime.ToString();
                Auto_polinomial_result.Text = Result.polinomialRegressionResult(polinomialAuto.Koef, 3);
                Aproxymation.AnalysysNonLinear(polinomialAuto, formdata);
                Auto_polinomial_korrelation.Text = polinomialAuto.korrelation.ToString();
                R_polinomial.Text = polinomialAuto.R.ToString();
                Auto_polinomial_Radj.Text = polinomialAuto.Radj.ToString();
                polinomialAuto_detail_korrelation.Text = polinomialAuto.korrelation.ToString();
                polinomialAuto_detail_R.Text = polinomialAuto.R.ToString();
                polinomialAuto_X1_collection.Items.Clear();
                polinomialAuto_X2_collection.Items.Clear();
                for (int i = 0; i < dataGridView1.ColumnCount - 1; i++)
                {
                    polinomialAuto_X1_collection.Items.Add("X" + Convert.ToString(i + 1));
                    polinomialAuto_X2_collection.Items.Add("X" + Convert.ToString(i + 1));
                }


                Total_Auto_time.Text = (linearTime + exponentialTime + polinomialAutoTime).ToString();

                mainTab.SelectTab(Auto_Result);
            }
            catch (DivideByZeroException)
            {
                MessageBox.Show("Please enter data");
            }
            catch (OverflowException)
            {
                MessageBox.Show("Select Table");
            }

        }

        private void setYButton_Click(object sender, EventArgs e)
        {
            int i;
            object k;
            if (dataGridView1.Columns.Count == 0)
            {
                MessageBox.Show("No data available");
            }
            else
            {
                if ((int.TryParse(setYTextBox.Text, out i) == true) && (Convert.ToInt32(setYTextBox.Text)<dataGridView1.Columns.Count))
                {
                    if (i > 0)
                    {
                        for (int j = 0; j < dataGridView1.RowCount - 1; j++)
                        {
                            k = dataGridView1[0, j].Value;
                            dataGridView1[0, j].Value = dataGridView1[i, j].Value;
                            dataGridView1[i, j].Value = k;
                        }
                    }
                    else
                    {
                        MessageBox.Show("This column already Y");
                    }
                }
                else
                {
                    MessageBox.Show("Incorrect data");
                }
            }

        }

        private void Linear_chart_Draw_Click(object sender, EventArgs e)
        {

            try
            {
                if (linear_X1_collection.SelectedIndex >= 0)
                {
                    Linear_Chart_viewer.Visible = false;
                    linearChart.Visible = true;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData LinearDraw = new ChartData();
                    if (Linear_standart_graph.Checked == true)
                    {
                        LinearDraw.Linear2DChart(linear, formdata, linear_X1_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.draw2Dchart(linearChart, LinearDraw.X1, LinearDraw.X2, formdata.Y, LinearDraw.Y);
                        Linear_Y_max.Text = LinearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = LinearDraw.Y.Min().ToString();
                    }
                    if (Linear_min_graph.Checked == true)
                    {
                        LinearDraw.Linear2DChartMin(linear, formdata, linear_X1_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.draw2Dchart(linearChart, LinearDraw.X1, LinearDraw.X2, formdata.Y, LinearDraw.Y);
                        Linear_Y_max.Text = LinearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = LinearDraw.Y.Min().ToString();
                    }
                    if (Linear_avg_graph.Checked == true)
                    {
                        LinearDraw.Linear2DChartAverage(linear, formdata, linear_X1_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.draw2Dchart(linearChart, LinearDraw.X1, LinearDraw.X2, formdata.Y, LinearDraw.Y);
                        Linear_Y_max.Text = LinearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = LinearDraw.Y.Min().ToString();
                    }
                    if (Linear_max_graph.Checked == true)
                    {
                        LinearDraw.Linear2DChartMax(linear, formdata, linear_X1_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.draw2Dchart(linearChart, LinearDraw.X1, LinearDraw.X2, formdata.Y, LinearDraw.Y);
                        Linear_Y_max.Text = LinearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = LinearDraw.Y.Min().ToString();
                    }

                }
                else 
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                linearChart.Visible = false;
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                linearChart.Visible = false;
            }

        }

        private void Exponential_chart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (exponential_X1_collection.SelectedIndex >= 0)
                {
                    Exponential_Chart_viewer.Visible = false;
                    exponentialChart.Visible = true;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData ExponentialDraw = new ChartData();
                    if (Exponential_standart_graph.Checked)
                    {
                        ExponentialDraw.Exponential2DChart(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.draw2Dchart(exponentialChart, ExponentialDraw.X1, ExponentialDraw.X2, formdata.Y, ExponentialDraw.Y);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_min_graph.Checked)
                    {
                        ExponentialDraw.Exponential2DChartMin(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.draw2Dchart(exponentialChart, ExponentialDraw.X1, ExponentialDraw.X2, formdata.Y, ExponentialDraw.Y);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_avg_graph.Checked)
                    {
                        ExponentialDraw.Exponential2DChartAverage(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.draw2Dchart(exponentialChart, ExponentialDraw.X1, ExponentialDraw.X2, formdata.Y, ExponentialDraw.Y);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_max_graph.Checked)
                    {
                        ExponentialDraw.Exponential2DChartMax(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.draw2Dchart(exponentialChart, ExponentialDraw.X1, ExponentialDraw.X2, formdata.Y, ExponentialDraw.Y);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                }
                else 
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                exponentialChart.Visible = false;
            }

        }

        private void Linear_3Dchart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (linear_X1_collection.SelectedIndex >= 0 && linear_X2_collection.SelectedIndex >= 0)
                {
                    Linear_Chart_viewer.Visible = true;
                    linearChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData linearDraw = new ChartData();
                    if (Linear_min_graph.Checked == true)
                    {
                        linearDraw.LinearChartMin(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.SurfacePlot(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                    if (Linear_avg_graph.Checked == true)
                    {
                        linearDraw.LinearChartAverage(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.SurfacePlot(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                    if (Linear_max_graph.Checked == true)
                    {
                        linearDraw.LinearChartMax(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.SurfacePlot(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                    if (Linear_standart_graph.Checked == true)
                    {
                        linearDraw.LinearChart(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.SurfacePlot(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Linear_Chart_viewer.Visible = false;
            }

        }

        private void Exponential_3Dchart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (exponential_X1_collection.SelectedIndex >= 0 && exponential_X2_collection.SelectedIndex >= 0)
                {
                    Exponential_Chart_viewer.Visible = true;
                    exponentialChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData ExponentialDraw = new ChartData();
                    if (Exponential_standart_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChart(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.SurfacePlot(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_min_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChartMin(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.SurfacePlot(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_avg_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChartAverage(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.SurfacePlot(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_max_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChartMax(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.SurfacePlot(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                }
                else 
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Exponential_Chart_viewer.Visible = false;
            }
        }

        private void linear_auto_details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Linear_Page);
        }

        private void exponential_auto_details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Exponential_Page);
        }

        private void polinomial_auto_details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(PolinomialAuto_Page);
        }

        private void linear_Levellnes_draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (linear_X1_collection.SelectedIndex >= 0 && linear_X2_collection.SelectedIndex >= 0)
                {
                    Linear_Chart_viewer.Visible = true;
                    linearChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData linearDraw = new ChartData();
                    if (Linear_standart_graph.Checked)
                    {
                        linearDraw.LinearChart(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.Livellines(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                    if (Linear_min_graph.Checked)
                    {
                        linearDraw.LinearChartMin(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.Livellines(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                    if (Linear_avg_graph.Checked)
                    {
                        linearDraw.LinearChartAverage(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.Livellines(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                    if (Linear_max_graph.Checked)
                    {
                        linearDraw.LinearChartMax(linear, formdata, linear_X1_collection.SelectedIndex, linear_X2_collection.SelectedIndex, linear_graph_function);
                        ChartDraw.Livellines(linearDraw.X1, linearDraw.X2, linearDraw.Y, Linear_Chart_viewer, linear_X1_collection.SelectedIndex + 1, linear_X2_collection.SelectedIndex + 1);
                        Linear_Y_max.Text = linearDraw.Y.Max().ToString();
                        Linear_Y_min.Text = linearDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Linear_Chart_viewer.Visible = false;
            }
        }

        private void Exponential_levellines_draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (exponential_X1_collection.SelectedIndex >= 0 && exponential_X2_collection.SelectedIndex >= 0)
                {
                    Exponential_Chart_viewer.Visible = true;
                    exponentialChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData ExponentialDraw = new ChartData();
                    if (Exponential_standart_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChart(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.Livellines(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_min_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChartMin(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.Livellines(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_avg_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChartAverage(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.Livellines(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                    if (Exponential_max_graph.Checked)
                    {
                        ExponentialDraw.ExponentialChartMax(exponential, formdata, exponential_X1_collection.SelectedIndex, exponential_X2_collection.SelectedIndex, exponential_grahp_function);
                        ChartDraw.Livellines(ExponentialDraw.X1, ExponentialDraw.X2, ExponentialDraw.Y, Exponential_Chart_viewer, exponential_X1_collection.SelectedIndex + 1, exponential_X2_collection.SelectedIndex + 1);
                        Exponential_Y_max.Text = ExponentialDraw.Y.Max().ToString();
                        Exponential_Y_min.Text = ExponentialDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Exponential_Chart_viewer.Visible = false;
            }
        }

        private void Polinomial_3Dchart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (polinomialAuto_X1_collection.SelectedIndex >= 0 && polinomialAuto_X2_collection.SelectedIndex >= 0)
                {
                    PolinomialAuto_Chart_viewer.Visible = true;
                    polinomialAutoChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData polinomialDraw = new ChartData();
                    if (PolinomialAuto_standart_graph.Checked)
                    {
                        polinomialDraw.PolinomialChart(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_min_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMin(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_avg_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartAverage(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_max_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMax(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Selcet X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                PolinomialAuto_Chart_viewer.Visible = false;
            }
        }

        private void Polinomial_levellines_draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (polinomialAuto_X1_collection.SelectedIndex >= 0 && polinomialAuto_X2_collection.SelectedIndex >= 0)
                {
                    PolinomialAuto_Chart_viewer.Visible = true;
                    polinomialAutoChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData polinomialDraw = new ChartData();
                    if (PolinomialAuto_standart_graph.Checked)
                    {
                        polinomialDraw.PolinomialChart(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_min_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMin(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_avg_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartAverage(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_max_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMax(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, polinomialAuto_X2_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, PolinomialAuto_Chart_viewer, polinomialAuto_X1_collection.SelectedIndex + 1, polinomialAuto_X2_collection.SelectedIndex + 1);
                        PolinomialAuto_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }

                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                PolinomialAuto_Chart_viewer.Visible = false;
            }
        }

        private void Degree_3Dchart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (degree_X1_collection.SelectedIndex >= 0 && degree_X2_collection.SelectedIndex >= 0)
                {
                    Degree_Chart_viewer.Visible = true;
                    degreeChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData DegreeDraw = new ChartData();
                    if (Degree_standart_graph.Checked)
                    {
                        DegreeDraw.DegreeChart(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.SurfacePlot(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_min_graph.Checked)
                    {
                        DegreeDraw.DegreeChartMin(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.SurfacePlot(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_avg_graph.Checked)
                    {
                        DegreeDraw.DegreeChartAverage(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.SurfacePlot(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_max_graph.Checked)
                    {
                        DegreeDraw.DegreeChartMax(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.SurfacePlot(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Degree_Chart_viewer.Visible = false;
            }
        }

        private void Giperbola_3Dchart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (giperbola_X1_collection.SelectedIndex >= 0 && giperbola_X2_collection.SelectedIndex >= 0)
                {
                    Giperbola_Chart_viewer.Visible = true;
                    giperbolaChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData GiperbolaDraw = new ChartData();
                    if (Giperbola_standart_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChart(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.SurfacePlot(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_min_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChartMin(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.SurfacePlot(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_avg_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChartAverage(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.SurfacePlot(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_max_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChartMax(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.SurfacePlot(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Giperbola_Chart_viewer.Visible = false;
            }
        }

        private void Square_3Dchart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (square_X1_collection.SelectedIndex >= 0 && square_X2_collection.SelectedIndex >= 0)
                {
                    Square_Chart_viewer.Visible = true;
                    squareChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData SquareDraw = new ChartData();
                    if (Square_standart_graph.Checked)
                    {
                        SquareDraw.SquareChart(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.SurfacePlot(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_min_graph.Checked)
                    {
                        SquareDraw.SquareChartMin(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.SurfacePlot(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_avg_graph.Checked)
                    {
                        SquareDraw.SquareChartAverage(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.SurfacePlot(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_max_graph.Checked)
                    {
                        SquareDraw.SquareChartMax(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.SurfacePlot(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Square_Chart_viewer.Visible = false;
            }
        }

        private void Degree_levellines_draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (degree_X1_collection.SelectedIndex >= 0 && degree_X2_collection.SelectedIndex >= 0)
                {
                    Degree_Chart_viewer.Visible = true;
                    degreeChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData DegreeDraw = new ChartData();
                    if (Degree_standart_graph.Checked)
                    {
                        DegreeDraw.DegreeChart(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.Livellines(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_min_graph.Checked)
                    {
                        DegreeDraw.DegreeChartMin(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.Livellines(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_avg_graph.Checked)
                    {
                        DegreeDraw.DegreeChartAverage(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.Livellines(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_max_graph.Checked)
                    {
                        DegreeDraw.DegreeChartMax(degree, formdata, degree_X1_collection.SelectedIndex, degree_X2_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.Livellines(DegreeDraw.X1, DegreeDraw.X2, DegreeDraw.Y, Degree_Chart_viewer, degree_X1_collection.SelectedIndex + 1, degree_X2_collection.SelectedIndex + 1);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }

                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Degree_Chart_viewer.Visible = false;
            }
        }

        private void Giperbola_levellines_draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (giperbola_X1_collection.SelectedIndex >= 0 && giperbola_X2_collection.SelectedIndex >= 0)
                {
                    Giperbola_Chart_viewer.Visible = true;
                    giperbolaChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData GiperbolaDraw = new ChartData();
                    if (Giperbola_standart_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChart(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.Livellines(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_min_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChartMin(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.Livellines(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_avg_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChartAverage(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.Livellines(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_max_graph.Checked)
                    {
                        GiperbolaDraw.GiperbolaChartMax(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_X2_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.Livellines(GiperbolaDraw.X1, GiperbolaDraw.X2, GiperbolaDraw.Y, Giperbola_Chart_viewer, giperbola_X1_collection.SelectedIndex + 1, giperbola_X2_collection.SelectedIndex + 1);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Giperbola_Chart_viewer.Visible = false;
            }
        }

        private void Square_levellines_draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (square_X1_collection.SelectedIndex >= 0 && square_X2_collection.SelectedIndex >= 0)
                {
                    Square_Chart_viewer.Visible = true;
                    squareChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData SquareDraw = new ChartData();
                    if (Square_standart_graph.Checked)
                    {
                        SquareDraw.SquareChart(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.Livellines(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_min_graph.Checked)
                    {
                        SquareDraw.SquareChartMin(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.Livellines(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_avg_graph.Checked)
                    {
                        SquareDraw.SquareChartAverage(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.Livellines(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_max_graph.Checked)
                    {
                        SquareDraw.SquareChartMax(square, formdata, square_X1_collection.SelectedIndex, square_X2_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.Livellines(SquareDraw.X1, SquareDraw.X2, SquareDraw.Y, Square_Chart_viewer, square_X1_collection.SelectedIndex + 1, square_X2_collection.SelectedIndex + 1);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Square_Chart_viewer.Visible = false;
            }
        }

        private void Polinomial_chart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (polinomialAuto_X1_collection.SelectedIndex >= 0)
                {
                    PolinomialAuto_Chart_viewer.Visible = false;
                    polinomialAutoChart.Visible = true;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData PolinomialDraw = new ChartData();
                    if (PolinomialAuto_standart_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChart(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.draw2Dchart(polinomialAutoChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        PolinomialAuto_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_min_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChartMin(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.draw2Dchart(polinomialAutoChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        PolinomialAuto_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_avg_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChartAverage(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.draw2Dchart(polinomialAutoChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        PolinomialAuto_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                    if (PolinomialAuto_max_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChartMax(polinomialAuto, formdata, polinomialAuto_X1_collection.SelectedIndex, 3, polinomialAuto_grahp_function);
                        ChartDraw.draw2Dchart(polinomialAutoChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        PolinomialAuto_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        PolinomialAuto_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                polinomialAutoChart.Visible = false;
            }
        }

        private void Degree_chart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (degree_X1_collection.SelectedIndex >= 0)
                {
                    Degree_Chart_viewer.Visible = false;
                    degreeChart.Visible = true;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData DegreeDraw = new ChartData();
                    if (Degree_standart_graph.Checked)
                    {
                        DegreeDraw.Degree2DChart(degree, formdata, degree_X1_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.draw2Dchart(degreeChart, DegreeDraw.X1, DegreeDraw.X2, formdata.Y, DegreeDraw.Y);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_min_graph.Checked)
                    {
                        DegreeDraw.Degree2DChartMin(degree, formdata, degree_X1_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.draw2Dchart(degreeChart, DegreeDraw.X1, DegreeDraw.X2, formdata.Y, DegreeDraw.Y);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_avg_graph.Checked)
                    {
                        DegreeDraw.Degree2DChartAverage(degree, formdata, degree_X1_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.draw2Dchart(degreeChart, DegreeDraw.X1, DegreeDraw.X2, formdata.Y, DegreeDraw.Y);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                    if (Degree_max_graph.Checked)
                    {
                        DegreeDraw.Degree2DChartMax(degree, formdata, degree_X1_collection.SelectedIndex, degree_grahp_function);
                        ChartDraw.draw2Dchart(degreeChart, DegreeDraw.X1, DegreeDraw.X2, formdata.Y, DegreeDraw.Y);
                        Degree_Y_max.Text = DegreeDraw.Y.Max().ToString();
                        Degree_Y_min.Text = DegreeDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                degreeChart.Visible = false;
            }
        }

        private void Giperbola_chart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (giperbola_X1_collection.SelectedIndex >= 0)
                {
                    Giperbola_Chart_viewer.Visible = false;
                    giperbolaChart.Visible = true;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData GiperbolaDraw = new ChartData();
                    if (Giperbola_standart_graph.Checked)
                    {
                        GiperbolaDraw.Giperbola2DChart(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.draw2Dchart(giperbolaChart, GiperbolaDraw.X1, GiperbolaDraw.X2, formdata.Y, GiperbolaDraw.Y);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_min_graph.Checked)
                    {
                        GiperbolaDraw.Giperbola2DChartMin(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.draw2Dchart(giperbolaChart, GiperbolaDraw.X1, GiperbolaDraw.X2, formdata.Y, GiperbolaDraw.Y);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_avg_graph.Checked)
                    {
                        GiperbolaDraw.Giperbola2DChartAverage(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.draw2Dchart(giperbolaChart, GiperbolaDraw.X1, GiperbolaDraw.X2, formdata.Y, GiperbolaDraw.Y);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                    if (Giperbola_max_graph.Checked)
                    {
                        GiperbolaDraw.Giperbola2DChartMax(giperbola, formdata, giperbola_X1_collection.SelectedIndex, giperbola_grahp_function);
                        ChartDraw.draw2Dchart(giperbolaChart, GiperbolaDraw.X1, GiperbolaDraw.X2, formdata.Y, GiperbolaDraw.Y);
                        Giperbola_Y_max.Text = GiperbolaDraw.Y.Max().ToString();
                        Giperbola_Y_min.Text = GiperbolaDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                giperbolaChart.Visible = false;
            }
        }

        private void Square_chart_Draw_Click(object sender, EventArgs e)
        {
            try
            {
                if (square_X1_collection.SelectedIndex >= 0)
                {
                    Square_Chart_viewer.Visible = false;
                    squareChart.Visible = true;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData SquareDraw = new ChartData();
                    if (Square_standart_graph.Checked)
                    {
                        SquareDraw.Square2DChart(square, formdata, square_X1_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.draw2Dchart(squareChart, SquareDraw.X1, SquareDraw.X2, formdata.Y, SquareDraw.Y);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_min_graph.Checked)
                    {
                        SquareDraw.Square2DChartMin(square, formdata, square_X1_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.draw2Dchart(squareChart, SquareDraw.X1, SquareDraw.X2, formdata.Y, SquareDraw.Y);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_avg_graph.Checked)
                    {
                        SquareDraw.Square2DChartAverage(square, formdata, square_X1_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.draw2Dchart(squareChart, SquareDraw.X1, SquareDraw.X2, formdata.Y, SquareDraw.Y);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                    if (Square_max_graph.Checked)
                    {
                        SquareDraw.Square2DChartMax(square, formdata, square_X1_collection.SelectedIndex, square_grahp_function);
                        ChartDraw.draw2Dchart(squareChart, SquareDraw.X1, SquareDraw.X2, formdata.Y, SquareDraw.Y);
                        Square_Y_max.Text = SquareDraw.Y.Max().ToString();
                        Square_Y_min.Text = SquareDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                squareChart.Visible = false;
            }
        }


        private void Linear_save_Click(object sender, EventArgs e)
        {
            Result.saveresult(Result.linearRegressionResult(linear.Koef));
        }

        private void Exponential_save_Click(object sender, EventArgs e)
        {
            Result.saveresult(Result.exponentialRegressionResult(exponential.Koef));
        }

        private void Polinomial_save_Click(object sender, EventArgs e)
        {
            Result.saveresult(Result.polinomialRegressionResult(polinomialAuto.Koef, 3));
        }

        private void Polinomial_Details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Polinomial_Page);
        }

        private void Exponential_Details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Exponential_Page);
        }

        private void Sqrt_Details_Click(object sender, EventArgs e)
        {
            mainTab.SelectTab(Square_Page);
        }

        private void Polinomial_chart_Draw_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (polinomial_X1_collection.SelectedIndex >= 0)
                {
                    Polinomial_Chart_viewer.Visible = false;
                    polinomialChart.Visible = true;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData PolinomialDraw = new ChartData();
                    if (Polinomial_standart_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChart(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.draw2Dchart(polinomialChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        Polinomial_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_min_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChartMin(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.draw2Dchart(polinomialChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        Polinomial_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_avg_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChartAverage(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.draw2Dchart(polinomialChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        Polinomial_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_max_graph.Checked)
                    {
                        PolinomialDraw.Polinomial2DChartMax(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.draw2Dchart(polinomialChart, PolinomialDraw.X1, PolinomialDraw.X2, formdata.Y, PolinomialDraw.Y);
                        Polinomial_Y_max.Text = PolinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = PolinomialDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                polinomialChart.Visible = false;
            }
            catch(IndexOutOfRangeException)
            {
                MessageBox.Show("X not selected");
                polinomialChart.Visible = false;
            }

        }

        private void Polinomial_3Dchart_Draw_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (polinomial_X1_collection.SelectedIndex >= 0 && polinomial_X2_collection.SelectedIndex >= 0)
                {
                    Polinomial_Chart_viewer.Visible = true;
                    polinomialChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData polinomialDraw = new ChartData();
                    if (Polinomial_standart_graph.Checked)
                    {
                        polinomialDraw.PolinomialChart(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_min_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMin(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_avg_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartAverage(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_max_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMax(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.SurfacePlot(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Polinomial_Chart_viewer.Visible = false;
            }

        }

        private void Polinomial_levellines_draw_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (polinomial_X1_collection.SelectedIndex >= 0 && polinomial_X2_collection.SelectedIndex >= 0)
                {
                    Polinomial_Chart_viewer.Visible = true;
                    polinomialAutoChart.Visible = false;
                    Data formdata = new Data();
                    formdata.setDataFromForm(dataGridView1);
                    ChartData polinomialDraw = new ChartData();
                    if (Polinomial_standart_graph.Checked)
                    {
                        polinomialDraw.PolinomialChart(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_min_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMin(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_avg_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartAverage(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                    if (Polinomial_max_graph.Checked)
                    {
                        polinomialDraw.PolinomialChartMax(polinomial, formdata, polinomial_X1_collection.SelectedIndex, polinomial_X2_collection.SelectedIndex, polinomial_range.SelectedIndex + 2, polinomial_grahp_function);
                        ChartDraw.Livellines(polinomialDraw.X1, polinomialDraw.X2, polinomialDraw.Y, Polinomial_Chart_viewer, polinomial_X1_collection.SelectedIndex + 1, polinomial_X2_collection.SelectedIndex + 1);
                        Polinomial_Y_max.Text = polinomialDraw.Y.Max().ToString();
                        Polinomial_Y_min.Text = polinomialDraw.Y.Min().ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Select X");
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Model not calculated or X not selected");
                Polinomial_Chart_viewer.Visible = false;
            }
        }

        private void Degree_save_Click(object sender, EventArgs e)
        {
            Result.saveresult(Result.degreeRegressionResult(degree.Koef));
        }

        private void Giperbola_save_Click(object sender, EventArgs e)
        {
            Result.saveresult(Result.giperbolaRegressionResult(giperbola.Koef));
        }

        private void Square_save_Click(object sender, EventArgs e)
        {
            Result.saveresult(Result.squareRegressionResult(square.Koef));
        }

        private void Polinomial_save_Click_1(object sender, EventArgs e)
        {
            Result.saveresult(Result.polinomialRegressionResult(polinomial.Koef, polinomial_range.SelectedIndex + 2));
        }

        private void removeRow_Click(object sender, EventArgs e)
        {
            int i;
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("Нечего удалять");
            }
            else
            {
                if ((int.TryParse(removeRowIndex.Text, out i) == true) && (Convert.ToInt32(removeRowIndex.Text) < dataGridView1.Rows.Count))
                {
                        dataGridView1.Rows.RemoveAt(i-1);
                }
                else
                {
                    MessageBox.Show("Неправельно введён индекс строки");
                }
            }
        }

        private void helpmainMenu_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("КЕРІВНИЦТВО КОРИСТУВАЧА.pdf");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Result_Summary_Show_Click(object sender, EventArgs e)
        {
            Data formdata = new Data();
            formdata.setDataFromForm(dataGridView1);

            try
            {
                Result_Summary_DataGrid.ColumnCount = 3;
                Result_Summary_DataGrid.Columns[0].Name = "Y";
                Result_Summary_DataGrid.Columns[1].Name = "Yreg";
                Result_Summary_DataGrid.Columns[2].Name = "σ";
                double Sigma = 0;
                switch (Result_Summary_Model_Select.SelectedItem.ToString())
                {
                    case "Linear":
                        Result_Summary_DataGrid.RowCount = formdata.Y.Length;
                        for (int i = 0; i < formdata.Y.Length; i++)
                        {
                            Result_Summary_DataGrid[0, i].Value = formdata.Y[i];
                            Result_Summary_DataGrid[1, i].Value = linear.Y[i];
                            Result_Summary_DataGrid[2, i].Value = Math.Pow(formdata.Y[i] - linear.Y[i], 2);
                            Sigma += Math.Pow(formdata.Y[i] - linear.Y[i], 2);
                            Result_Summary_Sigma.Text = Sigma.ToString();
                        };
                        break;
                    case "Exponential":
                        Result_Summary_DataGrid.RowCount = formdata.Y.Length;
                        for (int i = 0; i < formdata.Y.Length; i++)
                        {
                            Result_Summary_DataGrid[0, i].Value = formdata.Y[i];
                            Result_Summary_DataGrid[1, i].Value = exponential.Y[i];
                            Result_Summary_DataGrid[2, i].Value = Math.Pow(formdata.Y[i] - exponential.Y[i], 2);
                            Sigma += Math.Pow(formdata.Y[i] - exponential.Y[i], 2);
                            Result_Summary_Sigma.Text = Sigma.ToString();
                        };
                        break;
                    case "Polinomial":
                        Result_Summary_DataGrid.RowCount = formdata.Y.Length;
                        for (int i = 0; i < formdata.Y.Length; i++)
                        {
                            Result_Summary_DataGrid[0, i].Value = formdata.Y[i];
                            Result_Summary_DataGrid[1, i].Value = polinomial.Y[i];
                            Result_Summary_DataGrid[2, i].Value = Math.Pow(formdata.Y[i] - polinomial.Y[i], 2);
                            Sigma += Math.Pow(formdata.Y[i] - polinomial.Y[i], 2);
                            Result_Summary_Sigma.Text = Sigma.ToString();
                        };
                        break;
                    case "Polinomial Auto":
                        Result_Summary_DataGrid.RowCount = formdata.Y.Length;
                        for (int i = 0; i < formdata.Y.Length; i++)
                        {
                            Result_Summary_DataGrid[0, i].Value = formdata.Y[i];
                            Result_Summary_DataGrid[1, i].Value = polinomialAuto.Y[i];
                            Result_Summary_DataGrid[2, i].Value = Math.Pow(formdata.Y[i] - polinomialAuto.Y[i], 2);
                            Sigma += Math.Pow(formdata.Y[i] - polinomialAuto.Y[i], 2);
                            Result_Summary_Sigma.Text = Sigma.ToString();
                        };
                        break;
                    case "Degree":
                        Result_Summary_DataGrid.RowCount = formdata.Y.Length;
                        for (int i = 0; i < formdata.Y.Length; i++)
                        {
                            Result_Summary_DataGrid[0, i].Value = formdata.Y[i];
                            Result_Summary_DataGrid[1, i].Value = degree.Y[i];
                            Result_Summary_DataGrid[2, i].Value = Math.Pow(formdata.Y[i] - degree.Y[i], 2);
                            Sigma += Math.Pow(formdata.Y[i] - degree.Y[i], 2);
                            Result_Summary_Sigma.Text = Sigma.ToString();
                        };
                        break;
                    case "Giperbola":
                        Result_Summary_DataGrid.RowCount = formdata.Y.Length;
                        for (int i = 0; i < formdata.Y.Length; i++)
                        {
                            Result_Summary_DataGrid[0, i].Value = formdata.Y[i];
                            Result_Summary_DataGrid[1, i].Value = giperbola.Y[i];
                            Result_Summary_DataGrid[2, i].Value = Math.Pow(formdata.Y[i] - giperbola.Y[i], 2);
                            Sigma += Math.Pow(formdata.Y[i] - giperbola.Y[i], 2);
                            Result_Summary_Sigma.Text = Sigma.ToString();
                        };
                        break;
                    case "Square":
                        Result_Summary_DataGrid.RowCount = formdata.Y.Length;
                        for (int i = 0; i < formdata.Y.Length; i++)
                        {
                            Result_Summary_DataGrid[0, i].Value = formdata.Y[i];
                            Result_Summary_DataGrid[1, i].Value = square.Y[i];
                            Result_Summary_DataGrid[2, i].Value = Math.Pow(formdata.Y[i] - square.Y[i], 2);
                            Sigma += Math.Pow(formdata.Y[i] - square.Y[i], 2);
                            Result_Summary_Sigma.Text = Sigma.ToString();
                        };
                        break;
                    case " ": MessageBox.Show("Select model");
                        break;
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Select model");
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Model not calculated");
            }
        }
    }
}
