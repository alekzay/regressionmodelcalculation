﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RegressionModelsTest
{
    class Aproxymation
    {
        static public double korrelation(RegressionData data, double[] Y)
        {
            double baseDeviation = 0;
            double regressionDeviation = 0;
            double Yaverage = Y.Average();
            double regressionYaverage = data.Y.Average();
            double korrelation = 0;
            for (int i = 0; i < data.Y.Length; i++)
            {
                baseDeviation += Math.Pow((Y[i] - Yaverage), 2);
                regressionDeviation += Math.Pow((data.Y[i] - Yaverage), 2);
            }
            korrelation = Math.Sqrt(regressionDeviation / baseDeviation);
            return korrelation;
        }

        static public double korrelationIndex(RegressionData data, double[] Y)
        {
            double baseDeviation = 0;
            double regressionDeviation = 0;
            double Yaverage = Y.Average();
            double regressionYaverage = data.Y.Average();
            double korrelation = 0;
            for (int i = 0; i < data.Y.Length; i++)
            {
                baseDeviation += Math.Pow((data.Y[i] - Y[i]), 2);
                regressionDeviation += Math.Pow((Y[i] - Y.Average()), 2);
            }
            korrelation = Math.Sqrt(1 - (baseDeviation/regressionDeviation));
            return korrelation;

        }

        static public double R(double k)
        {
            return Math.Pow(k, 2);
        }

        static public double Radj(RegressionData data, double[,] X)
        {
            double Radj = 0;
            Radj = 1 - (1 - data.R) * (data.Y.Length - 1) / (data.Y.Length - (X.Length / data.Y.Length - 1) - 1);
            return Radj;
        }

        static public void Analysys(RegressionData data, Data formdata)
        {
            data.korrelation = Aproxymation.korrelation(data, formdata.Y);
            data.R = Aproxymation.R(data.korrelation);
            data.Radj = Aproxymation.Radj(data, formdata.X);
        }
        static public void AnalysysNonLinear(RegressionData data, Data formdata)
        {
            data.korrelation = Aproxymation.korrelationIndex(data, formdata.Y);
            data.R = Aproxymation.R(data.korrelation);
            data.Radj = Aproxymation.Radj(data, formdata.X);
        }
    }
}
