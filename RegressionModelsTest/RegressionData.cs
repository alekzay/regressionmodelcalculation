﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RegressionModelsTest
{
    class RegressionData 
    {
        public double[] Y;
        public double[] Koef;
        public double[,] Xall;
        public double R;
        public double korrelation;
        public double Radj;
        public double errorcount;

        
        public RegressionData(double[] X, double[] Y)
        {
            X = new double[X.Length];
            Y = new double[Y.Length];
            Koef = new double[X.Length +1];
        }
        public RegressionData(double[,] X, double[] Y)
        {
            Xall = new double[Y.Length, X.Length/Y.Length];
            Y = new double[Y.Length];
            Koef = new double[X.Length/Y.Length + 1];
        }
    }
}
