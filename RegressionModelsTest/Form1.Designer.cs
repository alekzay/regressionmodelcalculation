﻿namespace RegressionModelsTest
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea7 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.mainTab = new System.Windows.Forms.TabControl();
            this.Data_Page = new System.Windows.Forms.TabPage();
            this.removeRowIndex = new System.Windows.Forms.TextBox();
            this.removeRow = new System.Windows.Forms.Button();
            this.polinomial_range = new System.Windows.Forms.ComboBox();
            this.Polinomial_CheckBox = new System.Windows.Forms.CheckBox();
            this.Exponential_CheckBox = new System.Windows.Forms.CheckBox();
            this.Sqrt_CheckBox = new System.Windows.Forms.CheckBox();
            this.setYTextBox = new System.Windows.Forms.TextBox();
            this.setYButton = new System.Windows.Forms.Button();
            this.removeIndex = new System.Windows.Forms.TextBox();
            this.Auto_Calculate = new System.Windows.Forms.Button();
            this.comboExcelSheets = new System.Windows.Forms.ComboBox();
            this.Open_file_button = new System.Windows.Forms.Button();
            this.Select_Model = new System.Windows.Forms.Label();
            this.Giperbola_CheckBox = new System.Windows.Forms.CheckBox();
            this.Degree_CheckBox = new System.Windows.Forms.CheckBox();
            this.Linear_CheckBox = new System.Windows.Forms.CheckBox();
            this.removeColumn = new System.Windows.Forms.Button();
            this.addColumn = new System.Windows.Forms.Button();
            this.Calculate = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.YData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.XData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Result_Page = new System.Windows.Forms.TabPage();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.Result_Polinomial_Radj = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.Result_Polinomial_R = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.Result_Exponential_Radj = new System.Windows.Forms.TextBox();
            this.Result_Square_Radj = new System.Windows.Forms.TextBox();
            this.Result_Giperbola_Radj = new System.Windows.Forms.TextBox();
            this.Result_Degrree_Radj = new System.Windows.Forms.TextBox();
            this.Result_Linear_Radj = new System.Windows.Forms.TextBox();
            this.Result_Exponential_R = new System.Windows.Forms.TextBox();
            this.Result_Square_R = new System.Windows.Forms.TextBox();
            this.Result_Giperbola_R = new System.Windows.Forms.TextBox();
            this.Result_Degrree_R = new System.Windows.Forms.TextBox();
            this.Result_Linear_R = new System.Windows.Forms.TextBox();
            this.Result_polinomial = new System.Windows.Forms.RichTextBox();
            this.Polinomial_Details = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.Result_Polinomial_korrelation = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.Result_exponential = new System.Windows.Forms.RichTextBox();
            this.Exponential_Details = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.Result_Exponential_korrelation = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.Result_sqrt = new System.Windows.Forms.RichTextBox();
            this.Sqrt_Details = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.Result_Square_korrelation = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.Result_giperbola = new System.Windows.Forms.RichTextBox();
            this.Result_Linear = new System.Windows.Forms.RichTextBox();
            this.Result_Degree = new System.Windows.Forms.RichTextBox();
            this.Giperbola_Details = new System.Windows.Forms.Button();
            this.Degree_Details = new System.Windows.Forms.Button();
            this.Linear_Details = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.Result_Giperbola_korrelation = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Result_Degrree_korelation = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Result_Linear_korrelation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Auto_Result = new System.Windows.Forms.TabPage();
            this.Auto_polinomial_Radj = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.Auto_exponential_Radj = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.Auto_linear_Radj = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.polinomial_auto_details = new System.Windows.Forms.Button();
            this.exponential_auto_details = new System.Windows.Forms.Button();
            this.linear_auto_details = new System.Windows.Forms.Button();
            this.R_polinomial = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.R_exponential = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.R_linear = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Auto_polinomial_result = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Auto_polinomial_korrelation = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.Auto_exponential_result = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Auto_exponential_korrelation = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.Auto_linear_result = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Auto_linear_korrelation = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Linear_Page = new System.Windows.Forms.TabPage();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.Linear_Y_min = new System.Windows.Forms.TextBox();
            this.Linear_Y_max = new System.Windows.Forms.TextBox();
            this.Linear_standart_graph = new System.Windows.Forms.RadioButton();
            this.Linear_max_graph = new System.Windows.Forms.RadioButton();
            this.Linear_avg_graph = new System.Windows.Forms.RadioButton();
            this.Linear_min_graph = new System.Windows.Forms.RadioButton();
            this.linear_graph_function = new System.Windows.Forms.RichTextBox();
            this.Linear_save = new System.Windows.Forms.Button();
            this.linear_detail_R = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.linear_detail_korrelation = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Linear_Levellnes_draw = new System.Windows.Forms.Button();
            this.Linear_Chart_viewer = new ChartDirector.WinChartViewer();
            this.Linear_3Dchart_Draw = new System.Windows.Forms.Button();
            this.linear_X2_collection = new System.Windows.Forms.ComboBox();
            this.Linear_chart_Draw = new System.Windows.Forms.Button();
            this.linear_X1_collection = new System.Windows.Forms.ComboBox();
            this.linearChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Exponential_Page = new System.Windows.Forms.TabPage();
            this.label62 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.Exponential_Y_min = new System.Windows.Forms.TextBox();
            this.Exponential_Y_max = new System.Windows.Forms.TextBox();
            this.Exponential_standart_graph = new System.Windows.Forms.RadioButton();
            this.Exponential_max_graph = new System.Windows.Forms.RadioButton();
            this.Exponential_avg_graph = new System.Windows.Forms.RadioButton();
            this.Exponential_min_graph = new System.Windows.Forms.RadioButton();
            this.exponential_grahp_function = new System.Windows.Forms.RichTextBox();
            this.Exponential_save = new System.Windows.Forms.Button();
            this.Exponential_levellines_draw = new System.Windows.Forms.Button();
            this.exponential_detail_R = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.exponential_detail_korrelation = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Exponential_Chart_viewer = new ChartDirector.WinChartViewer();
            this.Exponential_3Dchart_Draw = new System.Windows.Forms.Button();
            this.exponential_X2_collection = new System.Windows.Forms.ComboBox();
            this.Exponential_chart_Draw = new System.Windows.Forms.Button();
            this.exponential_X1_collection = new System.Windows.Forms.ComboBox();
            this.exponentialChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.PolinomialAuto_Page = new System.Windows.Forms.TabPage();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.PolinomialAuto_Y_min = new System.Windows.Forms.TextBox();
            this.PolinomialAuto_Y_max = new System.Windows.Forms.TextBox();
            this.PolinomialAuto_standart_graph = new System.Windows.Forms.RadioButton();
            this.PolinomialAuto_max_graph = new System.Windows.Forms.RadioButton();
            this.PolinomialAuto_avg_graph = new System.Windows.Forms.RadioButton();
            this.PolinomialAuto_min_graph = new System.Windows.Forms.RadioButton();
            this.polinomialAuto_grahp_function = new System.Windows.Forms.RichTextBox();
            this.PolinomialAuto_save = new System.Windows.Forms.Button();
            this.polinomialAutoChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.PolinomialAuto_levellines_draw = new System.Windows.Forms.Button();
            this.PolinomialAuto_3Dchart_Draw = new System.Windows.Forms.Button();
            this.polinomialAuto_X2_collection = new System.Windows.Forms.ComboBox();
            this.PolinomialAuto_chart_Draw = new System.Windows.Forms.Button();
            this.polinomialAuto_X1_collection = new System.Windows.Forms.ComboBox();
            this.PolinomialAuto_Chart_viewer = new ChartDirector.WinChartViewer();
            this.polinomialAuto_detail_R = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.polinomialAuto_detail_korrelation = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Degree_Page = new System.Windows.Forms.TabPage();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.Degree_Y_min = new System.Windows.Forms.TextBox();
            this.Degree_Y_max = new System.Windows.Forms.TextBox();
            this.Degree_standart_graph = new System.Windows.Forms.RadioButton();
            this.Degree_max_graph = new System.Windows.Forms.RadioButton();
            this.Degree_avg_graph = new System.Windows.Forms.RadioButton();
            this.Degree_min_graph = new System.Windows.Forms.RadioButton();
            this.degree_grahp_function = new System.Windows.Forms.RichTextBox();
            this.Degree_save = new System.Windows.Forms.Button();
            this.Degree_levellines_draw = new System.Windows.Forms.Button();
            this.Degree_3Dchart_Draw = new System.Windows.Forms.Button();
            this.degree_X2_collection = new System.Windows.Forms.ComboBox();
            this.Degree_chart_Draw = new System.Windows.Forms.Button();
            this.degree_X1_collection = new System.Windows.Forms.ComboBox();
            this.Degree_Chart_viewer = new ChartDirector.WinChartViewer();
            this.degree_detail_R = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.degree_detail_korrelation = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.degreeChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Giperbola_Page = new System.Windows.Forms.TabPage();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.Giperbola_Y_min = new System.Windows.Forms.TextBox();
            this.Giperbola_Y_max = new System.Windows.Forms.TextBox();
            this.Giperbola_standart_graph = new System.Windows.Forms.RadioButton();
            this.Giperbola_max_graph = new System.Windows.Forms.RadioButton();
            this.Giperbola_avg_graph = new System.Windows.Forms.RadioButton();
            this.Giperbola_min_graph = new System.Windows.Forms.RadioButton();
            this.giperbola_grahp_function = new System.Windows.Forms.RichTextBox();
            this.Giperbola_save = new System.Windows.Forms.Button();
            this.Giperbola_levellines_draw = new System.Windows.Forms.Button();
            this.Giperbola_3Dchart_Draw = new System.Windows.Forms.Button();
            this.giperbola_X2_collection = new System.Windows.Forms.ComboBox();
            this.Giperbola_chart_Draw = new System.Windows.Forms.Button();
            this.giperbola_X1_collection = new System.Windows.Forms.ComboBox();
            this.Giperbola_Chart_viewer = new ChartDirector.WinChartViewer();
            this.giperbola_detail_R = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.giperbola_detail_korrelation = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.giperbolaChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Square_Page = new System.Windows.Forms.TabPage();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.Square_Y_min = new System.Windows.Forms.TextBox();
            this.Square_Y_max = new System.Windows.Forms.TextBox();
            this.Square_standart_graph = new System.Windows.Forms.RadioButton();
            this.Square_max_graph = new System.Windows.Forms.RadioButton();
            this.Square_avg_graph = new System.Windows.Forms.RadioButton();
            this.Square_min_graph = new System.Windows.Forms.RadioButton();
            this.square_grahp_function = new System.Windows.Forms.RichTextBox();
            this.Square_save = new System.Windows.Forms.Button();
            this.squareChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Square_levellines_draw = new System.Windows.Forms.Button();
            this.Square_3Dchart_Draw = new System.Windows.Forms.Button();
            this.square_X2_collection = new System.Windows.Forms.ComboBox();
            this.Square_chart_Draw = new System.Windows.Forms.Button();
            this.square_X1_collection = new System.Windows.Forms.ComboBox();
            this.Square_Chart_viewer = new ChartDirector.WinChartViewer();
            this.square_detail_R = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.square_detail_korrelation = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.Polinomial_Page = new System.Windows.Forms.TabPage();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.Polinomial_Y_min = new System.Windows.Forms.TextBox();
            this.Polinomial_Y_max = new System.Windows.Forms.TextBox();
            this.Polinomial_standart_graph = new System.Windows.Forms.RadioButton();
            this.Polinomial_max_graph = new System.Windows.Forms.RadioButton();
            this.Polinomial_avg_graph = new System.Windows.Forms.RadioButton();
            this.Polinomial_min_graph = new System.Windows.Forms.RadioButton();
            this.polinomial_grahp_function = new System.Windows.Forms.RichTextBox();
            this.Polinomial_save = new System.Windows.Forms.Button();
            this.Polinomial_Chart_viewer = new ChartDirector.WinChartViewer();
            this.polinomialChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.polinomial_detail_R = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.polinomial_detail_korrelation = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.Polinomial_levellines_draw = new System.Windows.Forms.Button();
            this.Polinomial_3Dchart_Draw = new System.Windows.Forms.Button();
            this.polinomial_X2_collection = new System.Windows.Forms.ComboBox();
            this.Polinomial_chart_Draw = new System.Windows.Forms.Button();
            this.polinomial_X1_collection = new System.Windows.Forms.ComboBox();
            this.TimePage = new System.Windows.Forms.TabPage();
            this.label72 = new System.Windows.Forms.Label();
            this.Total_Auto_time = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.Exponential_Auto_time = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.Linear_Auto_time = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.Polinomial_time = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.Square_time = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.Giperbola_time = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.Degree_time = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.Polinomial_Auto_time = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.Exponential_time = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.Linear_time = new System.Windows.Forms.TextBox();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.hmainMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpmainMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Result_Summary = new System.Windows.Forms.TabPage();
            this.Result_Summary_DataGrid = new System.Windows.Forms.DataGridView();
            this.Result_Summary_Show = new System.Windows.Forms.Button();
            this.Result_Summary_Model_Select = new System.Windows.Forms.ComboBox();
            this.Result_Summary_Sigma = new System.Windows.Forms.TextBox();
            this.mainTab.SuspendLayout();
            this.Data_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.Result_Page.SuspendLayout();
            this.Auto_Result.SuspendLayout();
            this.Linear_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Linear_Chart_viewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearChart)).BeginInit();
            this.Exponential_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Exponential_Chart_viewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentialChart)).BeginInit();
            this.PolinomialAuto_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polinomialAutoChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolinomialAuto_Chart_viewer)).BeginInit();
            this.Degree_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Degree_Chart_viewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.degreeChart)).BeginInit();
            this.Giperbola_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Giperbola_Chart_viewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.giperbolaChart)).BeginInit();
            this.Square_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.squareChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Square_Chart_viewer)).BeginInit();
            this.Polinomial_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Polinomial_Chart_viewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.polinomialChart)).BeginInit();
            this.TimePage.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.Result_Summary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Result_Summary_DataGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.Data_Page);
            this.mainTab.Controls.Add(this.Result_Page);
            this.mainTab.Controls.Add(this.Auto_Result);
            this.mainTab.Controls.Add(this.Linear_Page);
            this.mainTab.Controls.Add(this.Exponential_Page);
            this.mainTab.Controls.Add(this.PolinomialAuto_Page);
            this.mainTab.Controls.Add(this.Degree_Page);
            this.mainTab.Controls.Add(this.Giperbola_Page);
            this.mainTab.Controls.Add(this.Square_Page);
            this.mainTab.Controls.Add(this.Polinomial_Page);
            this.mainTab.Controls.Add(this.TimePage);
            this.mainTab.Controls.Add(this.Result_Summary);
            this.mainTab.Location = new System.Drawing.Point(25, 26);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedIndex = 0;
            this.mainTab.Size = new System.Drawing.Size(850, 480);
            this.mainTab.TabIndex = 0;
            // 
            // Data_Page
            // 
            this.Data_Page.Controls.Add(this.removeRowIndex);
            this.Data_Page.Controls.Add(this.removeRow);
            this.Data_Page.Controls.Add(this.polinomial_range);
            this.Data_Page.Controls.Add(this.Polinomial_CheckBox);
            this.Data_Page.Controls.Add(this.Exponential_CheckBox);
            this.Data_Page.Controls.Add(this.Sqrt_CheckBox);
            this.Data_Page.Controls.Add(this.setYTextBox);
            this.Data_Page.Controls.Add(this.setYButton);
            this.Data_Page.Controls.Add(this.removeIndex);
            this.Data_Page.Controls.Add(this.Auto_Calculate);
            this.Data_Page.Controls.Add(this.comboExcelSheets);
            this.Data_Page.Controls.Add(this.Open_file_button);
            this.Data_Page.Controls.Add(this.Select_Model);
            this.Data_Page.Controls.Add(this.Giperbola_CheckBox);
            this.Data_Page.Controls.Add(this.Degree_CheckBox);
            this.Data_Page.Controls.Add(this.Linear_CheckBox);
            this.Data_Page.Controls.Add(this.removeColumn);
            this.Data_Page.Controls.Add(this.addColumn);
            this.Data_Page.Controls.Add(this.Calculate);
            this.Data_Page.Controls.Add(this.dataGridView1);
            this.Data_Page.Location = new System.Drawing.Point(4, 22);
            this.Data_Page.Name = "Data_Page";
            this.Data_Page.Padding = new System.Windows.Forms.Padding(3);
            this.Data_Page.Size = new System.Drawing.Size(842, 454);
            this.Data_Page.TabIndex = 0;
            this.Data_Page.Text = "Data";
            this.Data_Page.UseVisualStyleBackColor = true;
            // 
            // removeRowIndex
            // 
            this.removeRowIndex.Location = new System.Drawing.Point(686, 288);
            this.removeRowIndex.Name = "removeRowIndex";
            this.removeRowIndex.Size = new System.Drawing.Size(30, 20);
            this.removeRowIndex.TabIndex = 19;
            // 
            // removeRow
            // 
            this.removeRow.Location = new System.Drawing.Point(572, 285);
            this.removeRow.Name = "removeRow";
            this.removeRow.Size = new System.Drawing.Size(100, 25);
            this.removeRow.TabIndex = 18;
            this.removeRow.Text = "remove Row";
            this.removeRow.UseVisualStyleBackColor = true;
            this.removeRow.Click += new System.EventHandler(this.removeRow_Click);
            // 
            // polinomial_range
            // 
            this.polinomial_range.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.polinomial_range.FormattingEnabled = true;
            this.polinomial_range.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5"});
            this.polinomial_range.Location = new System.Drawing.Point(792, 194);
            this.polinomial_range.Name = "polinomial_range";
            this.polinomial_range.Size = new System.Drawing.Size(30, 21);
            this.polinomial_range.TabIndex = 17;
            // 
            // Polinomial_CheckBox
            // 
            this.Polinomial_CheckBox.AutoSize = true;
            this.Polinomial_CheckBox.Location = new System.Drawing.Point(713, 196);
            this.Polinomial_CheckBox.Name = "Polinomial_CheckBox";
            this.Polinomial_CheckBox.Size = new System.Drawing.Size(73, 17);
            this.Polinomial_CheckBox.TabIndex = 16;
            this.Polinomial_CheckBox.Text = "Polinomial";
            this.Polinomial_CheckBox.UseVisualStyleBackColor = true;
            // 
            // Exponential_CheckBox
            // 
            this.Exponential_CheckBox.AutoSize = true;
            this.Exponential_CheckBox.Location = new System.Drawing.Point(713, 173);
            this.Exponential_CheckBox.Name = "Exponential_CheckBox";
            this.Exponential_CheckBox.Size = new System.Drawing.Size(81, 17);
            this.Exponential_CheckBox.TabIndex = 15;
            this.Exponential_CheckBox.Text = "Exponential";
            this.Exponential_CheckBox.UseVisualStyleBackColor = true;
            // 
            // Sqrt_CheckBox
            // 
            this.Sqrt_CheckBox.AutoSize = true;
            this.Sqrt_CheckBox.Location = new System.Drawing.Point(713, 150);
            this.Sqrt_CheckBox.Name = "Sqrt_CheckBox";
            this.Sqrt_CheckBox.Size = new System.Drawing.Size(60, 17);
            this.Sqrt_CheckBox.TabIndex = 14;
            this.Sqrt_CheckBox.Text = "Square";
            this.Sqrt_CheckBox.UseVisualStyleBackColor = true;
            // 
            // setYTextBox
            // 
            this.setYTextBox.Location = new System.Drawing.Point(686, 319);
            this.setYTextBox.Name = "setYTextBox";
            this.setYTextBox.Size = new System.Drawing.Size(30, 20);
            this.setYTextBox.TabIndex = 13;
            // 
            // setYButton
            // 
            this.setYButton.Location = new System.Drawing.Point(572, 316);
            this.setYButton.Name = "setYButton";
            this.setYButton.Size = new System.Drawing.Size(100, 25);
            this.setYButton.TabIndex = 12;
            this.setYButton.Text = "set Y";
            this.setYButton.UseVisualStyleBackColor = true;
            this.setYButton.Click += new System.EventHandler(this.setYButton_Click);
            // 
            // removeIndex
            // 
            this.removeIndex.Location = new System.Drawing.Point(686, 257);
            this.removeIndex.Name = "removeIndex";
            this.removeIndex.Size = new System.Drawing.Size(30, 20);
            this.removeIndex.TabIndex = 11;
            // 
            // Auto_Calculate
            // 
            this.Auto_Calculate.Location = new System.Drawing.Point(734, 410);
            this.Auto_Calculate.Name = "Auto_Calculate";
            this.Auto_Calculate.Size = new System.Drawing.Size(100, 30);
            this.Auto_Calculate.TabIndex = 10;
            this.Auto_Calculate.Text = "Auto";
            this.Auto_Calculate.UseVisualStyleBackColor = true;
            this.Auto_Calculate.Click += new System.EventHandler(this.Auto_Calculate_Click);
            // 
            // comboExcelSheets
            // 
            this.comboExcelSheets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboExcelSheets.FormattingEnabled = true;
            this.comboExcelSheets.Location = new System.Drawing.Point(572, 44);
            this.comboExcelSheets.Name = "comboExcelSheets";
            this.comboExcelSheets.Size = new System.Drawing.Size(121, 21);
            this.comboExcelSheets.TabIndex = 9;
            this.comboExcelSheets.Visible = false;
            this.comboExcelSheets.SelectedIndexChanged += new System.EventHandler(this.comboExcelSheets_SelectedIndexChanged);
            // 
            // Open_file_button
            // 
            this.Open_file_button.Location = new System.Drawing.Point(572, 103);
            this.Open_file_button.Name = "Open_file_button";
            this.Open_file_button.Size = new System.Drawing.Size(100, 25);
            this.Open_file_button.TabIndex = 8;
            this.Open_file_button.Text = "Open file";
            this.Open_file_button.UseVisualStyleBackColor = true;
            this.Open_file_button.Click += new System.EventHandler(this.Open_file_button_Click);
            // 
            // Select_Model
            // 
            this.Select_Model.AutoSize = true;
            this.Select_Model.Location = new System.Drawing.Point(710, 52);
            this.Select_Model.Name = "Select_Model";
            this.Select_Model.Size = new System.Drawing.Size(72, 13);
            this.Select_Model.TabIndex = 7;
            this.Select_Model.Text = "Select Model:";
            // 
            // Giperbola_CheckBox
            // 
            this.Giperbola_CheckBox.AutoSize = true;
            this.Giperbola_CheckBox.Location = new System.Drawing.Point(713, 127);
            this.Giperbola_CheckBox.Name = "Giperbola_CheckBox";
            this.Giperbola_CheckBox.Size = new System.Drawing.Size(71, 17);
            this.Giperbola_CheckBox.TabIndex = 6;
            this.Giperbola_CheckBox.Text = "Giperbola";
            this.Giperbola_CheckBox.UseVisualStyleBackColor = true;
            // 
            // Degree_CheckBox
            // 
            this.Degree_CheckBox.AutoSize = true;
            this.Degree_CheckBox.Location = new System.Drawing.Point(713, 103);
            this.Degree_CheckBox.Name = "Degree_CheckBox";
            this.Degree_CheckBox.Size = new System.Drawing.Size(61, 17);
            this.Degree_CheckBox.TabIndex = 5;
            this.Degree_CheckBox.Text = "Degree";
            this.Degree_CheckBox.UseVisualStyleBackColor = true;
            // 
            // Linear_CheckBox
            // 
            this.Linear_CheckBox.AutoSize = true;
            this.Linear_CheckBox.Location = new System.Drawing.Point(713, 79);
            this.Linear_CheckBox.Name = "Linear_CheckBox";
            this.Linear_CheckBox.Size = new System.Drawing.Size(55, 17);
            this.Linear_CheckBox.TabIndex = 4;
            this.Linear_CheckBox.Text = "Linear";
            this.Linear_CheckBox.UseVisualStyleBackColor = true;
            // 
            // removeColumn
            // 
            this.removeColumn.Location = new System.Drawing.Point(572, 254);
            this.removeColumn.Name = "removeColumn";
            this.removeColumn.Size = new System.Drawing.Size(100, 25);
            this.removeColumn.TabIndex = 3;
            this.removeColumn.Text = "remove Column";
            this.removeColumn.UseVisualStyleBackColor = true;
            this.removeColumn.Click += new System.EventHandler(this.removeColumn_Click);
            // 
            // addColumn
            // 
            this.addColumn.Location = new System.Drawing.Point(572, 224);
            this.addColumn.Name = "addColumn";
            this.addColumn.Size = new System.Drawing.Size(100, 25);
            this.addColumn.TabIndex = 2;
            this.addColumn.Text = "add Column";
            this.addColumn.UseVisualStyleBackColor = true;
            this.addColumn.Click += new System.EventHandler(this.addColumn_Click);
            // 
            // Calculate
            // 
            this.Calculate.Location = new System.Drawing.Point(734, 373);
            this.Calculate.Name = "Calculate";
            this.Calculate.Size = new System.Drawing.Size(100, 30);
            this.Calculate.TabIndex = 1;
            this.Calculate.Text = "Calculate";
            this.Calculate.UseVisualStyleBackColor = true;
            this.Calculate.Click += new System.EventHandler(this.Calculate_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.YData,
            this.XData});
            this.dataGridView1.Location = new System.Drawing.Point(35, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(515, 336);
            this.dataGridView1.TabIndex = 0;
            // 
            // YData
            // 
            this.YData.HeaderText = "Y";
            this.YData.Name = "YData";
            // 
            // XData
            // 
            this.XData.HeaderText = "X";
            this.XData.Name = "XData";
            // 
            // Result_Page
            // 
            this.Result_Page.AutoScroll = true;
            this.Result_Page.Controls.Add(this.label59);
            this.Result_Page.Controls.Add(this.label58);
            this.Result_Page.Controls.Add(this.label57);
            this.Result_Page.Controls.Add(this.label56);
            this.Result_Page.Controls.Add(this.label55);
            this.Result_Page.Controls.Add(this.label54);
            this.Result_Page.Controls.Add(this.Result_Polinomial_Radj);
            this.Result_Page.Controls.Add(this.label53);
            this.Result_Page.Controls.Add(this.Result_Polinomial_R);
            this.Result_Page.Controls.Add(this.label52);
            this.Result_Page.Controls.Add(this.label51);
            this.Result_Page.Controls.Add(this.label50);
            this.Result_Page.Controls.Add(this.label49);
            this.Result_Page.Controls.Add(this.label48);
            this.Result_Page.Controls.Add(this.Result_Exponential_Radj);
            this.Result_Page.Controls.Add(this.Result_Square_Radj);
            this.Result_Page.Controls.Add(this.Result_Giperbola_Radj);
            this.Result_Page.Controls.Add(this.Result_Degrree_Radj);
            this.Result_Page.Controls.Add(this.Result_Linear_Radj);
            this.Result_Page.Controls.Add(this.Result_Exponential_R);
            this.Result_Page.Controls.Add(this.Result_Square_R);
            this.Result_Page.Controls.Add(this.Result_Giperbola_R);
            this.Result_Page.Controls.Add(this.Result_Degrree_R);
            this.Result_Page.Controls.Add(this.Result_Linear_R);
            this.Result_Page.Controls.Add(this.Result_polinomial);
            this.Result_Page.Controls.Add(this.Polinomial_Details);
            this.Result_Page.Controls.Add(this.label40);
            this.Result_Page.Controls.Add(this.Result_Polinomial_korrelation);
            this.Result_Page.Controls.Add(this.label41);
            this.Result_Page.Controls.Add(this.label42);
            this.Result_Page.Controls.Add(this.Result_exponential);
            this.Result_Page.Controls.Add(this.Exponential_Details);
            this.Result_Page.Controls.Add(this.label43);
            this.Result_Page.Controls.Add(this.Result_Exponential_korrelation);
            this.Result_Page.Controls.Add(this.label44);
            this.Result_Page.Controls.Add(this.label45);
            this.Result_Page.Controls.Add(this.Result_sqrt);
            this.Result_Page.Controls.Add(this.Sqrt_Details);
            this.Result_Page.Controls.Add(this.label22);
            this.Result_Page.Controls.Add(this.Result_Square_korrelation);
            this.Result_Page.Controls.Add(this.label23);
            this.Result_Page.Controls.Add(this.label24);
            this.Result_Page.Controls.Add(this.Result_giperbola);
            this.Result_Page.Controls.Add(this.Result_Linear);
            this.Result_Page.Controls.Add(this.Result_Degree);
            this.Result_Page.Controls.Add(this.Giperbola_Details);
            this.Result_Page.Controls.Add(this.Degree_Details);
            this.Result_Page.Controls.Add(this.Linear_Details);
            this.Result_Page.Controls.Add(this.label7);
            this.Result_Page.Controls.Add(this.Result_Giperbola_korrelation);
            this.Result_Page.Controls.Add(this.label8);
            this.Result_Page.Controls.Add(this.label9);
            this.Result_Page.Controls.Add(this.label4);
            this.Result_Page.Controls.Add(this.Result_Degrree_korelation);
            this.Result_Page.Controls.Add(this.label5);
            this.Result_Page.Controls.Add(this.label6);
            this.Result_Page.Controls.Add(this.label3);
            this.Result_Page.Controls.Add(this.Result_Linear_korrelation);
            this.Result_Page.Controls.Add(this.label2);
            this.Result_Page.Controls.Add(this.label1);
            this.Result_Page.Location = new System.Drawing.Point(4, 22);
            this.Result_Page.Name = "Result_Page";
            this.Result_Page.Size = new System.Drawing.Size(842, 454);
            this.Result_Page.TabIndex = 4;
            this.Result_Page.Text = "Result";
            this.Result_Page.UseVisualStyleBackColor = true;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(598, 538);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(38, 13);
            this.label59.TabIndex = 62;
            this.label59.Text = "Radj =";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(598, 436);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(38, 13);
            this.label58.TabIndex = 61;
            this.label58.Text = "Radj =";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(598, 345);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(38, 13);
            this.label57.TabIndex = 60;
            this.label57.Text = "Radj =";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(598, 247);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(38, 13);
            this.label56.TabIndex = 59;
            this.label56.Text = "Radj =";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(598, 135);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(38, 13);
            this.label55.TabIndex = 58;
            this.label55.Text = "Radj =";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(598, 41);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(38, 13);
            this.label54.TabIndex = 57;
            this.label54.Text = "Radj =";
            // 
            // Result_Polinomial_Radj
            // 
            this.Result_Polinomial_Radj.Location = new System.Drawing.Point(638, 534);
            this.Result_Polinomial_Radj.Name = "Result_Polinomial_Radj";
            this.Result_Polinomial_Radj.Size = new System.Drawing.Size(80, 20);
            this.Result_Polinomial_Radj.TabIndex = 56;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(475, 560);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(36, 13);
            this.label53.TabIndex = 55;
            this.label53.Text = "R^2 =";
            // 
            // Result_Polinomial_R
            // 
            this.Result_Polinomial_R.Location = new System.Drawing.Point(512, 557);
            this.Result_Polinomial_R.Name = "Result_Polinomial_R";
            this.Result_Polinomial_R.Size = new System.Drawing.Size(80, 20);
            this.Result_Polinomial_R.TabIndex = 54;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(475, 462);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(36, 13);
            this.label52.TabIndex = 53;
            this.label52.Text = "R^2 =";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(475, 371);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(36, 13);
            this.label51.TabIndex = 52;
            this.label51.Text = "R^2 =";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(475, 273);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(36, 13);
            this.label50.TabIndex = 51;
            this.label50.Text = "R^2 =";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(475, 161);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(36, 13);
            this.label49.TabIndex = 50;
            this.label49.Text = "R^2 =";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(475, 67);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(36, 13);
            this.label48.TabIndex = 49;
            this.label48.Text = "R^2 =";
            // 
            // Result_Exponential_Radj
            // 
            this.Result_Exponential_Radj.Location = new System.Drawing.Point(638, 433);
            this.Result_Exponential_Radj.Name = "Result_Exponential_Radj";
            this.Result_Exponential_Radj.Size = new System.Drawing.Size(80, 20);
            this.Result_Exponential_Radj.TabIndex = 48;
            // 
            // Result_Square_Radj
            // 
            this.Result_Square_Radj.Location = new System.Drawing.Point(638, 342);
            this.Result_Square_Radj.Name = "Result_Square_Radj";
            this.Result_Square_Radj.Size = new System.Drawing.Size(80, 20);
            this.Result_Square_Radj.TabIndex = 47;
            // 
            // Result_Giperbola_Radj
            // 
            this.Result_Giperbola_Radj.Location = new System.Drawing.Point(638, 244);
            this.Result_Giperbola_Radj.Name = "Result_Giperbola_Radj";
            this.Result_Giperbola_Radj.Size = new System.Drawing.Size(80, 20);
            this.Result_Giperbola_Radj.TabIndex = 46;
            // 
            // Result_Degrree_Radj
            // 
            this.Result_Degrree_Radj.Location = new System.Drawing.Point(638, 132);
            this.Result_Degrree_Radj.Name = "Result_Degrree_Radj";
            this.Result_Degrree_Radj.Size = new System.Drawing.Size(80, 20);
            this.Result_Degrree_Radj.TabIndex = 45;
            // 
            // Result_Linear_Radj
            // 
            this.Result_Linear_Radj.Location = new System.Drawing.Point(638, 38);
            this.Result_Linear_Radj.Name = "Result_Linear_Radj";
            this.Result_Linear_Radj.Size = new System.Drawing.Size(80, 20);
            this.Result_Linear_Radj.TabIndex = 44;
            // 
            // Result_Exponential_R
            // 
            this.Result_Exponential_R.Location = new System.Drawing.Point(512, 459);
            this.Result_Exponential_R.Name = "Result_Exponential_R";
            this.Result_Exponential_R.Size = new System.Drawing.Size(80, 20);
            this.Result_Exponential_R.TabIndex = 43;
            // 
            // Result_Square_R
            // 
            this.Result_Square_R.Location = new System.Drawing.Point(512, 368);
            this.Result_Square_R.Name = "Result_Square_R";
            this.Result_Square_R.Size = new System.Drawing.Size(80, 20);
            this.Result_Square_R.TabIndex = 42;
            // 
            // Result_Giperbola_R
            // 
            this.Result_Giperbola_R.Location = new System.Drawing.Point(512, 270);
            this.Result_Giperbola_R.Name = "Result_Giperbola_R";
            this.Result_Giperbola_R.Size = new System.Drawing.Size(80, 20);
            this.Result_Giperbola_R.TabIndex = 41;
            // 
            // Result_Degrree_R
            // 
            this.Result_Degrree_R.Location = new System.Drawing.Point(512, 158);
            this.Result_Degrree_R.Name = "Result_Degrree_R";
            this.Result_Degrree_R.Size = new System.Drawing.Size(80, 20);
            this.Result_Degrree_R.TabIndex = 40;
            // 
            // Result_Linear_R
            // 
            this.Result_Linear_R.Location = new System.Drawing.Point(512, 64);
            this.Result_Linear_R.Name = "Result_Linear_R";
            this.Result_Linear_R.Size = new System.Drawing.Size(80, 20);
            this.Result_Linear_R.TabIndex = 39;
            // 
            // Result_polinomial
            // 
            this.Result_polinomial.Location = new System.Drawing.Point(71, 531);
            this.Result_polinomial.Name = "Result_polinomial";
            this.Result_polinomial.Size = new System.Drawing.Size(390, 70);
            this.Result_polinomial.TabIndex = 38;
            this.Result_polinomial.Text = "";
            // 
            // Polinomial_Details
            // 
            this.Polinomial_Details.Location = new System.Drawing.Point(724, 531);
            this.Polinomial_Details.Name = "Polinomial_Details";
            this.Polinomial_Details.Size = new System.Drawing.Size(85, 26);
            this.Polinomial_Details.TabIndex = 37;
            this.Polinomial_Details.Text = "Detail";
            this.Polinomial_Details.UseVisualStyleBackColor = true;
            this.Polinomial_Details.Click += new System.EventHandler(this.Polinomial_Details_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(38, 514);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(54, 13);
            this.label40.TabIndex = 36;
            this.label40.Text = "Polinomial";
            // 
            // Result_Polinomial_korrelation
            // 
            this.Result_Polinomial_korrelation.Location = new System.Drawing.Point(512, 531);
            this.Result_Polinomial_korrelation.Name = "Result_Polinomial_korrelation";
            this.Result_Polinomial_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Result_Polinomial_korrelation.TabIndex = 35;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(475, 534);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(31, 13);
            this.label41.TabIndex = 34;
            this.label41.Text = "kor =";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(44, 537);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(21, 13);
            this.label42.TabIndex = 33;
            this.label42.Text = "y =";
            // 
            // Result_exponential
            // 
            this.Result_exponential.Location = new System.Drawing.Point(71, 436);
            this.Result_exponential.Name = "Result_exponential";
            this.Result_exponential.Size = new System.Drawing.Size(390, 70);
            this.Result_exponential.TabIndex = 32;
            this.Result_exponential.Text = "";
            // 
            // Exponential_Details
            // 
            this.Exponential_Details.Location = new System.Drawing.Point(724, 433);
            this.Exponential_Details.Name = "Exponential_Details";
            this.Exponential_Details.Size = new System.Drawing.Size(85, 26);
            this.Exponential_Details.TabIndex = 31;
            this.Exponential_Details.Text = "Detail";
            this.Exponential_Details.UseVisualStyleBackColor = true;
            this.Exponential_Details.Click += new System.EventHandler(this.Exponential_Details_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(38, 416);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 13);
            this.label43.TabIndex = 30;
            this.label43.Text = "Exponential";
            // 
            // Result_Exponential_korrelation
            // 
            this.Result_Exponential_korrelation.Location = new System.Drawing.Point(512, 433);
            this.Result_Exponential_korrelation.Name = "Result_Exponential_korrelation";
            this.Result_Exponential_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Result_Exponential_korrelation.TabIndex = 29;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(475, 439);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(31, 13);
            this.label44.TabIndex = 28;
            this.label44.Text = "kor =";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(44, 439);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(21, 13);
            this.label45.TabIndex = 27;
            this.label45.Text = "y =";
            // 
            // Result_sqrt
            // 
            this.Result_sqrt.Location = new System.Drawing.Point(71, 345);
            this.Result_sqrt.Name = "Result_sqrt";
            this.Result_sqrt.Size = new System.Drawing.Size(390, 70);
            this.Result_sqrt.TabIndex = 26;
            this.Result_sqrt.Text = "";
            // 
            // Sqrt_Details
            // 
            this.Sqrt_Details.Location = new System.Drawing.Point(724, 342);
            this.Sqrt_Details.Name = "Sqrt_Details";
            this.Sqrt_Details.Size = new System.Drawing.Size(85, 26);
            this.Sqrt_Details.TabIndex = 25;
            this.Sqrt_Details.Text = "Detail";
            this.Sqrt_Details.UseVisualStyleBackColor = true;
            this.Sqrt_Details.Click += new System.EventHandler(this.Sqrt_Details_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(38, 325);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 24;
            this.label22.Text = "Square";
            // 
            // Result_Square_korrelation
            // 
            this.Result_Square_korrelation.Location = new System.Drawing.Point(512, 342);
            this.Result_Square_korrelation.Name = "Result_Square_korrelation";
            this.Result_Square_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Result_Square_korrelation.TabIndex = 23;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(475, 345);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "kor =";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(44, 348);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(21, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "y =";
            // 
            // Result_giperbola
            // 
            this.Result_giperbola.Location = new System.Drawing.Point(71, 244);
            this.Result_giperbola.Name = "Result_giperbola";
            this.Result_giperbola.Size = new System.Drawing.Size(390, 70);
            this.Result_giperbola.TabIndex = 20;
            this.Result_giperbola.Text = "";
            // 
            // Result_Linear
            // 
            this.Result_Linear.Location = new System.Drawing.Point(71, 38);
            this.Result_Linear.Name = "Result_Linear";
            this.Result_Linear.Size = new System.Drawing.Size(390, 70);
            this.Result_Linear.TabIndex = 19;
            this.Result_Linear.Text = "";
            // 
            // Result_Degree
            // 
            this.Result_Degree.Location = new System.Drawing.Point(71, 136);
            this.Result_Degree.Name = "Result_Degree";
            this.Result_Degree.Size = new System.Drawing.Size(390, 70);
            this.Result_Degree.TabIndex = 18;
            this.Result_Degree.Text = "";
            // 
            // Giperbola_Details
            // 
            this.Giperbola_Details.Location = new System.Drawing.Point(724, 244);
            this.Giperbola_Details.Name = "Giperbola_Details";
            this.Giperbola_Details.Size = new System.Drawing.Size(85, 26);
            this.Giperbola_Details.TabIndex = 17;
            this.Giperbola_Details.Text = "Detail";
            this.Giperbola_Details.UseVisualStyleBackColor = true;
            this.Giperbola_Details.Click += new System.EventHandler(this.Giperbola_Details_Click);
            // 
            // Degree_Details
            // 
            this.Degree_Details.Location = new System.Drawing.Point(724, 135);
            this.Degree_Details.Name = "Degree_Details";
            this.Degree_Details.Size = new System.Drawing.Size(85, 26);
            this.Degree_Details.TabIndex = 16;
            this.Degree_Details.Text = "Detail";
            this.Degree_Details.UseVisualStyleBackColor = true;
            this.Degree_Details.Click += new System.EventHandler(this.Degree_Details_Click);
            // 
            // Linear_Details
            // 
            this.Linear_Details.Location = new System.Drawing.Point(724, 34);
            this.Linear_Details.Name = "Linear_Details";
            this.Linear_Details.Size = new System.Drawing.Size(85, 26);
            this.Linear_Details.TabIndex = 15;
            this.Linear_Details.Text = "Detail";
            this.Linear_Details.UseVisualStyleBackColor = true;
            this.Linear_Details.Click += new System.EventHandler(this.Linear_Details_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Giperbola";
            // 
            // Result_Giperbola_korrelation
            // 
            this.Result_Giperbola_korrelation.Location = new System.Drawing.Point(512, 244);
            this.Result_Giperbola_korrelation.Name = "Result_Giperbola_korrelation";
            this.Result_Giperbola_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Result_Giperbola_korrelation.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(475, 247);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "kor =";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 247);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "y =";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Degree";
            // 
            // Result_Degrree_korelation
            // 
            this.Result_Degrree_korelation.Location = new System.Drawing.Point(512, 132);
            this.Result_Degrree_korelation.Name = "Result_Degrree_korelation";
            this.Result_Degrree_korelation.Size = new System.Drawing.Size(80, 20);
            this.Result_Degrree_korelation.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(475, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "kor =";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "y =";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Linear";
            // 
            // Result_Linear_korrelation
            // 
            this.Result_Linear_korrelation.Location = new System.Drawing.Point(512, 38);
            this.Result_Linear_korrelation.Name = "Result_Linear_korrelation";
            this.Result_Linear_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Result_Linear_korrelation.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(475, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "kor =";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "y =";
            // 
            // Auto_Result
            // 
            this.Auto_Result.Controls.Add(this.Auto_polinomial_Radj);
            this.Auto_Result.Controls.Add(this.label39);
            this.Auto_Result.Controls.Add(this.Auto_exponential_Radj);
            this.Auto_Result.Controls.Add(this.label38);
            this.Auto_Result.Controls.Add(this.Auto_linear_Radj);
            this.Auto_Result.Controls.Add(this.label37);
            this.Auto_Result.Controls.Add(this.polinomial_auto_details);
            this.Auto_Result.Controls.Add(this.exponential_auto_details);
            this.Auto_Result.Controls.Add(this.linear_auto_details);
            this.Auto_Result.Controls.Add(this.R_polinomial);
            this.Auto_Result.Controls.Add(this.label21);
            this.Auto_Result.Controls.Add(this.R_exponential);
            this.Auto_Result.Controls.Add(this.label20);
            this.Auto_Result.Controls.Add(this.R_linear);
            this.Auto_Result.Controls.Add(this.label19);
            this.Auto_Result.Controls.Add(this.Auto_polinomial_result);
            this.Auto_Result.Controls.Add(this.label16);
            this.Auto_Result.Controls.Add(this.Auto_polinomial_korrelation);
            this.Auto_Result.Controls.Add(this.label17);
            this.Auto_Result.Controls.Add(this.label18);
            this.Auto_Result.Controls.Add(this.Auto_exponential_result);
            this.Auto_Result.Controls.Add(this.label13);
            this.Auto_Result.Controls.Add(this.Auto_exponential_korrelation);
            this.Auto_Result.Controls.Add(this.label14);
            this.Auto_Result.Controls.Add(this.label15);
            this.Auto_Result.Controls.Add(this.Auto_linear_result);
            this.Auto_Result.Controls.Add(this.label10);
            this.Auto_Result.Controls.Add(this.Auto_linear_korrelation);
            this.Auto_Result.Controls.Add(this.label11);
            this.Auto_Result.Controls.Add(this.label12);
            this.Auto_Result.Location = new System.Drawing.Point(4, 22);
            this.Auto_Result.Name = "Auto_Result";
            this.Auto_Result.Size = new System.Drawing.Size(842, 454);
            this.Auto_Result.TabIndex = 5;
            this.Auto_Result.Text = "Auto";
            this.Auto_Result.UseVisualStyleBackColor = true;
            // 
            // Auto_polinomial_Radj
            // 
            this.Auto_polinomial_Radj.Location = new System.Drawing.Point(650, 232);
            this.Auto_polinomial_Radj.Name = "Auto_polinomial_Radj";
            this.Auto_polinomial_Radj.Size = new System.Drawing.Size(80, 20);
            this.Auto_polinomial_Radj.TabIndex = 49;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(606, 235);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(38, 13);
            this.label39.TabIndex = 48;
            this.label39.Text = "Radj =";
            // 
            // Auto_exponential_Radj
            // 
            this.Auto_exponential_Radj.Location = new System.Drawing.Point(650, 132);
            this.Auto_exponential_Radj.Name = "Auto_exponential_Radj";
            this.Auto_exponential_Radj.Size = new System.Drawing.Size(80, 20);
            this.Auto_exponential_Radj.TabIndex = 47;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(606, 135);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(38, 13);
            this.label38.TabIndex = 46;
            this.label38.Text = "Radj =";
            // 
            // Auto_linear_Radj
            // 
            this.Auto_linear_Radj.Location = new System.Drawing.Point(650, 33);
            this.Auto_linear_Radj.Name = "Auto_linear_Radj";
            this.Auto_linear_Radj.Size = new System.Drawing.Size(80, 20);
            this.Auto_linear_Radj.TabIndex = 45;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(606, 36);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(38, 13);
            this.label37.TabIndex = 44;
            this.label37.Text = "Radj =";
            // 
            // polinomial_auto_details
            // 
            this.polinomial_auto_details.Location = new System.Drawing.Point(750, 228);
            this.polinomial_auto_details.Name = "polinomial_auto_details";
            this.polinomial_auto_details.Size = new System.Drawing.Size(85, 26);
            this.polinomial_auto_details.TabIndex = 43;
            this.polinomial_auto_details.Text = "Detail";
            this.polinomial_auto_details.UseVisualStyleBackColor = true;
            this.polinomial_auto_details.Click += new System.EventHandler(this.polinomial_auto_details_Click);
            // 
            // exponential_auto_details
            // 
            this.exponential_auto_details.Location = new System.Drawing.Point(750, 128);
            this.exponential_auto_details.Name = "exponential_auto_details";
            this.exponential_auto_details.Size = new System.Drawing.Size(85, 26);
            this.exponential_auto_details.TabIndex = 42;
            this.exponential_auto_details.Text = "Detail";
            this.exponential_auto_details.UseVisualStyleBackColor = true;
            this.exponential_auto_details.Click += new System.EventHandler(this.exponential_auto_details_Click);
            // 
            // linear_auto_details
            // 
            this.linear_auto_details.Location = new System.Drawing.Point(750, 29);
            this.linear_auto_details.Name = "linear_auto_details";
            this.linear_auto_details.Size = new System.Drawing.Size(85, 26);
            this.linear_auto_details.TabIndex = 41;
            this.linear_auto_details.Text = "Detail";
            this.linear_auto_details.UseVisualStyleBackColor = true;
            this.linear_auto_details.Click += new System.EventHandler(this.linear_auto_details_Click);
            // 
            // R_polinomial
            // 
            this.R_polinomial.Location = new System.Drawing.Point(508, 258);
            this.R_polinomial.Name = "R_polinomial";
            this.R_polinomial.Size = new System.Drawing.Size(80, 20);
            this.R_polinomial.TabIndex = 40;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(473, 261);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "R^2 =";
            // 
            // R_exponential
            // 
            this.R_exponential.Location = new System.Drawing.Point(508, 158);
            this.R_exponential.Name = "R_exponential";
            this.R_exponential.Size = new System.Drawing.Size(80, 20);
            this.R_exponential.TabIndex = 38;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(473, 161);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = "R^2 =";
            // 
            // R_linear
            // 
            this.R_linear.Location = new System.Drawing.Point(508, 59);
            this.R_linear.Name = "R_linear";
            this.R_linear.Size = new System.Drawing.Size(80, 20);
            this.R_linear.TabIndex = 36;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(473, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "R^2 =";
            // 
            // Auto_polinomial_result
            // 
            this.Auto_polinomial_result.Location = new System.Drawing.Point(63, 232);
            this.Auto_polinomial_result.Name = "Auto_polinomial_result";
            this.Auto_polinomial_result.Size = new System.Drawing.Size(390, 70);
            this.Auto_polinomial_result.TabIndex = 34;
            this.Auto_polinomial_result.Text = "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(36, 216);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = "Polinomial";
            // 
            // Auto_polinomial_korrelation
            // 
            this.Auto_polinomial_korrelation.Location = new System.Drawing.Point(508, 232);
            this.Auto_polinomial_korrelation.Name = "Auto_polinomial_korrelation";
            this.Auto_polinomial_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Auto_polinomial_korrelation.TabIndex = 32;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(478, 235);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "kor =";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(36, 235);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "y =";
            // 
            // Auto_exponential_result
            // 
            this.Auto_exponential_result.Location = new System.Drawing.Point(63, 132);
            this.Auto_exponential_result.Name = "Auto_exponential_result";
            this.Auto_exponential_result.Size = new System.Drawing.Size(390, 70);
            this.Auto_exponential_result.TabIndex = 29;
            this.Auto_exponential_result.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 116);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Exponential";
            // 
            // Auto_exponential_korrelation
            // 
            this.Auto_exponential_korrelation.Location = new System.Drawing.Point(508, 132);
            this.Auto_exponential_korrelation.Name = "Auto_exponential_korrelation";
            this.Auto_exponential_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Auto_exponential_korrelation.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(478, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "kor =";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 135);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 25;
            this.label15.Text = "y =";
            // 
            // Auto_linear_result
            // 
            this.Auto_linear_result.Location = new System.Drawing.Point(63, 33);
            this.Auto_linear_result.Name = "Auto_linear_result";
            this.Auto_linear_result.Size = new System.Drawing.Size(390, 70);
            this.Auto_linear_result.TabIndex = 24;
            this.Auto_linear_result.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(36, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Linear";
            // 
            // Auto_linear_korrelation
            // 
            this.Auto_linear_korrelation.Location = new System.Drawing.Point(508, 33);
            this.Auto_linear_korrelation.Name = "Auto_linear_korrelation";
            this.Auto_linear_korrelation.Size = new System.Drawing.Size(80, 20);
            this.Auto_linear_korrelation.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(478, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "kor =";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(36, 36);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "y =";
            // 
            // Linear_Page
            // 
            this.Linear_Page.Controls.Add(this.label61);
            this.Linear_Page.Controls.Add(this.label60);
            this.Linear_Page.Controls.Add(this.Linear_Y_min);
            this.Linear_Page.Controls.Add(this.Linear_Y_max);
            this.Linear_Page.Controls.Add(this.Linear_standart_graph);
            this.Linear_Page.Controls.Add(this.Linear_max_graph);
            this.Linear_Page.Controls.Add(this.Linear_avg_graph);
            this.Linear_Page.Controls.Add(this.Linear_min_graph);
            this.Linear_Page.Controls.Add(this.linear_graph_function);
            this.Linear_Page.Controls.Add(this.Linear_save);
            this.Linear_Page.Controls.Add(this.linear_detail_R);
            this.Linear_Page.Controls.Add(this.label26);
            this.Linear_Page.Controls.Add(this.linear_detail_korrelation);
            this.Linear_Page.Controls.Add(this.label27);
            this.Linear_Page.Controls.Add(this.Linear_Levellnes_draw);
            this.Linear_Page.Controls.Add(this.Linear_Chart_viewer);
            this.Linear_Page.Controls.Add(this.Linear_3Dchart_Draw);
            this.Linear_Page.Controls.Add(this.linear_X2_collection);
            this.Linear_Page.Controls.Add(this.Linear_chart_Draw);
            this.Linear_Page.Controls.Add(this.linear_X1_collection);
            this.Linear_Page.Controls.Add(this.linearChart);
            this.Linear_Page.Location = new System.Drawing.Point(4, 22);
            this.Linear_Page.Name = "Linear_Page";
            this.Linear_Page.Padding = new System.Windows.Forms.Padding(3);
            this.Linear_Page.Size = new System.Drawing.Size(842, 454);
            this.Linear_Page.TabIndex = 1;
            this.Linear_Page.Text = "Linear";
            this.Linear_Page.UseVisualStyleBackColor = true;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(709, 89);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(39, 13);
            this.label61.TabIndex = 50;
            this.label61.Text = "Ymin =";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(706, 63);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(42, 13);
            this.label60.TabIndex = 49;
            this.label60.Text = "Ymax =";
            // 
            // Linear_Y_min
            // 
            this.Linear_Y_min.Location = new System.Drawing.Point(754, 86);
            this.Linear_Y_min.Name = "Linear_Y_min";
            this.Linear_Y_min.Size = new System.Drawing.Size(50, 20);
            this.Linear_Y_min.TabIndex = 48;
            // 
            // Linear_Y_max
            // 
            this.Linear_Y_max.Location = new System.Drawing.Point(754, 60);
            this.Linear_Y_max.Name = "Linear_Y_max";
            this.Linear_Y_max.Size = new System.Drawing.Size(50, 20);
            this.Linear_Y_max.TabIndex = 47;
            // 
            // Linear_standart_graph
            // 
            this.Linear_standart_graph.AutoSize = true;
            this.Linear_standart_graph.Location = new System.Drawing.Point(721, 240);
            this.Linear_standart_graph.Name = "Linear_standart_graph";
            this.Linear_standart_graph.Size = new System.Drawing.Size(65, 17);
            this.Linear_standart_graph.TabIndex = 46;
            this.Linear_standart_graph.TabStop = true;
            this.Linear_standart_graph.Text = "Standart";
            this.Linear_standart_graph.UseVisualStyleBackColor = true;
            // 
            // Linear_max_graph
            // 
            this.Linear_max_graph.AutoSize = true;
            this.Linear_max_graph.Location = new System.Drawing.Point(640, 286);
            this.Linear_max_graph.Name = "Linear_max_graph";
            this.Linear_max_graph.Size = new System.Drawing.Size(45, 17);
            this.Linear_max_graph.TabIndex = 45;
            this.Linear_max_graph.TabStop = true;
            this.Linear_max_graph.Text = "Max";
            this.Linear_max_graph.UseVisualStyleBackColor = true;
            // 
            // Linear_avg_graph
            // 
            this.Linear_avg_graph.AutoSize = true;
            this.Linear_avg_graph.Location = new System.Drawing.Point(640, 263);
            this.Linear_avg_graph.Name = "Linear_avg_graph";
            this.Linear_avg_graph.Size = new System.Drawing.Size(65, 17);
            this.Linear_avg_graph.TabIndex = 44;
            this.Linear_avg_graph.TabStop = true;
            this.Linear_avg_graph.Text = "Average";
            this.Linear_avg_graph.UseVisualStyleBackColor = true;
            // 
            // Linear_min_graph
            // 
            this.Linear_min_graph.AutoSize = true;
            this.Linear_min_graph.Location = new System.Drawing.Point(640, 240);
            this.Linear_min_graph.Name = "Linear_min_graph";
            this.Linear_min_graph.Size = new System.Drawing.Size(42, 17);
            this.Linear_min_graph.TabIndex = 43;
            this.Linear_min_graph.TabStop = true;
            this.Linear_min_graph.Text = "Min";
            this.Linear_min_graph.UseVisualStyleBackColor = true;
            // 
            // linear_graph_function
            // 
            this.linear_graph_function.Location = new System.Drawing.Point(619, 122);
            this.linear_graph_function.Name = "linear_graph_function";
            this.linear_graph_function.Size = new System.Drawing.Size(220, 50);
            this.linear_graph_function.TabIndex = 42;
            this.linear_graph_function.Text = "";
            // 
            // Linear_save
            // 
            this.Linear_save.Location = new System.Drawing.Point(750, 400);
            this.Linear_save.Name = "Linear_save";
            this.Linear_save.Size = new System.Drawing.Size(75, 23);
            this.Linear_save.TabIndex = 41;
            this.Linear_save.Text = "Save";
            this.Linear_save.UseVisualStyleBackColor = true;
            this.Linear_save.Click += new System.EventHandler(this.Linear_save_Click);
            // 
            // linear_detail_R
            // 
            this.linear_detail_R.Location = new System.Drawing.Point(620, 86);
            this.linear_detail_R.Name = "linear_detail_R";
            this.linear_detail_R.Size = new System.Drawing.Size(80, 20);
            this.linear_detail_R.TabIndex = 40;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(578, 89);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 13);
            this.label26.TabIndex = 39;
            this.label26.Text = "R^2 =";
            // 
            // linear_detail_korrelation
            // 
            this.linear_detail_korrelation.Location = new System.Drawing.Point(620, 60);
            this.linear_detail_korrelation.Name = "linear_detail_korrelation";
            this.linear_detail_korrelation.Size = new System.Drawing.Size(80, 20);
            this.linear_detail_korrelation.TabIndex = 38;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(583, 63);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "kor =";
            // 
            // Linear_Levellnes_draw
            // 
            this.Linear_Levellnes_draw.Location = new System.Drawing.Point(721, 349);
            this.Linear_Levellnes_draw.Name = "Linear_Levellnes_draw";
            this.Linear_Levellnes_draw.Size = new System.Drawing.Size(75, 23);
            this.Linear_Levellnes_draw.TabIndex = 16;
            this.Linear_Levellnes_draw.Text = "Level lines";
            this.Linear_Levellnes_draw.UseVisualStyleBackColor = true;
            this.Linear_Levellnes_draw.Click += new System.EventHandler(this.linear_Levellnes_draw_Click);
            // 
            // Linear_Chart_viewer
            // 
            this.Linear_Chart_viewer.Location = new System.Drawing.Point(30, 30);
            this.Linear_Chart_viewer.Name = "Linear_Chart_viewer";
            this.Linear_Chart_viewer.Size = new System.Drawing.Size(300, 200);
            this.Linear_Chart_viewer.TabIndex = 15;
            this.Linear_Chart_viewer.TabStop = false;
            // 
            // Linear_3Dchart_Draw
            // 
            this.Linear_3Dchart_Draw.Location = new System.Drawing.Point(721, 320);
            this.Linear_3Dchart_Draw.Name = "Linear_3Dchart_Draw";
            this.Linear_3Dchart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Linear_3Dchart_Draw.TabIndex = 14;
            this.Linear_3Dchart_Draw.Text = "Surface";
            this.Linear_3Dchart_Draw.UseVisualStyleBackColor = true;
            this.Linear_3Dchart_Draw.Click += new System.EventHandler(this.Linear_3Dchart_Draw_Click);
            // 
            // linear_X2_collection
            // 
            this.linear_X2_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.linear_X2_collection.FormattingEnabled = true;
            this.linear_X2_collection.Location = new System.Drawing.Point(721, 203);
            this.linear_X2_collection.Name = "linear_X2_collection";
            this.linear_X2_collection.Size = new System.Drawing.Size(48, 21);
            this.linear_X2_collection.TabIndex = 13;
            // 
            // Linear_chart_Draw
            // 
            this.Linear_chart_Draw.Location = new System.Drawing.Point(640, 320);
            this.Linear_chart_Draw.Name = "Linear_chart_Draw";
            this.Linear_chart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Linear_chart_Draw.TabIndex = 12;
            this.Linear_chart_Draw.Text = "Line";
            this.Linear_chart_Draw.UseVisualStyleBackColor = true;
            this.Linear_chart_Draw.Click += new System.EventHandler(this.Linear_chart_Draw_Click);
            // 
            // linear_X1_collection
            // 
            this.linear_X1_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.linear_X1_collection.FormattingEnabled = true;
            this.linear_X1_collection.Location = new System.Drawing.Point(667, 203);
            this.linear_X1_collection.Name = "linear_X1_collection";
            this.linear_X1_collection.Size = new System.Drawing.Size(48, 21);
            this.linear_X1_collection.TabIndex = 11;
            // 
            // linearChart
            // 
            this.linearChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea1.Name = "ChartArea1";
            this.linearChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.linearChart.Legends.Add(legend1);
            this.linearChart.Location = new System.Drawing.Point(30, 30);
            this.linearChart.Name = "linearChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.Legend = "Legend1";
            series1.Name = "Data";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Regression";
            this.linearChart.Series.Add(series1);
            this.linearChart.Series.Add(series2);
            this.linearChart.Size = new System.Drawing.Size(560, 300);
            this.linearChart.TabIndex = 0;
            this.linearChart.Text = "chart1";
            this.linearChart.Visible = false;
            // 
            // Exponential_Page
            // 
            this.Exponential_Page.Controls.Add(this.label62);
            this.Exponential_Page.Controls.Add(this.label73);
            this.Exponential_Page.Controls.Add(this.Exponential_Y_min);
            this.Exponential_Page.Controls.Add(this.Exponential_Y_max);
            this.Exponential_Page.Controls.Add(this.Exponential_standart_graph);
            this.Exponential_Page.Controls.Add(this.Exponential_max_graph);
            this.Exponential_Page.Controls.Add(this.Exponential_avg_graph);
            this.Exponential_Page.Controls.Add(this.Exponential_min_graph);
            this.Exponential_Page.Controls.Add(this.exponential_grahp_function);
            this.Exponential_Page.Controls.Add(this.Exponential_save);
            this.Exponential_Page.Controls.Add(this.Exponential_levellines_draw);
            this.Exponential_Page.Controls.Add(this.exponential_detail_R);
            this.Exponential_Page.Controls.Add(this.label25);
            this.Exponential_Page.Controls.Add(this.exponential_detail_korrelation);
            this.Exponential_Page.Controls.Add(this.label28);
            this.Exponential_Page.Controls.Add(this.Exponential_Chart_viewer);
            this.Exponential_Page.Controls.Add(this.Exponential_3Dchart_Draw);
            this.Exponential_Page.Controls.Add(this.exponential_X2_collection);
            this.Exponential_Page.Controls.Add(this.Exponential_chart_Draw);
            this.Exponential_Page.Controls.Add(this.exponential_X1_collection);
            this.Exponential_Page.Controls.Add(this.exponentialChart);
            this.Exponential_Page.Location = new System.Drawing.Point(4, 22);
            this.Exponential_Page.Name = "Exponential_Page";
            this.Exponential_Page.Size = new System.Drawing.Size(842, 454);
            this.Exponential_Page.TabIndex = 6;
            this.Exponential_Page.Text = "Exponential";
            this.Exponential_Page.UseVisualStyleBackColor = true;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(709, 89);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(39, 13);
            this.label62.TabIndex = 54;
            this.label62.Text = "Ymin =";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(706, 63);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(42, 13);
            this.label73.TabIndex = 53;
            this.label73.Text = "Ymax =";
            // 
            // Exponential_Y_min
            // 
            this.Exponential_Y_min.Location = new System.Drawing.Point(754, 86);
            this.Exponential_Y_min.Name = "Exponential_Y_min";
            this.Exponential_Y_min.Size = new System.Drawing.Size(50, 20);
            this.Exponential_Y_min.TabIndex = 52;
            // 
            // Exponential_Y_max
            // 
            this.Exponential_Y_max.Location = new System.Drawing.Point(754, 60);
            this.Exponential_Y_max.Name = "Exponential_Y_max";
            this.Exponential_Y_max.Size = new System.Drawing.Size(50, 20);
            this.Exponential_Y_max.TabIndex = 51;
            // 
            // Exponential_standart_graph
            // 
            this.Exponential_standart_graph.AutoSize = true;
            this.Exponential_standart_graph.Location = new System.Drawing.Point(721, 240);
            this.Exponential_standart_graph.Name = "Exponential_standart_graph";
            this.Exponential_standart_graph.Size = new System.Drawing.Size(65, 17);
            this.Exponential_standart_graph.TabIndex = 50;
            this.Exponential_standart_graph.TabStop = true;
            this.Exponential_standart_graph.Text = "Standart";
            this.Exponential_standart_graph.UseVisualStyleBackColor = true;
            // 
            // Exponential_max_graph
            // 
            this.Exponential_max_graph.AutoSize = true;
            this.Exponential_max_graph.Location = new System.Drawing.Point(640, 286);
            this.Exponential_max_graph.Name = "Exponential_max_graph";
            this.Exponential_max_graph.Size = new System.Drawing.Size(45, 17);
            this.Exponential_max_graph.TabIndex = 49;
            this.Exponential_max_graph.TabStop = true;
            this.Exponential_max_graph.Text = "Max";
            this.Exponential_max_graph.UseVisualStyleBackColor = true;
            // 
            // Exponential_avg_graph
            // 
            this.Exponential_avg_graph.AutoSize = true;
            this.Exponential_avg_graph.Location = new System.Drawing.Point(640, 263);
            this.Exponential_avg_graph.Name = "Exponential_avg_graph";
            this.Exponential_avg_graph.Size = new System.Drawing.Size(65, 17);
            this.Exponential_avg_graph.TabIndex = 48;
            this.Exponential_avg_graph.TabStop = true;
            this.Exponential_avg_graph.Text = "Average";
            this.Exponential_avg_graph.UseVisualStyleBackColor = true;
            // 
            // Exponential_min_graph
            // 
            this.Exponential_min_graph.AutoSize = true;
            this.Exponential_min_graph.Location = new System.Drawing.Point(640, 240);
            this.Exponential_min_graph.Name = "Exponential_min_graph";
            this.Exponential_min_graph.Size = new System.Drawing.Size(42, 17);
            this.Exponential_min_graph.TabIndex = 47;
            this.Exponential_min_graph.TabStop = true;
            this.Exponential_min_graph.Text = "Min";
            this.Exponential_min_graph.UseVisualStyleBackColor = true;
            // 
            // exponential_grahp_function
            // 
            this.exponential_grahp_function.Location = new System.Drawing.Point(619, 122);
            this.exponential_grahp_function.Name = "exponential_grahp_function";
            this.exponential_grahp_function.Size = new System.Drawing.Size(220, 50);
            this.exponential_grahp_function.TabIndex = 43;
            this.exponential_grahp_function.Text = "";
            // 
            // Exponential_save
            // 
            this.Exponential_save.Location = new System.Drawing.Point(750, 400);
            this.Exponential_save.Name = "Exponential_save";
            this.Exponential_save.Size = new System.Drawing.Size(75, 23);
            this.Exponential_save.TabIndex = 42;
            this.Exponential_save.Text = "Save";
            this.Exponential_save.UseVisualStyleBackColor = true;
            this.Exponential_save.Click += new System.EventHandler(this.Exponential_save_Click);
            // 
            // Exponential_levellines_draw
            // 
            this.Exponential_levellines_draw.Location = new System.Drawing.Point(721, 349);
            this.Exponential_levellines_draw.Name = "Exponential_levellines_draw";
            this.Exponential_levellines_draw.Size = new System.Drawing.Size(75, 23);
            this.Exponential_levellines_draw.TabIndex = 41;
            this.Exponential_levellines_draw.Text = "Level lines";
            this.Exponential_levellines_draw.UseVisualStyleBackColor = true;
            this.Exponential_levellines_draw.Click += new System.EventHandler(this.Exponential_levellines_draw_Click);
            // 
            // exponential_detail_R
            // 
            this.exponential_detail_R.Location = new System.Drawing.Point(620, 86);
            this.exponential_detail_R.Name = "exponential_detail_R";
            this.exponential_detail_R.Size = new System.Drawing.Size(80, 20);
            this.exponential_detail_R.TabIndex = 40;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(578, 89);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(36, 13);
            this.label25.TabIndex = 39;
            this.label25.Text = "R^2 =";
            // 
            // exponential_detail_korrelation
            // 
            this.exponential_detail_korrelation.Location = new System.Drawing.Point(620, 60);
            this.exponential_detail_korrelation.Name = "exponential_detail_korrelation";
            this.exponential_detail_korrelation.Size = new System.Drawing.Size(80, 20);
            this.exponential_detail_korrelation.TabIndex = 38;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(583, 63);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(31, 13);
            this.label28.TabIndex = 37;
            this.label28.Text = "kor =";
            // 
            // Exponential_Chart_viewer
            // 
            this.Exponential_Chart_viewer.Location = new System.Drawing.Point(30, 30);
            this.Exponential_Chart_viewer.Name = "Exponential_Chart_viewer";
            this.Exponential_Chart_viewer.Size = new System.Drawing.Size(300, 200);
            this.Exponential_Chart_viewer.TabIndex = 20;
            this.Exponential_Chart_viewer.TabStop = false;
            // 
            // Exponential_3Dchart_Draw
            // 
            this.Exponential_3Dchart_Draw.Location = new System.Drawing.Point(721, 320);
            this.Exponential_3Dchart_Draw.Name = "Exponential_3Dchart_Draw";
            this.Exponential_3Dchart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Exponential_3Dchart_Draw.TabIndex = 19;
            this.Exponential_3Dchart_Draw.Text = "Surface";
            this.Exponential_3Dchart_Draw.UseVisualStyleBackColor = true;
            this.Exponential_3Dchart_Draw.Click += new System.EventHandler(this.Exponential_3Dchart_Draw_Click);
            // 
            // exponential_X2_collection
            // 
            this.exponential_X2_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.exponential_X2_collection.FormattingEnabled = true;
            this.exponential_X2_collection.Location = new System.Drawing.Point(721, 203);
            this.exponential_X2_collection.Name = "exponential_X2_collection";
            this.exponential_X2_collection.Size = new System.Drawing.Size(48, 21);
            this.exponential_X2_collection.TabIndex = 18;
            // 
            // Exponential_chart_Draw
            // 
            this.Exponential_chart_Draw.Location = new System.Drawing.Point(640, 320);
            this.Exponential_chart_Draw.Name = "Exponential_chart_Draw";
            this.Exponential_chart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Exponential_chart_Draw.TabIndex = 17;
            this.Exponential_chart_Draw.Text = "Line";
            this.Exponential_chart_Draw.UseVisualStyleBackColor = true;
            this.Exponential_chart_Draw.Click += new System.EventHandler(this.Exponential_chart_Draw_Click);
            // 
            // exponential_X1_collection
            // 
            this.exponential_X1_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.exponential_X1_collection.FormattingEnabled = true;
            this.exponential_X1_collection.Location = new System.Drawing.Point(667, 203);
            this.exponential_X1_collection.Name = "exponential_X1_collection";
            this.exponential_X1_collection.Size = new System.Drawing.Size(48, 21);
            this.exponential_X1_collection.TabIndex = 16;
            // 
            // exponentialChart
            // 
            this.exponentialChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea2.Name = "ChartArea1";
            this.exponentialChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.exponentialChart.Legends.Add(legend2);
            this.exponentialChart.Location = new System.Drawing.Point(30, 30);
            this.exponentialChart.Name = "exponentialChart";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series3.Legend = "Legend1";
            series3.Name = "Data";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Legend = "Legend1";
            series4.Name = "Regression";
            this.exponentialChart.Series.Add(series3);
            this.exponentialChart.Series.Add(series4);
            this.exponentialChart.Size = new System.Drawing.Size(560, 300);
            this.exponentialChart.TabIndex = 13;
            this.exponentialChart.Text = "chart1";
            this.exponentialChart.Visible = false;
            // 
            // PolinomialAuto_Page
            // 
            this.PolinomialAuto_Page.Controls.Add(this.label74);
            this.PolinomialAuto_Page.Controls.Add(this.label75);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_Y_min);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_Y_max);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_standart_graph);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_max_graph);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_avg_graph);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_min_graph);
            this.PolinomialAuto_Page.Controls.Add(this.polinomialAuto_grahp_function);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_save);
            this.PolinomialAuto_Page.Controls.Add(this.polinomialAutoChart);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_levellines_draw);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_3Dchart_Draw);
            this.PolinomialAuto_Page.Controls.Add(this.polinomialAuto_X2_collection);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_chart_Draw);
            this.PolinomialAuto_Page.Controls.Add(this.polinomialAuto_X1_collection);
            this.PolinomialAuto_Page.Controls.Add(this.PolinomialAuto_Chart_viewer);
            this.PolinomialAuto_Page.Controls.Add(this.polinomialAuto_detail_R);
            this.PolinomialAuto_Page.Controls.Add(this.label29);
            this.PolinomialAuto_Page.Controls.Add(this.polinomialAuto_detail_korrelation);
            this.PolinomialAuto_Page.Controls.Add(this.label30);
            this.PolinomialAuto_Page.Location = new System.Drawing.Point(4, 22);
            this.PolinomialAuto_Page.Name = "PolinomialAuto_Page";
            this.PolinomialAuto_Page.Size = new System.Drawing.Size(842, 454);
            this.PolinomialAuto_Page.TabIndex = 7;
            this.PolinomialAuto_Page.Text = "Polinomial Auto";
            this.PolinomialAuto_Page.UseVisualStyleBackColor = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(709, 89);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(39, 13);
            this.label74.TabIndex = 66;
            this.label74.Text = "Ymin =";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(706, 63);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(42, 13);
            this.label75.TabIndex = 65;
            this.label75.Text = "Ymax =";
            // 
            // PolinomialAuto_Y_min
            // 
            this.PolinomialAuto_Y_min.Location = new System.Drawing.Point(754, 86);
            this.PolinomialAuto_Y_min.Name = "PolinomialAuto_Y_min";
            this.PolinomialAuto_Y_min.Size = new System.Drawing.Size(50, 20);
            this.PolinomialAuto_Y_min.TabIndex = 64;
            // 
            // PolinomialAuto_Y_max
            // 
            this.PolinomialAuto_Y_max.Location = new System.Drawing.Point(754, 60);
            this.PolinomialAuto_Y_max.Name = "PolinomialAuto_Y_max";
            this.PolinomialAuto_Y_max.Size = new System.Drawing.Size(50, 20);
            this.PolinomialAuto_Y_max.TabIndex = 63;
            // 
            // PolinomialAuto_standart_graph
            // 
            this.PolinomialAuto_standart_graph.AutoSize = true;
            this.PolinomialAuto_standart_graph.Location = new System.Drawing.Point(721, 240);
            this.PolinomialAuto_standart_graph.Name = "PolinomialAuto_standart_graph";
            this.PolinomialAuto_standart_graph.Size = new System.Drawing.Size(65, 17);
            this.PolinomialAuto_standart_graph.TabIndex = 62;
            this.PolinomialAuto_standart_graph.TabStop = true;
            this.PolinomialAuto_standart_graph.Text = "Standart";
            this.PolinomialAuto_standart_graph.UseVisualStyleBackColor = true;
            // 
            // PolinomialAuto_max_graph
            // 
            this.PolinomialAuto_max_graph.AutoSize = true;
            this.PolinomialAuto_max_graph.Location = new System.Drawing.Point(640, 286);
            this.PolinomialAuto_max_graph.Name = "PolinomialAuto_max_graph";
            this.PolinomialAuto_max_graph.Size = new System.Drawing.Size(45, 17);
            this.PolinomialAuto_max_graph.TabIndex = 61;
            this.PolinomialAuto_max_graph.TabStop = true;
            this.PolinomialAuto_max_graph.Text = "Max";
            this.PolinomialAuto_max_graph.UseVisualStyleBackColor = true;
            // 
            // PolinomialAuto_avg_graph
            // 
            this.PolinomialAuto_avg_graph.AutoSize = true;
            this.PolinomialAuto_avg_graph.Location = new System.Drawing.Point(640, 263);
            this.PolinomialAuto_avg_graph.Name = "PolinomialAuto_avg_graph";
            this.PolinomialAuto_avg_graph.Size = new System.Drawing.Size(65, 17);
            this.PolinomialAuto_avg_graph.TabIndex = 60;
            this.PolinomialAuto_avg_graph.TabStop = true;
            this.PolinomialAuto_avg_graph.Text = "Average";
            this.PolinomialAuto_avg_graph.UseVisualStyleBackColor = true;
            // 
            // PolinomialAuto_min_graph
            // 
            this.PolinomialAuto_min_graph.AutoSize = true;
            this.PolinomialAuto_min_graph.Location = new System.Drawing.Point(640, 240);
            this.PolinomialAuto_min_graph.Name = "PolinomialAuto_min_graph";
            this.PolinomialAuto_min_graph.Size = new System.Drawing.Size(42, 17);
            this.PolinomialAuto_min_graph.TabIndex = 59;
            this.PolinomialAuto_min_graph.TabStop = true;
            this.PolinomialAuto_min_graph.Text = "Min";
            this.PolinomialAuto_min_graph.UseVisualStyleBackColor = true;
            // 
            // polinomialAuto_grahp_function
            // 
            this.polinomialAuto_grahp_function.Location = new System.Drawing.Point(619, 122);
            this.polinomialAuto_grahp_function.Name = "polinomialAuto_grahp_function";
            this.polinomialAuto_grahp_function.Size = new System.Drawing.Size(220, 50);
            this.polinomialAuto_grahp_function.TabIndex = 49;
            this.polinomialAuto_grahp_function.Text = "";
            // 
            // PolinomialAuto_save
            // 
            this.PolinomialAuto_save.Location = new System.Drawing.Point(750, 400);
            this.PolinomialAuto_save.Name = "PolinomialAuto_save";
            this.PolinomialAuto_save.Size = new System.Drawing.Size(75, 23);
            this.PolinomialAuto_save.TabIndex = 48;
            this.PolinomialAuto_save.Text = "Save";
            this.PolinomialAuto_save.UseVisualStyleBackColor = true;
            this.PolinomialAuto_save.Click += new System.EventHandler(this.Polinomial_save_Click);
            // 
            // polinomialAutoChart
            // 
            this.polinomialAutoChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea3.Name = "ChartArea1";
            this.polinomialAutoChart.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.polinomialAutoChart.Legends.Add(legend3);
            this.polinomialAutoChart.Location = new System.Drawing.Point(30, 30);
            this.polinomialAutoChart.Name = "polinomialAutoChart";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series5.Legend = "Legend1";
            series5.Name = "Data";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Legend = "Legend1";
            series6.Name = "Regression";
            this.polinomialAutoChart.Series.Add(series5);
            this.polinomialAutoChart.Series.Add(series6);
            this.polinomialAutoChart.Size = new System.Drawing.Size(560, 300);
            this.polinomialAutoChart.TabIndex = 47;
            this.polinomialAutoChart.Text = "chart1";
            this.polinomialAutoChart.Visible = false;
            // 
            // PolinomialAuto_levellines_draw
            // 
            this.PolinomialAuto_levellines_draw.Location = new System.Drawing.Point(721, 349);
            this.PolinomialAuto_levellines_draw.Name = "PolinomialAuto_levellines_draw";
            this.PolinomialAuto_levellines_draw.Size = new System.Drawing.Size(75, 23);
            this.PolinomialAuto_levellines_draw.TabIndex = 46;
            this.PolinomialAuto_levellines_draw.Text = "Level lines";
            this.PolinomialAuto_levellines_draw.UseVisualStyleBackColor = true;
            this.PolinomialAuto_levellines_draw.Click += new System.EventHandler(this.Polinomial_levellines_draw_Click);
            // 
            // PolinomialAuto_3Dchart_Draw
            // 
            this.PolinomialAuto_3Dchart_Draw.Location = new System.Drawing.Point(721, 320);
            this.PolinomialAuto_3Dchart_Draw.Name = "PolinomialAuto_3Dchart_Draw";
            this.PolinomialAuto_3Dchart_Draw.Size = new System.Drawing.Size(75, 23);
            this.PolinomialAuto_3Dchart_Draw.TabIndex = 45;
            this.PolinomialAuto_3Dchart_Draw.Text = "Surface";
            this.PolinomialAuto_3Dchart_Draw.UseVisualStyleBackColor = true;
            this.PolinomialAuto_3Dchart_Draw.Click += new System.EventHandler(this.Polinomial_3Dchart_Draw_Click);
            // 
            // polinomialAuto_X2_collection
            // 
            this.polinomialAuto_X2_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.polinomialAuto_X2_collection.FormattingEnabled = true;
            this.polinomialAuto_X2_collection.Location = new System.Drawing.Point(721, 203);
            this.polinomialAuto_X2_collection.Name = "polinomialAuto_X2_collection";
            this.polinomialAuto_X2_collection.Size = new System.Drawing.Size(48, 21);
            this.polinomialAuto_X2_collection.TabIndex = 44;
            // 
            // PolinomialAuto_chart_Draw
            // 
            this.PolinomialAuto_chart_Draw.Location = new System.Drawing.Point(640, 320);
            this.PolinomialAuto_chart_Draw.Name = "PolinomialAuto_chart_Draw";
            this.PolinomialAuto_chart_Draw.Size = new System.Drawing.Size(75, 23);
            this.PolinomialAuto_chart_Draw.TabIndex = 43;
            this.PolinomialAuto_chart_Draw.Text = "Line";
            this.PolinomialAuto_chart_Draw.UseVisualStyleBackColor = true;
            this.PolinomialAuto_chart_Draw.Click += new System.EventHandler(this.Polinomial_chart_Draw_Click);
            // 
            // polinomialAuto_X1_collection
            // 
            this.polinomialAuto_X1_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.polinomialAuto_X1_collection.FormattingEnabled = true;
            this.polinomialAuto_X1_collection.Location = new System.Drawing.Point(667, 203);
            this.polinomialAuto_X1_collection.Name = "polinomialAuto_X1_collection";
            this.polinomialAuto_X1_collection.Size = new System.Drawing.Size(48, 21);
            this.polinomialAuto_X1_collection.TabIndex = 42;
            // 
            // PolinomialAuto_Chart_viewer
            // 
            this.PolinomialAuto_Chart_viewer.Location = new System.Drawing.Point(30, 30);
            this.PolinomialAuto_Chart_viewer.Name = "PolinomialAuto_Chart_viewer";
            this.PolinomialAuto_Chart_viewer.Size = new System.Drawing.Size(300, 200);
            this.PolinomialAuto_Chart_viewer.TabIndex = 41;
            this.PolinomialAuto_Chart_viewer.TabStop = false;
            // 
            // polinomialAuto_detail_R
            // 
            this.polinomialAuto_detail_R.Location = new System.Drawing.Point(620, 86);
            this.polinomialAuto_detail_R.Name = "polinomialAuto_detail_R";
            this.polinomialAuto_detail_R.Size = new System.Drawing.Size(80, 20);
            this.polinomialAuto_detail_R.TabIndex = 40;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(578, 89);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(36, 13);
            this.label29.TabIndex = 39;
            this.label29.Text = "R^2 =";
            // 
            // polinomialAuto_detail_korrelation
            // 
            this.polinomialAuto_detail_korrelation.Location = new System.Drawing.Point(620, 60);
            this.polinomialAuto_detail_korrelation.Name = "polinomialAuto_detail_korrelation";
            this.polinomialAuto_detail_korrelation.Size = new System.Drawing.Size(80, 20);
            this.polinomialAuto_detail_korrelation.TabIndex = 38;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(583, 63);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(31, 13);
            this.label30.TabIndex = 37;
            this.label30.Text = "kor =";
            // 
            // Degree_Page
            // 
            this.Degree_Page.Controls.Add(this.label76);
            this.Degree_Page.Controls.Add(this.label77);
            this.Degree_Page.Controls.Add(this.Degree_Y_min);
            this.Degree_Page.Controls.Add(this.Degree_Y_max);
            this.Degree_Page.Controls.Add(this.Degree_standart_graph);
            this.Degree_Page.Controls.Add(this.Degree_max_graph);
            this.Degree_Page.Controls.Add(this.Degree_avg_graph);
            this.Degree_Page.Controls.Add(this.Degree_min_graph);
            this.Degree_Page.Controls.Add(this.degree_grahp_function);
            this.Degree_Page.Controls.Add(this.Degree_save);
            this.Degree_Page.Controls.Add(this.Degree_levellines_draw);
            this.Degree_Page.Controls.Add(this.Degree_3Dchart_Draw);
            this.Degree_Page.Controls.Add(this.degree_X2_collection);
            this.Degree_Page.Controls.Add(this.Degree_chart_Draw);
            this.Degree_Page.Controls.Add(this.degree_X1_collection);
            this.Degree_Page.Controls.Add(this.Degree_Chart_viewer);
            this.Degree_Page.Controls.Add(this.degree_detail_R);
            this.Degree_Page.Controls.Add(this.label31);
            this.Degree_Page.Controls.Add(this.degree_detail_korrelation);
            this.Degree_Page.Controls.Add(this.label32);
            this.Degree_Page.Controls.Add(this.degreeChart);
            this.Degree_Page.Location = new System.Drawing.Point(4, 22);
            this.Degree_Page.Name = "Degree_Page";
            this.Degree_Page.Size = new System.Drawing.Size(842, 454);
            this.Degree_Page.TabIndex = 2;
            this.Degree_Page.Text = "Degree";
            this.Degree_Page.UseVisualStyleBackColor = true;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(709, 89);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(39, 13);
            this.label76.TabIndex = 70;
            this.label76.Text = "Ymin =";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(706, 63);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(42, 13);
            this.label77.TabIndex = 69;
            this.label77.Text = "Ymax =";
            // 
            // Degree_Y_min
            // 
            this.Degree_Y_min.Location = new System.Drawing.Point(754, 86);
            this.Degree_Y_min.Name = "Degree_Y_min";
            this.Degree_Y_min.Size = new System.Drawing.Size(50, 20);
            this.Degree_Y_min.TabIndex = 68;
            // 
            // Degree_Y_max
            // 
            this.Degree_Y_max.Location = new System.Drawing.Point(754, 60);
            this.Degree_Y_max.Name = "Degree_Y_max";
            this.Degree_Y_max.Size = new System.Drawing.Size(50, 20);
            this.Degree_Y_max.TabIndex = 67;
            // 
            // Degree_standart_graph
            // 
            this.Degree_standart_graph.AutoSize = true;
            this.Degree_standart_graph.Location = new System.Drawing.Point(721, 240);
            this.Degree_standart_graph.Name = "Degree_standart_graph";
            this.Degree_standart_graph.Size = new System.Drawing.Size(65, 17);
            this.Degree_standart_graph.TabIndex = 58;
            this.Degree_standart_graph.TabStop = true;
            this.Degree_standart_graph.Text = "Standart";
            this.Degree_standart_graph.UseVisualStyleBackColor = true;
            // 
            // Degree_max_graph
            // 
            this.Degree_max_graph.AutoSize = true;
            this.Degree_max_graph.Location = new System.Drawing.Point(640, 286);
            this.Degree_max_graph.Name = "Degree_max_graph";
            this.Degree_max_graph.Size = new System.Drawing.Size(45, 17);
            this.Degree_max_graph.TabIndex = 57;
            this.Degree_max_graph.TabStop = true;
            this.Degree_max_graph.Text = "Max";
            this.Degree_max_graph.UseVisualStyleBackColor = true;
            // 
            // Degree_avg_graph
            // 
            this.Degree_avg_graph.AutoSize = true;
            this.Degree_avg_graph.Location = new System.Drawing.Point(640, 263);
            this.Degree_avg_graph.Name = "Degree_avg_graph";
            this.Degree_avg_graph.Size = new System.Drawing.Size(65, 17);
            this.Degree_avg_graph.TabIndex = 56;
            this.Degree_avg_graph.TabStop = true;
            this.Degree_avg_graph.Text = "Average";
            this.Degree_avg_graph.UseVisualStyleBackColor = true;
            // 
            // Degree_min_graph
            // 
            this.Degree_min_graph.AutoSize = true;
            this.Degree_min_graph.Location = new System.Drawing.Point(640, 240);
            this.Degree_min_graph.Name = "Degree_min_graph";
            this.Degree_min_graph.Size = new System.Drawing.Size(42, 17);
            this.Degree_min_graph.TabIndex = 55;
            this.Degree_min_graph.TabStop = true;
            this.Degree_min_graph.Text = "Min";
            this.Degree_min_graph.UseVisualStyleBackColor = true;
            // 
            // degree_grahp_function
            // 
            this.degree_grahp_function.Location = new System.Drawing.Point(619, 122);
            this.degree_grahp_function.Name = "degree_grahp_function";
            this.degree_grahp_function.Size = new System.Drawing.Size(220, 50);
            this.degree_grahp_function.TabIndex = 54;
            this.degree_grahp_function.Text = "";
            // 
            // Degree_save
            // 
            this.Degree_save.Location = new System.Drawing.Point(750, 400);
            this.Degree_save.Name = "Degree_save";
            this.Degree_save.Size = new System.Drawing.Size(75, 23);
            this.Degree_save.TabIndex = 52;
            this.Degree_save.Text = "Save";
            this.Degree_save.UseVisualStyleBackColor = true;
            this.Degree_save.Click += new System.EventHandler(this.Degree_save_Click);
            // 
            // Degree_levellines_draw
            // 
            this.Degree_levellines_draw.Location = new System.Drawing.Point(721, 349);
            this.Degree_levellines_draw.Name = "Degree_levellines_draw";
            this.Degree_levellines_draw.Size = new System.Drawing.Size(75, 23);
            this.Degree_levellines_draw.TabIndex = 51;
            this.Degree_levellines_draw.Text = "Level lines";
            this.Degree_levellines_draw.UseVisualStyleBackColor = true;
            this.Degree_levellines_draw.Click += new System.EventHandler(this.Degree_levellines_draw_Click);
            // 
            // Degree_3Dchart_Draw
            // 
            this.Degree_3Dchart_Draw.Location = new System.Drawing.Point(721, 320);
            this.Degree_3Dchart_Draw.Name = "Degree_3Dchart_Draw";
            this.Degree_3Dchart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Degree_3Dchart_Draw.TabIndex = 50;
            this.Degree_3Dchart_Draw.Text = "Surface";
            this.Degree_3Dchart_Draw.UseVisualStyleBackColor = true;
            this.Degree_3Dchart_Draw.Click += new System.EventHandler(this.Degree_3Dchart_Draw_Click);
            // 
            // degree_X2_collection
            // 
            this.degree_X2_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.degree_X2_collection.FormattingEnabled = true;
            this.degree_X2_collection.Location = new System.Drawing.Point(721, 203);
            this.degree_X2_collection.Name = "degree_X2_collection";
            this.degree_X2_collection.Size = new System.Drawing.Size(48, 21);
            this.degree_X2_collection.TabIndex = 49;
            // 
            // Degree_chart_Draw
            // 
            this.Degree_chart_Draw.Location = new System.Drawing.Point(640, 320);
            this.Degree_chart_Draw.Name = "Degree_chart_Draw";
            this.Degree_chart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Degree_chart_Draw.TabIndex = 48;
            this.Degree_chart_Draw.Text = "Line";
            this.Degree_chart_Draw.UseVisualStyleBackColor = true;
            this.Degree_chart_Draw.Click += new System.EventHandler(this.Degree_chart_Draw_Click);
            // 
            // degree_X1_collection
            // 
            this.degree_X1_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.degree_X1_collection.FormattingEnabled = true;
            this.degree_X1_collection.Location = new System.Drawing.Point(667, 203);
            this.degree_X1_collection.Name = "degree_X1_collection";
            this.degree_X1_collection.Size = new System.Drawing.Size(48, 21);
            this.degree_X1_collection.TabIndex = 47;
            // 
            // Degree_Chart_viewer
            // 
            this.Degree_Chart_viewer.Location = new System.Drawing.Point(30, 30);
            this.Degree_Chart_viewer.Name = "Degree_Chart_viewer";
            this.Degree_Chart_viewer.Size = new System.Drawing.Size(300, 200);
            this.Degree_Chart_viewer.TabIndex = 42;
            this.Degree_Chart_viewer.TabStop = false;
            // 
            // degree_detail_R
            // 
            this.degree_detail_R.Location = new System.Drawing.Point(620, 86);
            this.degree_detail_R.Name = "degree_detail_R";
            this.degree_detail_R.Size = new System.Drawing.Size(80, 20);
            this.degree_detail_R.TabIndex = 40;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(578, 89);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 13);
            this.label31.TabIndex = 39;
            this.label31.Text = "R^2 =";
            // 
            // degree_detail_korrelation
            // 
            this.degree_detail_korrelation.Location = new System.Drawing.Point(620, 60);
            this.degree_detail_korrelation.Name = "degree_detail_korrelation";
            this.degree_detail_korrelation.Size = new System.Drawing.Size(80, 20);
            this.degree_detail_korrelation.TabIndex = 38;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(583, 63);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(31, 13);
            this.label32.TabIndex = 37;
            this.label32.Text = "kor =";
            // 
            // degreeChart
            // 
            chartArea4.Name = "ChartArea1";
            this.degreeChart.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.degreeChart.Legends.Add(legend4);
            this.degreeChart.Location = new System.Drawing.Point(30, 30);
            this.degreeChart.Name = "degreeChart";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series7.Legend = "Legend1";
            series7.Name = "Data";
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series8.Legend = "Legend1";
            series8.Name = "Regression";
            this.degreeChart.Series.Add(series7);
            this.degreeChart.Series.Add(series8);
            this.degreeChart.Size = new System.Drawing.Size(560, 300);
            this.degreeChart.TabIndex = 0;
            this.degreeChart.Text = "chart1";
            this.degreeChart.Visible = false;
            // 
            // Giperbola_Page
            // 
            this.Giperbola_Page.Controls.Add(this.label78);
            this.Giperbola_Page.Controls.Add(this.label79);
            this.Giperbola_Page.Controls.Add(this.Giperbola_Y_min);
            this.Giperbola_Page.Controls.Add(this.Giperbola_Y_max);
            this.Giperbola_Page.Controls.Add(this.Giperbola_standart_graph);
            this.Giperbola_Page.Controls.Add(this.Giperbola_max_graph);
            this.Giperbola_Page.Controls.Add(this.Giperbola_avg_graph);
            this.Giperbola_Page.Controls.Add(this.Giperbola_min_graph);
            this.Giperbola_Page.Controls.Add(this.giperbola_grahp_function);
            this.Giperbola_Page.Controls.Add(this.Giperbola_save);
            this.Giperbola_Page.Controls.Add(this.Giperbola_levellines_draw);
            this.Giperbola_Page.Controls.Add(this.Giperbola_3Dchart_Draw);
            this.Giperbola_Page.Controls.Add(this.giperbola_X2_collection);
            this.Giperbola_Page.Controls.Add(this.Giperbola_chart_Draw);
            this.Giperbola_Page.Controls.Add(this.giperbola_X1_collection);
            this.Giperbola_Page.Controls.Add(this.Giperbola_Chart_viewer);
            this.Giperbola_Page.Controls.Add(this.giperbola_detail_R);
            this.Giperbola_Page.Controls.Add(this.label33);
            this.Giperbola_Page.Controls.Add(this.giperbola_detail_korrelation);
            this.Giperbola_Page.Controls.Add(this.label34);
            this.Giperbola_Page.Controls.Add(this.giperbolaChart);
            this.Giperbola_Page.Location = new System.Drawing.Point(4, 22);
            this.Giperbola_Page.Name = "Giperbola_Page";
            this.Giperbola_Page.Size = new System.Drawing.Size(842, 454);
            this.Giperbola_Page.TabIndex = 3;
            this.Giperbola_Page.Text = "Giperbola";
            this.Giperbola_Page.UseVisualStyleBackColor = true;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(709, 89);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(39, 13);
            this.label78.TabIndex = 74;
            this.label78.Text = "Ymin =";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(706, 63);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(42, 13);
            this.label79.TabIndex = 73;
            this.label79.Text = "Ymax =";
            // 
            // Giperbola_Y_min
            // 
            this.Giperbola_Y_min.Location = new System.Drawing.Point(754, 86);
            this.Giperbola_Y_min.Name = "Giperbola_Y_min";
            this.Giperbola_Y_min.Size = new System.Drawing.Size(50, 20);
            this.Giperbola_Y_min.TabIndex = 72;
            // 
            // Giperbola_Y_max
            // 
            this.Giperbola_Y_max.Location = new System.Drawing.Point(754, 60);
            this.Giperbola_Y_max.Name = "Giperbola_Y_max";
            this.Giperbola_Y_max.Size = new System.Drawing.Size(50, 20);
            this.Giperbola_Y_max.TabIndex = 71;
            // 
            // Giperbola_standart_graph
            // 
            this.Giperbola_standart_graph.AutoSize = true;
            this.Giperbola_standart_graph.Location = new System.Drawing.Point(721, 240);
            this.Giperbola_standart_graph.Name = "Giperbola_standart_graph";
            this.Giperbola_standart_graph.Size = new System.Drawing.Size(65, 17);
            this.Giperbola_standart_graph.TabIndex = 62;
            this.Giperbola_standart_graph.TabStop = true;
            this.Giperbola_standart_graph.Text = "Standart";
            this.Giperbola_standart_graph.UseVisualStyleBackColor = true;
            // 
            // Giperbola_max_graph
            // 
            this.Giperbola_max_graph.AutoSize = true;
            this.Giperbola_max_graph.Location = new System.Drawing.Point(640, 286);
            this.Giperbola_max_graph.Name = "Giperbola_max_graph";
            this.Giperbola_max_graph.Size = new System.Drawing.Size(45, 17);
            this.Giperbola_max_graph.TabIndex = 61;
            this.Giperbola_max_graph.TabStop = true;
            this.Giperbola_max_graph.Text = "Max";
            this.Giperbola_max_graph.UseVisualStyleBackColor = true;
            // 
            // Giperbola_avg_graph
            // 
            this.Giperbola_avg_graph.AutoSize = true;
            this.Giperbola_avg_graph.Location = new System.Drawing.Point(640, 263);
            this.Giperbola_avg_graph.Name = "Giperbola_avg_graph";
            this.Giperbola_avg_graph.Size = new System.Drawing.Size(65, 17);
            this.Giperbola_avg_graph.TabIndex = 60;
            this.Giperbola_avg_graph.TabStop = true;
            this.Giperbola_avg_graph.Text = "Average";
            this.Giperbola_avg_graph.UseVisualStyleBackColor = true;
            // 
            // Giperbola_min_graph
            // 
            this.Giperbola_min_graph.AutoSize = true;
            this.Giperbola_min_graph.Location = new System.Drawing.Point(640, 240);
            this.Giperbola_min_graph.Name = "Giperbola_min_graph";
            this.Giperbola_min_graph.Size = new System.Drawing.Size(42, 17);
            this.Giperbola_min_graph.TabIndex = 59;
            this.Giperbola_min_graph.TabStop = true;
            this.Giperbola_min_graph.Text = "Min";
            this.Giperbola_min_graph.UseVisualStyleBackColor = true;
            // 
            // giperbola_grahp_function
            // 
            this.giperbola_grahp_function.Location = new System.Drawing.Point(619, 122);
            this.giperbola_grahp_function.Name = "giperbola_grahp_function";
            this.giperbola_grahp_function.Size = new System.Drawing.Size(220, 50);
            this.giperbola_grahp_function.TabIndex = 54;
            this.giperbola_grahp_function.Text = "";
            // 
            // Giperbola_save
            // 
            this.Giperbola_save.Location = new System.Drawing.Point(750, 400);
            this.Giperbola_save.Name = "Giperbola_save";
            this.Giperbola_save.Size = new System.Drawing.Size(75, 23);
            this.Giperbola_save.TabIndex = 52;
            this.Giperbola_save.Text = "Save";
            this.Giperbola_save.UseVisualStyleBackColor = true;
            this.Giperbola_save.Click += new System.EventHandler(this.Giperbola_save_Click);
            // 
            // Giperbola_levellines_draw
            // 
            this.Giperbola_levellines_draw.Location = new System.Drawing.Point(721, 349);
            this.Giperbola_levellines_draw.Name = "Giperbola_levellines_draw";
            this.Giperbola_levellines_draw.Size = new System.Drawing.Size(75, 23);
            this.Giperbola_levellines_draw.TabIndex = 51;
            this.Giperbola_levellines_draw.Text = "Level lines";
            this.Giperbola_levellines_draw.UseVisualStyleBackColor = true;
            this.Giperbola_levellines_draw.Click += new System.EventHandler(this.Giperbola_levellines_draw_Click);
            // 
            // Giperbola_3Dchart_Draw
            // 
            this.Giperbola_3Dchart_Draw.Location = new System.Drawing.Point(721, 320);
            this.Giperbola_3Dchart_Draw.Name = "Giperbola_3Dchart_Draw";
            this.Giperbola_3Dchart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Giperbola_3Dchart_Draw.TabIndex = 50;
            this.Giperbola_3Dchart_Draw.Text = "Surface";
            this.Giperbola_3Dchart_Draw.UseVisualStyleBackColor = true;
            this.Giperbola_3Dchart_Draw.Click += new System.EventHandler(this.Giperbola_3Dchart_Draw_Click);
            // 
            // giperbola_X2_collection
            // 
            this.giperbola_X2_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.giperbola_X2_collection.FormattingEnabled = true;
            this.giperbola_X2_collection.Location = new System.Drawing.Point(721, 203);
            this.giperbola_X2_collection.Name = "giperbola_X2_collection";
            this.giperbola_X2_collection.Size = new System.Drawing.Size(48, 21);
            this.giperbola_X2_collection.TabIndex = 49;
            // 
            // Giperbola_chart_Draw
            // 
            this.Giperbola_chart_Draw.Location = new System.Drawing.Point(640, 320);
            this.Giperbola_chart_Draw.Name = "Giperbola_chart_Draw";
            this.Giperbola_chart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Giperbola_chart_Draw.TabIndex = 48;
            this.Giperbola_chart_Draw.Text = "Line";
            this.Giperbola_chart_Draw.UseVisualStyleBackColor = true;
            this.Giperbola_chart_Draw.Click += new System.EventHandler(this.Giperbola_chart_Draw_Click);
            // 
            // giperbola_X1_collection
            // 
            this.giperbola_X1_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.giperbola_X1_collection.FormattingEnabled = true;
            this.giperbola_X1_collection.Location = new System.Drawing.Point(667, 203);
            this.giperbola_X1_collection.Name = "giperbola_X1_collection";
            this.giperbola_X1_collection.Size = new System.Drawing.Size(48, 21);
            this.giperbola_X1_collection.TabIndex = 47;
            // 
            // Giperbola_Chart_viewer
            // 
            this.Giperbola_Chart_viewer.Location = new System.Drawing.Point(30, 30);
            this.Giperbola_Chart_viewer.Name = "Giperbola_Chart_viewer";
            this.Giperbola_Chart_viewer.Size = new System.Drawing.Size(300, 200);
            this.Giperbola_Chart_viewer.TabIndex = 42;
            this.Giperbola_Chart_viewer.TabStop = false;
            // 
            // giperbola_detail_R
            // 
            this.giperbola_detail_R.Location = new System.Drawing.Point(620, 86);
            this.giperbola_detail_R.Name = "giperbola_detail_R";
            this.giperbola_detail_R.Size = new System.Drawing.Size(80, 20);
            this.giperbola_detail_R.TabIndex = 40;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(578, 89);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(36, 13);
            this.label33.TabIndex = 39;
            this.label33.Text = "R^2 =";
            // 
            // giperbola_detail_korrelation
            // 
            this.giperbola_detail_korrelation.Location = new System.Drawing.Point(620, 60);
            this.giperbola_detail_korrelation.Name = "giperbola_detail_korrelation";
            this.giperbola_detail_korrelation.Size = new System.Drawing.Size(80, 20);
            this.giperbola_detail_korrelation.TabIndex = 38;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(583, 63);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 13);
            this.label34.TabIndex = 37;
            this.label34.Text = "kor =";
            // 
            // giperbolaChart
            // 
            chartArea5.Name = "ChartArea1";
            this.giperbolaChart.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.giperbolaChart.Legends.Add(legend5);
            this.giperbolaChart.Location = new System.Drawing.Point(30, 30);
            this.giperbolaChart.Name = "giperbolaChart";
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series9.Legend = "Legend1";
            series9.Name = "Data";
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series10.Legend = "Legend1";
            series10.Name = "Regression";
            this.giperbolaChart.Series.Add(series9);
            this.giperbolaChart.Series.Add(series10);
            this.giperbolaChart.Size = new System.Drawing.Size(560, 300);
            this.giperbolaChart.TabIndex = 0;
            this.giperbolaChart.Text = "chart2";
            this.giperbolaChart.Visible = false;
            // 
            // Square_Page
            // 
            this.Square_Page.Controls.Add(this.label80);
            this.Square_Page.Controls.Add(this.label81);
            this.Square_Page.Controls.Add(this.Square_Y_min);
            this.Square_Page.Controls.Add(this.Square_Y_max);
            this.Square_Page.Controls.Add(this.Square_standart_graph);
            this.Square_Page.Controls.Add(this.Square_max_graph);
            this.Square_Page.Controls.Add(this.Square_avg_graph);
            this.Square_Page.Controls.Add(this.Square_min_graph);
            this.Square_Page.Controls.Add(this.square_grahp_function);
            this.Square_Page.Controls.Add(this.Square_save);
            this.Square_Page.Controls.Add(this.squareChart);
            this.Square_Page.Controls.Add(this.Square_levellines_draw);
            this.Square_Page.Controls.Add(this.Square_3Dchart_Draw);
            this.Square_Page.Controls.Add(this.square_X2_collection);
            this.Square_Page.Controls.Add(this.Square_chart_Draw);
            this.Square_Page.Controls.Add(this.square_X1_collection);
            this.Square_Page.Controls.Add(this.Square_Chart_viewer);
            this.Square_Page.Controls.Add(this.square_detail_R);
            this.Square_Page.Controls.Add(this.label35);
            this.Square_Page.Controls.Add(this.square_detail_korrelation);
            this.Square_Page.Controls.Add(this.label36);
            this.Square_Page.Location = new System.Drawing.Point(4, 22);
            this.Square_Page.Name = "Square_Page";
            this.Square_Page.Size = new System.Drawing.Size(842, 454);
            this.Square_Page.TabIndex = 8;
            this.Square_Page.Text = "Square";
            this.Square_Page.UseVisualStyleBackColor = true;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(709, 89);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(39, 13);
            this.label80.TabIndex = 78;
            this.label80.Text = "Ymin =";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(706, 63);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(42, 13);
            this.label81.TabIndex = 77;
            this.label81.Text = "Ymax =";
            // 
            // Square_Y_min
            // 
            this.Square_Y_min.Location = new System.Drawing.Point(754, 86);
            this.Square_Y_min.Name = "Square_Y_min";
            this.Square_Y_min.Size = new System.Drawing.Size(50, 20);
            this.Square_Y_min.TabIndex = 76;
            // 
            // Square_Y_max
            // 
            this.Square_Y_max.Location = new System.Drawing.Point(754, 60);
            this.Square_Y_max.Name = "Square_Y_max";
            this.Square_Y_max.Size = new System.Drawing.Size(50, 20);
            this.Square_Y_max.TabIndex = 75;
            // 
            // Square_standart_graph
            // 
            this.Square_standart_graph.AutoSize = true;
            this.Square_standart_graph.Location = new System.Drawing.Point(721, 240);
            this.Square_standart_graph.Name = "Square_standart_graph";
            this.Square_standart_graph.Size = new System.Drawing.Size(65, 17);
            this.Square_standart_graph.TabIndex = 66;
            this.Square_standart_graph.TabStop = true;
            this.Square_standart_graph.Text = "Standart";
            this.Square_standart_graph.UseVisualStyleBackColor = true;
            // 
            // Square_max_graph
            // 
            this.Square_max_graph.AutoSize = true;
            this.Square_max_graph.Location = new System.Drawing.Point(640, 286);
            this.Square_max_graph.Name = "Square_max_graph";
            this.Square_max_graph.Size = new System.Drawing.Size(45, 17);
            this.Square_max_graph.TabIndex = 65;
            this.Square_max_graph.TabStop = true;
            this.Square_max_graph.Text = "Max";
            this.Square_max_graph.UseVisualStyleBackColor = true;
            // 
            // Square_avg_graph
            // 
            this.Square_avg_graph.AutoSize = true;
            this.Square_avg_graph.Location = new System.Drawing.Point(640, 263);
            this.Square_avg_graph.Name = "Square_avg_graph";
            this.Square_avg_graph.Size = new System.Drawing.Size(65, 17);
            this.Square_avg_graph.TabIndex = 64;
            this.Square_avg_graph.TabStop = true;
            this.Square_avg_graph.Text = "Average";
            this.Square_avg_graph.UseVisualStyleBackColor = true;
            // 
            // Square_min_graph
            // 
            this.Square_min_graph.AutoSize = true;
            this.Square_min_graph.Location = new System.Drawing.Point(640, 240);
            this.Square_min_graph.Name = "Square_min_graph";
            this.Square_min_graph.Size = new System.Drawing.Size(42, 17);
            this.Square_min_graph.TabIndex = 63;
            this.Square_min_graph.TabStop = true;
            this.Square_min_graph.Text = "Min";
            this.Square_min_graph.UseVisualStyleBackColor = true;
            // 
            // square_grahp_function
            // 
            this.square_grahp_function.Location = new System.Drawing.Point(619, 122);
            this.square_grahp_function.Name = "square_grahp_function";
            this.square_grahp_function.Size = new System.Drawing.Size(220, 50);
            this.square_grahp_function.TabIndex = 54;
            this.square_grahp_function.Text = "";
            // 
            // Square_save
            // 
            this.Square_save.Location = new System.Drawing.Point(750, 400);
            this.Square_save.Name = "Square_save";
            this.Square_save.Size = new System.Drawing.Size(75, 23);
            this.Square_save.TabIndex = 53;
            this.Square_save.Text = "Save";
            this.Square_save.UseVisualStyleBackColor = true;
            this.Square_save.Click += new System.EventHandler(this.Square_save_Click);
            // 
            // squareChart
            // 
            chartArea6.Name = "ChartArea1";
            this.squareChart.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.squareChart.Legends.Add(legend6);
            this.squareChart.Location = new System.Drawing.Point(30, 30);
            this.squareChart.Name = "squareChart";
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series11.Legend = "Legend1";
            series11.Name = "Data";
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series12.Legend = "Legend1";
            series12.Name = "Regression";
            this.squareChart.Series.Add(series11);
            this.squareChart.Series.Add(series12);
            this.squareChart.Size = new System.Drawing.Size(560, 300);
            this.squareChart.TabIndex = 52;
            this.squareChart.Text = "chart2";
            this.squareChart.Visible = false;
            // 
            // Square_levellines_draw
            // 
            this.Square_levellines_draw.Location = new System.Drawing.Point(721, 349);
            this.Square_levellines_draw.Name = "Square_levellines_draw";
            this.Square_levellines_draw.Size = new System.Drawing.Size(75, 23);
            this.Square_levellines_draw.TabIndex = 51;
            this.Square_levellines_draw.Text = "Level lines";
            this.Square_levellines_draw.UseVisualStyleBackColor = true;
            this.Square_levellines_draw.Click += new System.EventHandler(this.Square_levellines_draw_Click);
            // 
            // Square_3Dchart_Draw
            // 
            this.Square_3Dchart_Draw.Location = new System.Drawing.Point(721, 320);
            this.Square_3Dchart_Draw.Name = "Square_3Dchart_Draw";
            this.Square_3Dchart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Square_3Dchart_Draw.TabIndex = 50;
            this.Square_3Dchart_Draw.Text = "Surface";
            this.Square_3Dchart_Draw.UseVisualStyleBackColor = true;
            this.Square_3Dchart_Draw.Click += new System.EventHandler(this.Square_3Dchart_Draw_Click);
            // 
            // square_X2_collection
            // 
            this.square_X2_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.square_X2_collection.FormattingEnabled = true;
            this.square_X2_collection.Location = new System.Drawing.Point(721, 203);
            this.square_X2_collection.Name = "square_X2_collection";
            this.square_X2_collection.Size = new System.Drawing.Size(48, 21);
            this.square_X2_collection.TabIndex = 49;
            // 
            // Square_chart_Draw
            // 
            this.Square_chart_Draw.Location = new System.Drawing.Point(640, 320);
            this.Square_chart_Draw.Name = "Square_chart_Draw";
            this.Square_chart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Square_chart_Draw.TabIndex = 48;
            this.Square_chart_Draw.Text = "Line";
            this.Square_chart_Draw.UseVisualStyleBackColor = true;
            this.Square_chart_Draw.Click += new System.EventHandler(this.Square_chart_Draw_Click);
            // 
            // square_X1_collection
            // 
            this.square_X1_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.square_X1_collection.FormattingEnabled = true;
            this.square_X1_collection.Location = new System.Drawing.Point(667, 203);
            this.square_X1_collection.Name = "square_X1_collection";
            this.square_X1_collection.Size = new System.Drawing.Size(48, 21);
            this.square_X1_collection.TabIndex = 47;
            // 
            // Square_Chart_viewer
            // 
            this.Square_Chart_viewer.Location = new System.Drawing.Point(30, 30);
            this.Square_Chart_viewer.Name = "Square_Chart_viewer";
            this.Square_Chart_viewer.Size = new System.Drawing.Size(300, 200);
            this.Square_Chart_viewer.TabIndex = 42;
            this.Square_Chart_viewer.TabStop = false;
            // 
            // square_detail_R
            // 
            this.square_detail_R.Location = new System.Drawing.Point(620, 86);
            this.square_detail_R.Name = "square_detail_R";
            this.square_detail_R.Size = new System.Drawing.Size(80, 20);
            this.square_detail_R.TabIndex = 40;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(578, 89);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 13);
            this.label35.TabIndex = 39;
            this.label35.Text = "R^2 =";
            // 
            // square_detail_korrelation
            // 
            this.square_detail_korrelation.Location = new System.Drawing.Point(620, 60);
            this.square_detail_korrelation.Name = "square_detail_korrelation";
            this.square_detail_korrelation.Size = new System.Drawing.Size(80, 20);
            this.square_detail_korrelation.TabIndex = 38;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(583, 63);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(31, 13);
            this.label36.TabIndex = 37;
            this.label36.Text = "kor =";
            // 
            // Polinomial_Page
            // 
            this.Polinomial_Page.Controls.Add(this.label82);
            this.Polinomial_Page.Controls.Add(this.label83);
            this.Polinomial_Page.Controls.Add(this.Polinomial_Y_min);
            this.Polinomial_Page.Controls.Add(this.Polinomial_Y_max);
            this.Polinomial_Page.Controls.Add(this.Polinomial_standart_graph);
            this.Polinomial_Page.Controls.Add(this.Polinomial_max_graph);
            this.Polinomial_Page.Controls.Add(this.Polinomial_avg_graph);
            this.Polinomial_Page.Controls.Add(this.Polinomial_min_graph);
            this.Polinomial_Page.Controls.Add(this.polinomial_grahp_function);
            this.Polinomial_Page.Controls.Add(this.Polinomial_save);
            this.Polinomial_Page.Controls.Add(this.Polinomial_Chart_viewer);
            this.Polinomial_Page.Controls.Add(this.polinomialChart);
            this.Polinomial_Page.Controls.Add(this.polinomial_detail_R);
            this.Polinomial_Page.Controls.Add(this.label46);
            this.Polinomial_Page.Controls.Add(this.polinomial_detail_korrelation);
            this.Polinomial_Page.Controls.Add(this.label47);
            this.Polinomial_Page.Controls.Add(this.Polinomial_levellines_draw);
            this.Polinomial_Page.Controls.Add(this.Polinomial_3Dchart_Draw);
            this.Polinomial_Page.Controls.Add(this.polinomial_X2_collection);
            this.Polinomial_Page.Controls.Add(this.Polinomial_chart_Draw);
            this.Polinomial_Page.Controls.Add(this.polinomial_X1_collection);
            this.Polinomial_Page.Location = new System.Drawing.Point(4, 22);
            this.Polinomial_Page.Name = "Polinomial_Page";
            this.Polinomial_Page.Size = new System.Drawing.Size(842, 454);
            this.Polinomial_Page.TabIndex = 9;
            this.Polinomial_Page.Text = "Polinomial";
            this.Polinomial_Page.UseVisualStyleBackColor = true;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(709, 89);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(39, 13);
            this.label82.TabIndex = 82;
            this.label82.Text = "Ymin =";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(706, 63);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(42, 13);
            this.label83.TabIndex = 81;
            this.label83.Text = "Ymax =";
            // 
            // Polinomial_Y_min
            // 
            this.Polinomial_Y_min.Location = new System.Drawing.Point(754, 86);
            this.Polinomial_Y_min.Name = "Polinomial_Y_min";
            this.Polinomial_Y_min.Size = new System.Drawing.Size(50, 20);
            this.Polinomial_Y_min.TabIndex = 80;
            // 
            // Polinomial_Y_max
            // 
            this.Polinomial_Y_max.Location = new System.Drawing.Point(754, 60);
            this.Polinomial_Y_max.Name = "Polinomial_Y_max";
            this.Polinomial_Y_max.Size = new System.Drawing.Size(50, 20);
            this.Polinomial_Y_max.TabIndex = 79;
            // 
            // Polinomial_standart_graph
            // 
            this.Polinomial_standart_graph.AutoSize = true;
            this.Polinomial_standart_graph.Location = new System.Drawing.Point(721, 240);
            this.Polinomial_standart_graph.Name = "Polinomial_standart_graph";
            this.Polinomial_standart_graph.Size = new System.Drawing.Size(65, 17);
            this.Polinomial_standart_graph.TabIndex = 57;
            this.Polinomial_standart_graph.TabStop = true;
            this.Polinomial_standart_graph.Text = "Standart";
            this.Polinomial_standart_graph.UseVisualStyleBackColor = true;
            // 
            // Polinomial_max_graph
            // 
            this.Polinomial_max_graph.AutoSize = true;
            this.Polinomial_max_graph.Location = new System.Drawing.Point(640, 286);
            this.Polinomial_max_graph.Name = "Polinomial_max_graph";
            this.Polinomial_max_graph.Size = new System.Drawing.Size(45, 17);
            this.Polinomial_max_graph.TabIndex = 56;
            this.Polinomial_max_graph.TabStop = true;
            this.Polinomial_max_graph.Text = "Max";
            this.Polinomial_max_graph.UseVisualStyleBackColor = true;
            // 
            // Polinomial_avg_graph
            // 
            this.Polinomial_avg_graph.AutoSize = true;
            this.Polinomial_avg_graph.Location = new System.Drawing.Point(640, 263);
            this.Polinomial_avg_graph.Name = "Polinomial_avg_graph";
            this.Polinomial_avg_graph.Size = new System.Drawing.Size(65, 17);
            this.Polinomial_avg_graph.TabIndex = 55;
            this.Polinomial_avg_graph.TabStop = true;
            this.Polinomial_avg_graph.Text = "Average";
            this.Polinomial_avg_graph.UseVisualStyleBackColor = true;
            // 
            // Polinomial_min_graph
            // 
            this.Polinomial_min_graph.AutoSize = true;
            this.Polinomial_min_graph.Location = new System.Drawing.Point(640, 240);
            this.Polinomial_min_graph.Name = "Polinomial_min_graph";
            this.Polinomial_min_graph.Size = new System.Drawing.Size(42, 17);
            this.Polinomial_min_graph.TabIndex = 54;
            this.Polinomial_min_graph.TabStop = true;
            this.Polinomial_min_graph.Text = "Min";
            this.Polinomial_min_graph.UseVisualStyleBackColor = true;
            // 
            // polinomial_grahp_function
            // 
            this.polinomial_grahp_function.Location = new System.Drawing.Point(619, 122);
            this.polinomial_grahp_function.Name = "polinomial_grahp_function";
            this.polinomial_grahp_function.Size = new System.Drawing.Size(220, 50);
            this.polinomial_grahp_function.TabIndex = 53;
            this.polinomial_grahp_function.Text = "";
            // 
            // Polinomial_save
            // 
            this.Polinomial_save.Location = new System.Drawing.Point(750, 400);
            this.Polinomial_save.Name = "Polinomial_save";
            this.Polinomial_save.Size = new System.Drawing.Size(75, 23);
            this.Polinomial_save.TabIndex = 52;
            this.Polinomial_save.Text = "Save";
            this.Polinomial_save.UseVisualStyleBackColor = true;
            this.Polinomial_save.Click += new System.EventHandler(this.Polinomial_save_Click_1);
            // 
            // Polinomial_Chart_viewer
            // 
            this.Polinomial_Chart_viewer.Location = new System.Drawing.Point(30, 30);
            this.Polinomial_Chart_viewer.Name = "Polinomial_Chart_viewer";
            this.Polinomial_Chart_viewer.Size = new System.Drawing.Size(300, 200);
            this.Polinomial_Chart_viewer.TabIndex = 51;
            this.Polinomial_Chart_viewer.TabStop = false;
            // 
            // polinomialChart
            // 
            this.polinomialChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea7.Name = "ChartArea1";
            this.polinomialChart.ChartAreas.Add(chartArea7);
            legend7.Name = "Legend1";
            this.polinomialChart.Legends.Add(legend7);
            this.polinomialChart.Location = new System.Drawing.Point(30, 30);
            this.polinomialChart.Name = "polinomialChart";
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series13.Legend = "Legend1";
            series13.Name = "Data";
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Legend = "Legend1";
            series14.Name = "Regression";
            this.polinomialChart.Series.Add(series13);
            this.polinomialChart.Series.Add(series14);
            this.polinomialChart.Size = new System.Drawing.Size(560, 300);
            this.polinomialChart.TabIndex = 50;
            this.polinomialChart.Text = "chart1";
            this.polinomialChart.Visible = false;
            // 
            // polinomial_detail_R
            // 
            this.polinomial_detail_R.Location = new System.Drawing.Point(620, 86);
            this.polinomial_detail_R.Name = "polinomial_detail_R";
            this.polinomial_detail_R.Size = new System.Drawing.Size(80, 20);
            this.polinomial_detail_R.TabIndex = 49;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(578, 89);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(36, 13);
            this.label46.TabIndex = 48;
            this.label46.Text = "R^2 =";
            // 
            // polinomial_detail_korrelation
            // 
            this.polinomial_detail_korrelation.Location = new System.Drawing.Point(620, 60);
            this.polinomial_detail_korrelation.Name = "polinomial_detail_korrelation";
            this.polinomial_detail_korrelation.Size = new System.Drawing.Size(80, 20);
            this.polinomial_detail_korrelation.TabIndex = 47;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(583, 63);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(31, 13);
            this.label47.TabIndex = 46;
            this.label47.Text = "kor =";
            // 
            // Polinomial_levellines_draw
            // 
            this.Polinomial_levellines_draw.Location = new System.Drawing.Point(721, 349);
            this.Polinomial_levellines_draw.Name = "Polinomial_levellines_draw";
            this.Polinomial_levellines_draw.Size = new System.Drawing.Size(75, 23);
            this.Polinomial_levellines_draw.TabIndex = 45;
            this.Polinomial_levellines_draw.Text = "Level lines";
            this.Polinomial_levellines_draw.UseVisualStyleBackColor = true;
            this.Polinomial_levellines_draw.Click += new System.EventHandler(this.Polinomial_levellines_draw_Click_1);
            // 
            // Polinomial_3Dchart_Draw
            // 
            this.Polinomial_3Dchart_Draw.Location = new System.Drawing.Point(721, 320);
            this.Polinomial_3Dchart_Draw.Name = "Polinomial_3Dchart_Draw";
            this.Polinomial_3Dchart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Polinomial_3Dchart_Draw.TabIndex = 44;
            this.Polinomial_3Dchart_Draw.Text = "Surface";
            this.Polinomial_3Dchart_Draw.UseVisualStyleBackColor = true;
            this.Polinomial_3Dchart_Draw.Click += new System.EventHandler(this.Polinomial_3Dchart_Draw_Click_1);
            // 
            // polinomial_X2_collection
            // 
            this.polinomial_X2_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.polinomial_X2_collection.FormattingEnabled = true;
            this.polinomial_X2_collection.Location = new System.Drawing.Point(721, 203);
            this.polinomial_X2_collection.Name = "polinomial_X2_collection";
            this.polinomial_X2_collection.Size = new System.Drawing.Size(48, 21);
            this.polinomial_X2_collection.TabIndex = 43;
            // 
            // Polinomial_chart_Draw
            // 
            this.Polinomial_chart_Draw.Location = new System.Drawing.Point(640, 320);
            this.Polinomial_chart_Draw.Name = "Polinomial_chart_Draw";
            this.Polinomial_chart_Draw.Size = new System.Drawing.Size(75, 23);
            this.Polinomial_chart_Draw.TabIndex = 42;
            this.Polinomial_chart_Draw.Text = "Line";
            this.Polinomial_chart_Draw.UseVisualStyleBackColor = true;
            this.Polinomial_chart_Draw.Click += new System.EventHandler(this.Polinomial_chart_Draw_Click_1);
            // 
            // polinomial_X1_collection
            // 
            this.polinomial_X1_collection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.polinomial_X1_collection.FormattingEnabled = true;
            this.polinomial_X1_collection.Location = new System.Drawing.Point(667, 203);
            this.polinomial_X1_collection.Name = "polinomial_X1_collection";
            this.polinomial_X1_collection.Size = new System.Drawing.Size(48, 21);
            this.polinomial_X1_collection.TabIndex = 41;
            // 
            // TimePage
            // 
            this.TimePage.Controls.Add(this.label72);
            this.TimePage.Controls.Add(this.Total_Auto_time);
            this.TimePage.Controls.Add(this.label71);
            this.TimePage.Controls.Add(this.Exponential_Auto_time);
            this.TimePage.Controls.Add(this.label70);
            this.TimePage.Controls.Add(this.Linear_Auto_time);
            this.TimePage.Controls.Add(this.label69);
            this.TimePage.Controls.Add(this.Polinomial_time);
            this.TimePage.Controls.Add(this.label68);
            this.TimePage.Controls.Add(this.Square_time);
            this.TimePage.Controls.Add(this.label67);
            this.TimePage.Controls.Add(this.Giperbola_time);
            this.TimePage.Controls.Add(this.label66);
            this.TimePage.Controls.Add(this.Degree_time);
            this.TimePage.Controls.Add(this.label65);
            this.TimePage.Controls.Add(this.Polinomial_Auto_time);
            this.TimePage.Controls.Add(this.label64);
            this.TimePage.Controls.Add(this.Exponential_time);
            this.TimePage.Controls.Add(this.label63);
            this.TimePage.Controls.Add(this.Linear_time);
            this.TimePage.Location = new System.Drawing.Point(4, 22);
            this.TimePage.Name = "TimePage";
            this.TimePage.Size = new System.Drawing.Size(842, 454);
            this.TimePage.TabIndex = 10;
            this.TimePage.Text = "Time";
            this.TimePage.UseVisualStyleBackColor = true;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(289, 121);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(65, 13);
            this.label72.TabIndex = 73;
            this.label72.Text = "Total Auto =";
            // 
            // Total_Auto_time
            // 
            this.Total_Auto_time.Location = new System.Drawing.Point(360, 118);
            this.Total_Auto_time.Name = "Total_Auto_time";
            this.Total_Auto_time.Size = new System.Drawing.Size(120, 20);
            this.Total_Auto_time.TabIndex = 72;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(258, 95);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(96, 13);
            this.label71.TabIndex = 71;
            this.label71.Text = "Exponential Auto =";
            // 
            // Exponential_Auto_time
            // 
            this.Exponential_Auto_time.Location = new System.Drawing.Point(360, 92);
            this.Exponential_Auto_time.Name = "Exponential_Auto_time";
            this.Exponential_Auto_time.Size = new System.Drawing.Size(120, 20);
            this.Exponential_Auto_time.TabIndex = 70;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(284, 43);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(70, 13);
            this.label70.TabIndex = 69;
            this.label70.Text = "Linear Auto =";
            // 
            // Linear_Auto_time
            // 
            this.Linear_Auto_time.Location = new System.Drawing.Point(360, 40);
            this.Linear_Auto_time.Name = "Linear_Auto_time";
            this.Linear_Auto_time.Size = new System.Drawing.Size(120, 20);
            this.Linear_Auto_time.TabIndex = 68;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(31, 173);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(63, 13);
            this.label69.TabIndex = 67;
            this.label69.Text = "Polinomial =";
            // 
            // Polinomial_time
            // 
            this.Polinomial_time.Location = new System.Drawing.Point(100, 170);
            this.Polinomial_time.Name = "Polinomial_time";
            this.Polinomial_time.Size = new System.Drawing.Size(120, 20);
            this.Polinomial_time.TabIndex = 66;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(44, 147);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(50, 13);
            this.label68.TabIndex = 65;
            this.label68.Text = "Square =";
            // 
            // Square_time
            // 
            this.Square_time.Location = new System.Drawing.Point(100, 144);
            this.Square_time.Name = "Square_time";
            this.Square_time.Size = new System.Drawing.Size(120, 20);
            this.Square_time.TabIndex = 64;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(33, 121);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(61, 13);
            this.label67.TabIndex = 63;
            this.label67.Text = "Giperbola =";
            // 
            // Giperbola_time
            // 
            this.Giperbola_time.Location = new System.Drawing.Point(100, 118);
            this.Giperbola_time.Name = "Giperbola_time";
            this.Giperbola_time.Size = new System.Drawing.Size(120, 20);
            this.Giperbola_time.TabIndex = 62;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(43, 95);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(51, 13);
            this.label66.TabIndex = 61;
            this.label66.Text = "Degree =";
            // 
            // Degree_time
            // 
            this.Degree_time.Location = new System.Drawing.Point(100, 92);
            this.Degree_time.Name = "Degree_time";
            this.Degree_time.Size = new System.Drawing.Size(120, 20);
            this.Degree_time.TabIndex = 60;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(266, 69);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(88, 13);
            this.label65.TabIndex = 59;
            this.label65.Text = "Polinomial Auto =";
            // 
            // Polinomial_Auto_time
            // 
            this.Polinomial_Auto_time.Location = new System.Drawing.Point(360, 66);
            this.Polinomial_Auto_time.Name = "Polinomial_Auto_time";
            this.Polinomial_Auto_time.Size = new System.Drawing.Size(120, 20);
            this.Polinomial_Auto_time.TabIndex = 58;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(23, 69);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(71, 13);
            this.label64.TabIndex = 57;
            this.label64.Text = "Exponential =";
            // 
            // Exponential_time
            // 
            this.Exponential_time.Location = new System.Drawing.Point(100, 66);
            this.Exponential_time.Name = "Exponential_time";
            this.Exponential_time.Size = new System.Drawing.Size(120, 20);
            this.Exponential_time.TabIndex = 56;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(49, 43);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(45, 13);
            this.label63.TabIndex = 55;
            this.label63.Text = "Linear =";
            // 
            // Linear_time
            // 
            this.Linear_time.Location = new System.Drawing.Point(100, 40);
            this.Linear_time.Name = "Linear_time";
            this.Linear_time.Size = new System.Drawing.Size(120, 20);
            this.Linear_time.TabIndex = 54;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hmainMenu});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(884, 24);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "Main";
            // 
            // hmainMenu
            // 
            this.hmainMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpmainMenu,
            this.exitToolStripMenuItem});
            this.hmainMenu.Name = "hmainMenu";
            this.hmainMenu.Size = new System.Drawing.Size(45, 20);
            this.hmainMenu.Text = "Main";
            // 
            // helpmainMenu
            // 
            this.helpmainMenu.Name = "helpmainMenu";
            this.helpmainMenu.Size = new System.Drawing.Size(98, 22);
            this.helpmainMenu.Text = "Help";
            this.helpmainMenu.Click += new System.EventHandler(this.helpmainMenu_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // Result_Summary
            // 
            this.Result_Summary.Controls.Add(this.Result_Summary_Sigma);
            this.Result_Summary.Controls.Add(this.Result_Summary_Model_Select);
            this.Result_Summary.Controls.Add(this.Result_Summary_Show);
            this.Result_Summary.Controls.Add(this.Result_Summary_DataGrid);
            this.Result_Summary.Location = new System.Drawing.Point(4, 22);
            this.Result_Summary.Name = "Result_Summary";
            this.Result_Summary.Size = new System.Drawing.Size(842, 454);
            this.Result_Summary.TabIndex = 11;
            this.Result_Summary.Text = "Result Summary";
            this.Result_Summary.UseVisualStyleBackColor = true;
            // 
            // Result_Summary_DataGrid
            // 
            this.Result_Summary_DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Result_Summary_DataGrid.Location = new System.Drawing.Point(20, 15);
            this.Result_Summary_DataGrid.Name = "Result_Summary_DataGrid";
            this.Result_Summary_DataGrid.Size = new System.Drawing.Size(487, 359);
            this.Result_Summary_DataGrid.TabIndex = 0;
            // 
            // Result_Summary_Show
            // 
            this.Result_Summary_Show.Location = new System.Drawing.Point(750, 15);
            this.Result_Summary_Show.Name = "Result_Summary_Show";
            this.Result_Summary_Show.Size = new System.Drawing.Size(75, 23);
            this.Result_Summary_Show.TabIndex = 1;
            this.Result_Summary_Show.Text = "Show";
            this.Result_Summary_Show.UseVisualStyleBackColor = true;
            this.Result_Summary_Show.Click += new System.EventHandler(this.Result_Summary_Show_Click);
            // 
            // Result_Summary_Model_Select
            // 
            this.Result_Summary_Model_Select.AutoCompleteCustomSource.AddRange(new string[] {
            "Linear",
            "Exponential",
            "Polinomial",
            "Polinomial Auto",
            "Degree",
            "Giperbola",
            "Square"});
            this.Result_Summary_Model_Select.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Result_Summary_Model_Select.FormattingEnabled = true;
            this.Result_Summary_Model_Select.Items.AddRange(new object[] {
            "Linear",
            "Exponential",
            "Polinomial",
            "Polinomial Auto",
            "Degree",
            "Giperbola",
            "Square"});
            this.Result_Summary_Model_Select.Location = new System.Drawing.Point(589, 16);
            this.Result_Summary_Model_Select.Name = "Result_Summary_Model_Select";
            this.Result_Summary_Model_Select.Size = new System.Drawing.Size(121, 21);
            this.Result_Summary_Model_Select.TabIndex = 2;
            // 
            // Result_Summary_Sigma
            // 
            this.Result_Summary_Sigma.Location = new System.Drawing.Point(745, 68);
            this.Result_Summary_Sigma.Name = "Result_Summary_Sigma";
            this.Result_Summary_Sigma.Size = new System.Drawing.Size(80, 20);
            this.Result_Summary_Sigma.TabIndex = 39;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 512);
            this.Controls.Add(this.mainTab);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "Form1";
            this.Text = "RegressionModel";
            this.mainTab.ResumeLayout(false);
            this.Data_Page.ResumeLayout(false);
            this.Data_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.Result_Page.ResumeLayout(false);
            this.Result_Page.PerformLayout();
            this.Auto_Result.ResumeLayout(false);
            this.Auto_Result.PerformLayout();
            this.Linear_Page.ResumeLayout(false);
            this.Linear_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Linear_Chart_viewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearChart)).EndInit();
            this.Exponential_Page.ResumeLayout(false);
            this.Exponential_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Exponential_Chart_viewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exponentialChart)).EndInit();
            this.PolinomialAuto_Page.ResumeLayout(false);
            this.PolinomialAuto_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.polinomialAutoChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PolinomialAuto_Chart_viewer)).EndInit();
            this.Degree_Page.ResumeLayout(false);
            this.Degree_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Degree_Chart_viewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.degreeChart)).EndInit();
            this.Giperbola_Page.ResumeLayout(false);
            this.Giperbola_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Giperbola_Chart_viewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.giperbolaChart)).EndInit();
            this.Square_Page.ResumeLayout(false);
            this.Square_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.squareChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Square_Chart_viewer)).EndInit();
            this.Polinomial_Page.ResumeLayout(false);
            this.Polinomial_Page.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Polinomial_Chart_viewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.polinomialChart)).EndInit();
            this.TimePage.ResumeLayout(false);
            this.TimePage.PerformLayout();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.Result_Summary.ResumeLayout(false);
            this.Result_Summary.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Result_Summary_DataGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl mainTab;
        private System.Windows.Forms.TabPage Data_Page;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage Linear_Page;
        private System.Windows.Forms.Button Calculate;
        private System.Windows.Forms.TabPage Degree_Page;
        private System.Windows.Forms.DataVisualization.Charting.Chart degreeChart;
        private System.Windows.Forms.TabPage Giperbola_Page;
        private System.Windows.Forms.DataVisualization.Charting.Chart giperbolaChart;
        private System.Windows.Forms.Button addColumn;
        private System.Windows.Forms.Button removeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn YData;
        private System.Windows.Forms.DataGridViewTextBoxColumn XData;
        private System.Windows.Forms.TabPage Result_Page;
        private System.Windows.Forms.Button Giperbola_Details;
        private System.Windows.Forms.Button Degree_Details;
        private System.Windows.Forms.Button Linear_Details;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Result_Giperbola_korrelation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Result_Degrree_korelation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Result_Linear_korrelation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Select_Model;
        private System.Windows.Forms.CheckBox Giperbola_CheckBox;
        private System.Windows.Forms.CheckBox Degree_CheckBox;
        private System.Windows.Forms.CheckBox Linear_CheckBox;
        private System.Windows.Forms.Button Open_file_button;
        private System.Windows.Forms.ComboBox comboExcelSheets;
        private System.Windows.Forms.RichTextBox Result_Degree;
        private System.Windows.Forms.RichTextBox Result_Linear;
        private System.Windows.Forms.RichTextBox Result_giperbola;
        private System.Windows.Forms.Button Auto_Calculate;
        private System.Windows.Forms.TabPage Auto_Result;
        private System.Windows.Forms.TextBox R_polinomial;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox R_exponential;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox R_linear;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RichTextBox Auto_polinomial_result;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Auto_polinomial_korrelation;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox Auto_exponential_result;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Auto_exponential_korrelation;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RichTextBox Auto_linear_result;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Auto_linear_korrelation;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox removeIndex;
        private System.Windows.Forms.TextBox setYTextBox;
        private System.Windows.Forms.Button setYButton;
        private System.Windows.Forms.RichTextBox Result_sqrt;
        private System.Windows.Forms.Button Sqrt_Details;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Result_Square_korrelation;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox Sqrt_CheckBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart linearChart;
        private System.Windows.Forms.ComboBox linear_X1_collection;
        private System.Windows.Forms.Button Linear_chart_Draw;
        private System.Windows.Forms.TabPage Exponential_Page;
        private System.Windows.Forms.Button Exponential_chart_Draw;
        private System.Windows.Forms.ComboBox exponential_X1_collection;
        private System.Windows.Forms.DataVisualization.Charting.Chart exponentialChart;
        private System.Windows.Forms.Button Linear_3Dchart_Draw;
        private System.Windows.Forms.ComboBox linear_X2_collection;
        private ChartDirector.WinChartViewer Linear_Chart_viewer;
        private System.Windows.Forms.ComboBox exponential_X2_collection;
        private System.Windows.Forms.Button Exponential_3Dchart_Draw;
        private ChartDirector.WinChartViewer Exponential_Chart_viewer;
        private System.Windows.Forms.Button polinomial_auto_details;
        private System.Windows.Forms.Button exponential_auto_details;
        private System.Windows.Forms.Button linear_auto_details;
        private System.Windows.Forms.TabPage PolinomialAuto_Page;
        private System.Windows.Forms.TabPage Square_Page;
        private System.Windows.Forms.TextBox linear_detail_R;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox linear_detail_korrelation;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button Linear_Levellnes_draw;
        private System.Windows.Forms.TextBox exponential_detail_R;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox exponential_detail_korrelation;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox polinomialAuto_detail_R;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox polinomialAuto_detail_korrelation;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox degree_detail_R;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox degree_detail_korrelation;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox giperbola_detail_R;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox giperbola_detail_korrelation;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox square_detail_R;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox square_detail_korrelation;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button Exponential_levellines_draw;
        private System.Windows.Forms.Button PolinomialAuto_levellines_draw;
        private System.Windows.Forms.Button PolinomialAuto_3Dchart_Draw;
        private System.Windows.Forms.ComboBox polinomialAuto_X2_collection;
        private System.Windows.Forms.Button PolinomialAuto_chart_Draw;
        private System.Windows.Forms.ComboBox polinomialAuto_X1_collection;
        private ChartDirector.WinChartViewer PolinomialAuto_Chart_viewer;
        private System.Windows.Forms.Button Degree_levellines_draw;
        private System.Windows.Forms.Button Degree_3Dchart_Draw;
        private System.Windows.Forms.ComboBox degree_X2_collection;
        private System.Windows.Forms.Button Degree_chart_Draw;
        private System.Windows.Forms.ComboBox degree_X1_collection;
        private ChartDirector.WinChartViewer Degree_Chart_viewer;
        private System.Windows.Forms.Button Giperbola_levellines_draw;
        private System.Windows.Forms.Button Giperbola_3Dchart_Draw;
        private System.Windows.Forms.ComboBox giperbola_X2_collection;
        private System.Windows.Forms.Button Giperbola_chart_Draw;
        private System.Windows.Forms.ComboBox giperbola_X1_collection;
        private ChartDirector.WinChartViewer Giperbola_Chart_viewer;
        private System.Windows.Forms.Button Square_levellines_draw;
        private System.Windows.Forms.Button Square_3Dchart_Draw;
        private System.Windows.Forms.ComboBox square_X2_collection;
        private System.Windows.Forms.Button Square_chart_Draw;
        private System.Windows.Forms.ComboBox square_X1_collection;
        private ChartDirector.WinChartViewer Square_Chart_viewer;
        private System.Windows.Forms.DataVisualization.Charting.Chart polinomialAutoChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart squareChart;
        private System.Windows.Forms.TextBox Auto_polinomial_Radj;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox Auto_exponential_Radj;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox Auto_linear_Radj;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button Linear_save;
        private System.Windows.Forms.Button Exponential_save;
        private System.Windows.Forms.Button PolinomialAuto_save;
        private System.Windows.Forms.RichTextBox Result_polinomial;
        private System.Windows.Forms.Button Polinomial_Details;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox Result_Polinomial_korrelation;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.RichTextBox Result_exponential;
        private System.Windows.Forms.Button Exponential_Details;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox Result_Exponential_korrelation;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.CheckBox Polinomial_CheckBox;
        private System.Windows.Forms.CheckBox Exponential_CheckBox;
        private System.Windows.Forms.ComboBox polinomial_range;
        private System.Windows.Forms.TabPage Polinomial_Page;
        private System.Windows.Forms.TextBox polinomial_detail_R;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox polinomial_detail_korrelation;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button Polinomial_levellines_draw;
        private System.Windows.Forms.Button Polinomial_3Dchart_Draw;
        private System.Windows.Forms.ComboBox polinomial_X2_collection;
        private System.Windows.Forms.Button Polinomial_chart_Draw;
        private System.Windows.Forms.ComboBox polinomial_X1_collection;
        private ChartDirector.WinChartViewer Polinomial_Chart_viewer;
        private System.Windows.Forms.DataVisualization.Charting.Chart polinomialChart;
        private System.Windows.Forms.TextBox Result_Exponential_Radj;
        private System.Windows.Forms.TextBox Result_Square_Radj;
        private System.Windows.Forms.TextBox Result_Giperbola_Radj;
        private System.Windows.Forms.TextBox Result_Degrree_Radj;
        private System.Windows.Forms.TextBox Result_Linear_Radj;
        private System.Windows.Forms.TextBox Result_Exponential_R;
        private System.Windows.Forms.TextBox Result_Square_R;
        private System.Windows.Forms.TextBox Result_Giperbola_R;
        private System.Windows.Forms.TextBox Result_Degrree_R;
        private System.Windows.Forms.TextBox Result_Linear_R;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox Result_Polinomial_Radj;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox Result_Polinomial_R;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Button Degree_save;
        private System.Windows.Forms.Button Giperbola_save;
        private System.Windows.Forms.Button Square_save;
        private System.Windows.Forms.Button Polinomial_save;
        private System.Windows.Forms.TextBox removeRowIndex;
        private System.Windows.Forms.Button removeRow;
        private System.Windows.Forms.TabPage TimePage;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox Square_time;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox Giperbola_time;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox Degree_time;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox Polinomial_Auto_time;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox Exponential_time;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox Linear_time;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox Polinomial_time;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox Total_Auto_time;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox Exponential_Auto_time;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox Linear_Auto_time;
        private System.Windows.Forms.RichTextBox linear_graph_function;
        private System.Windows.Forms.RichTextBox exponential_grahp_function;
        private System.Windows.Forms.RichTextBox polinomialAuto_grahp_function;
        private System.Windows.Forms.RichTextBox polinomial_grahp_function;
        private System.Windows.Forms.RichTextBox degree_grahp_function;
        private System.Windows.Forms.RichTextBox giperbola_grahp_function;
        private System.Windows.Forms.RichTextBox square_grahp_function;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem hmainMenu;
        private System.Windows.Forms.ToolStripMenuItem helpmainMenu;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.RadioButton Linear_avg_graph;
        private System.Windows.Forms.RadioButton Linear_min_graph;
        private System.Windows.Forms.RadioButton Linear_max_graph;
        private System.Windows.Forms.RadioButton Linear_standart_graph;
        private System.Windows.Forms.RadioButton Polinomial_standart_graph;
        private System.Windows.Forms.RadioButton Polinomial_max_graph;
        private System.Windows.Forms.RadioButton Polinomial_avg_graph;
        private System.Windows.Forms.RadioButton Polinomial_min_graph;
        private System.Windows.Forms.RadioButton Exponential_standart_graph;
        private System.Windows.Forms.RadioButton Exponential_max_graph;
        private System.Windows.Forms.RadioButton Exponential_avg_graph;
        private System.Windows.Forms.RadioButton Exponential_min_graph;
        private System.Windows.Forms.RadioButton Degree_standart_graph;
        private System.Windows.Forms.RadioButton Degree_max_graph;
        private System.Windows.Forms.RadioButton Degree_avg_graph;
        private System.Windows.Forms.RadioButton Degree_min_graph;
        private System.Windows.Forms.RadioButton Giperbola_standart_graph;
        private System.Windows.Forms.RadioButton Giperbola_max_graph;
        private System.Windows.Forms.RadioButton Giperbola_avg_graph;
        private System.Windows.Forms.RadioButton Giperbola_min_graph;
        private System.Windows.Forms.RadioButton Square_standart_graph;
        private System.Windows.Forms.RadioButton Square_max_graph;
        private System.Windows.Forms.RadioButton Square_avg_graph;
        private System.Windows.Forms.RadioButton Square_min_graph;
        private System.Windows.Forms.RadioButton PolinomialAuto_standart_graph;
        private System.Windows.Forms.RadioButton PolinomialAuto_max_graph;
        private System.Windows.Forms.RadioButton PolinomialAuto_avg_graph;
        private System.Windows.Forms.RadioButton PolinomialAuto_min_graph;
        private System.Windows.Forms.TextBox Linear_Y_max;
        private System.Windows.Forms.TextBox Linear_Y_min;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox Exponential_Y_min;
        private System.Windows.Forms.TextBox Exponential_Y_max;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox PolinomialAuto_Y_min;
        private System.Windows.Forms.TextBox PolinomialAuto_Y_max;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox Degree_Y_min;
        private System.Windows.Forms.TextBox Degree_Y_max;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox Giperbola_Y_min;
        private System.Windows.Forms.TextBox Giperbola_Y_max;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox Square_Y_min;
        private System.Windows.Forms.TextBox Square_Y_max;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox Polinomial_Y_min;
        private System.Windows.Forms.TextBox Polinomial_Y_max;
        private System.Windows.Forms.TabPage Result_Summary;
        private System.Windows.Forms.DataGridView Result_Summary_DataGrid;
        private System.Windows.Forms.ComboBox Result_Summary_Model_Select;
        private System.Windows.Forms.Button Result_Summary_Show;
        private System.Windows.Forms.TextBox Result_Summary_Sigma;
    }
}

