﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RegressionModelsTest
{
    class ChartData :  Data
    {
        public double[] X1;
        public double[] X2;
        private double[] X1temp;
        private double[] X2temp;
        private double[,] Xtemp;
        private double[] XtempAll;

        public ChartData()
        {

        }
        public void LinearChart(RegressionData model, Data data, int index1,int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[3, data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length*X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
                Xtemp[0,i] = data.X[0, i];
                Xtemp[1,i] = data.X[index1 + 1, i];
                Xtemp[2,i] = data.X[index2 + 1, i];
            }
            RegressionData linearTemp = RegressionModel.linearRegression(Xtemp,data.Y);
            result.Text = Result.linearRegressionResult(linearTemp.Koef);
            
            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }


            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = linearTemp.Koef[0] + linearTemp.Koef[1] * X1[j] + linearTemp.Koef[2] * X2[i];
                }
             }
        }

        public void LinearChartMin(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[1, data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }
            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if((i!= index1+1)&&(i!= index2+1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * XtempAll.Min();
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }


            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1+1] * X1[j] + model.Koef[index2+1] * X2[i] + sumOther;
                }
            }
        }

        public void LinearChartAverage(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[1, data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }
            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * XtempAll.Average();
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }


            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1 + 1] * X1[j] + model.Koef[index2 + 1] * X2[i] + sumOther;
                }
            }
        }

        public void LinearChartMax(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[1, data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }
            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * XtempAll.Max();
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }


            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1 + 1] * X1[j] + model.Koef[index2 + 1] * X2[i] + sumOther;
                }
            }
        }

        public void Linear2DChart(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            Xtemp = new double[2,data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
            }

            RegressionData linearTemp = RegressionModel.linearRegression(Xtemp, data.Y);
            result.Text = Result.linearRegressionResult(linearTemp.Koef);

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = linearTemp.Koef[0] + linearTemp.Koef[1] * k;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Linear2DChartMin(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * XtempAll.Min();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] + model.Koef[index1+1] * k + sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Linear2DChartAverage(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * XtempAll.Average();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] + model.Koef[index1 + 1] * k + sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Linear2DChartMax(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * XtempAll.Max();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] + model.Koef[index1 + 1] * k + sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void ExponentialChart(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[3, data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
                Xtemp[2, i] = data.X[index2 + 1, i];
            }
            RegressionData exponentialTemp = RegressionModel.exponentialRegression(Xtemp, data.Y);
            result.Text = Result.exponentialRegressionResult(exponentialTemp.Koef);

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = exponentialTemp.Koef[0] * Math.Exp(exponentialTemp.Koef[1] * X1[j] + exponentialTemp.Koef[2] * X2[i]);
                }
            }
        }

        public void ExponentialChartMin(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * XtempAll.Min();
                }
            }


            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] * Math.Exp(model.Koef[index1+1] * X1[j] + model.Koef[index2+1] * X2[i] + sumOther);
                }
            }
        }
        public void ExponentialChartAverage(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * XtempAll.Average();
                }
            }


            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] * Math.Exp(model.Koef[index1 + 1] * X1[j] + model.Koef[index2 + 1] * X2[i] + sumOther);
                }
            }
        }

        public void ExponentialChartMax(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * XtempAll.Max();
                }
            }


            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] * Math.Exp(model.Koef[index1 + 1] * X1[j] + model.Koef[index2 + 1] * X2[i] + sumOther);
                }
            }
        }

        public void Exponential2DChart(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            Xtemp = new double[2, data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
            }

            RegressionData exponentialTemp = RegressionModel.exponentialRegression(Xtemp, data.Y);
            result.Text = Result.exponentialRegressionResult(exponentialTemp.Koef);

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = exponentialTemp.Koef[0]*Math.Exp(exponentialTemp.Koef[1] * k);
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Exponential2DChartMin(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * XtempAll.Min();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] * Math.Exp(model.Koef[index1+1] * k + sumOther);
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Exponential2DChartAverage(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * XtempAll.Average();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] * Math.Exp(model.Koef[index1 + 1] * k + sumOther);
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Exponential2DChartMax(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * XtempAll.Max();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] * Math.Exp(model.Koef[index1 + 1] * k + sumOther);
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void PolinomialChart(RegressionData model, Data data, int index1, int index2, int n, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[3, data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
                Xtemp[2, i] = data.X[index2 + 1, i];
            }

            RegressionData polinomialTemp = RegressionModel.polimonialRegression(Xtemp, data.Y,n);
            result.Text = Result.polinomialRegressionResult(polinomialTemp.Koef,n);

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {
                    double sum1 = 0;
                    double sum2 = 0;
                    int k1 = 1;
                    int k2 = 1;

                 
                        for (int m =  1; m < 0 * n + n + 1; m++)
                        {
                            sum1 += polinomialTemp.Koef[m] * Math.Pow(X1[j],k1);
                            k1++;
                        }
                   

                        for (int m = n + 1; m < 1 * n + n + 1; m++)
                        {
                            sum2 += polinomialTemp.Koef[m] * Math.Pow(X2[i],k2);
                            k2++;
                        }

                    Y[i * X1.Length + j] = polinomialTemp.Koef[0] + sum1 +sum2;
                }
            }

           
        }

        public void PolinomialChartMin(RegressionData model, Data data, int index1, int index2, int n, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    int l =1;
                    for (int s = (i-1) * n+1; s < (i -1)* n + n+1; s++)
                    {

                        sumOther += model.Koef[s] * Math.Pow(XtempAll.Min(), l);
                        l++;
                    }
                }
            }


            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {
                    double sum1 = 0;
                    double sum2 = 0;
                    int k1 = 1;
                    int k2 = 1;


                    for (int m = index1*n +1; m < index1 * n + n + 1; m++)
                    {
                        sum1 += model.Koef[m] * Math.Pow(X1[j], k1);
                        k1++;
                    }


                    for (int m = index2*n + 1; m < index2 * n + n + 1; m++)
                    {
                        sum2 += model.Koef[m] * Math.Pow(X2[i], k2);
                        k2++;
                    }

                    Y[i * X1.Length + j] = model.Koef[0] + sum1 + sum2 +sumOther;
                }
            }
        }

        public void PolinomialChartAverage(RegressionData model, Data data, int index1, int index2, int n, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    int l = 1;
                    for (int s = (i - 1) * n + 1; s < (i - 1) * n + n + 1; s++)
                    {

                        sumOther += model.Koef[s] * Math.Pow(XtempAll.Average(), l);
                        l++;
                    }
                }
            }


            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {
                    double sum1 = 0;
                    double sum2 = 0;
                    int k1 = 1;
                    int k2 = 1;


                    for (int m = index1 * n + 1; m < index1 * n + n + 1; m++)
                    {
                        sum1 += model.Koef[m] * Math.Pow(X1[j], k1);
                        k1++;
                    }


                    for (int m = index2 * n + 1; m < index2 * n + n + 1; m++)
                    {
                        sum2 += model.Koef[m] * Math.Pow(X2[i], k2);
                        k2++;
                    }

                    Y[i * X1.Length + j] = model.Koef[0] + sum1 + sum2 + sumOther;
                }
            }
        }

        public void PolinomialChartMax(RegressionData model, Data data, int index1, int index2, int n, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    int l = 1;
                    for (int s = (i - 1) * n + 1; s < (i - 1) * n + n + 1; s++)
                    {

                        sumOther += model.Koef[s] * Math.Pow(XtempAll.Max(), l);
                        l++;
                    }
                }
            }


            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {
                    double sum1 = 0;
                    double sum2 = 0;
                    int k1 = 1;
                    int k2 = 1;


                    for (int m = index1 * n + 1; m < index1 * n + n + 1; m++)
                    {
                        sum1 += model.Koef[m] * Math.Pow(X1[j], k1);
                        k1++;
                    }


                    for (int m = index2 * n + 1; m < index2 * n + n + 1; m++)
                    {
                        sum2 += model.Koef[m] * Math.Pow(X2[i], k2);
                        k2++;
                    }

                    Y[i * X1.Length + j] = model.Koef[0] + sum1 + sum2 + sumOther;
                }
            }
        }

        public void Polinomial2DChart(RegressionData model, Data data, int index1, int n, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            Xtemp = new double[2, data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
            }

            RegressionData polinomialTemp = RegressionModel.polimonialRegression(Xtemp, data.Y, n);
            result.Text = Result.polinomialRegressionResult(polinomialTemp.Koef, n);

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                double sum1 = 0;
                int l=1;

                for (int m = 1; m < n + 1; m++)
                {
                    sum1 += polinomialTemp.Koef[m] * Math.Pow(k,l);
                    l++;
                }

                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
                Y[i] = polinomialTemp.Koef[0] + sum1;
            }
        }

        public void Polinomial2DChartMin(RegressionData model, Data data, int index1, int n, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int h = 0; h < data.Y.Length; h++)
                    {
                        XtempAll[h] = data.X[i, h];
                    }
                    int l = 1;
                    for (int s = (i - 1) * n + 1; s < (i - 1) * n + n + 1; s++)
                    {

                        sumOther += model.Koef[s] * Math.Pow(XtempAll.Min(), l);
                        l++;
                    }
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                double sum1 = 0;
                int l = 1;
                for (int m = index1*n+1; m < index1*n+n + 1; m++)
                {
                    sum1 += model.Koef[m] * Math.Pow(k, l);
                    l++;
                }
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
                Y[i] = model.Koef[0] + sum1 +sumOther;
            }
        }

        public void Polinomial2DChartAverage(RegressionData model, Data data, int index1, int n, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int h = 0; h < data.Y.Length; h++)
                    {
                        XtempAll[h] = data.X[i, h];
                    }
                    int l = 1;
                    for (int s = (i - 1) * n + 1; s < (i - 1) * n + n + 1; s++)
                    {

                        sumOther += model.Koef[s] * Math.Pow(XtempAll.Average(), l);
                        l++;
                    }
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                double sum1 = 0;
                int l = 1;
                for (int m = index1 * n + 1; m < index1 * n + n + 1; m++)
                {
                    sum1 += model.Koef[m] * Math.Pow(k, l);
                    l++;
                }
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
                Y[i] = model.Koef[0] + sum1 + sumOther;
            }
        }

        public void Polinomial2DChartMax(RegressionData model, Data data, int index1, int n, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            double sumOther = 0;
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int h = 0; h < data.Y.Length; h++)
                    {
                        XtempAll[h] = data.X[i, h];
                    }
                    int l = 1;
                    for (int s = (i - 1) * n + 1; s < (i - 1) * n + n + 1; s++)
                    {

                        sumOther += model.Koef[s] * Math.Pow(XtempAll.Max(), l);
                        l++;
                    }
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                double sum1 = 0;
                int l = 1;
                for (int m = index1 * n + 1; m < index1 * n + n + 1; m++)
                {
                    sum1 += model.Koef[m] * Math.Pow(k, l);
                    l++;
                }
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
                Y[i] = model.Koef[0] + sum1 + sumOther;
            }
        }

        public void DegreeChart(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[3, data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
                Xtemp[2, i] = data.X[index2 + 1, i];
            }

            RegressionData degreeTemp = RegressionModel.degreeRegression(Xtemp, data.Y);
            result.Text = Result.degreeRegressionResult(degreeTemp.Koef);

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = Math.Pow(10,degreeTemp.Koef[0]) * Math.Pow(X1[j], degreeTemp.Koef[1]) *  Math.Pow(X2[i],degreeTemp.Koef[2]);
                }
            }
        }

        public void DegreeChartMin(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 1;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther *= Math.Pow(XtempAll.Min(),model.Koef[i]);
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = Math.Pow(10, model.Koef[0]) * Math.Pow(X1[j], model.Koef[index1+1]) * Math.Pow(X2[i], model.Koef[index2+1])*sumOther;
                }
            }
        }

        public void DegreeChartAverage(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 1;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther *= Math.Pow(XtempAll.Average(), model.Koef[i]);
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = Math.Pow(10, model.Koef[0]) * Math.Pow(X1[j], model.Koef[index1 + 1]) * Math.Pow(X2[i], model.Koef[index2 + 1]) * sumOther;
                }
            }
        }

        public void DegreeChartMax(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 1;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther *= Math.Pow(XtempAll.Max(), model.Koef[i]);
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = Math.Pow(10, model.Koef[0]) * Math.Pow(X1[j], model.Koef[index1 + 1]) * Math.Pow(X2[i], model.Koef[index2 + 1]) * sumOther;
                }
            }
        }

        public void Degree2DChart(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            Xtemp = new double[2, data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
            }

            RegressionData degreeTemp = RegressionModel.degreeRegression(Xtemp, data.Y);
            result.Text = Result.degreeRegressionResult(degreeTemp.Koef);

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = Math.Pow(10,degreeTemp.Koef[0])* Math.Pow(k,degreeTemp.Koef[1]);
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Degree2DChartMin(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 1;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];

            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther *= Math.Pow(XtempAll.Min(), model.Koef[i]);
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = Math.Pow(10, model.Koef[0]) * Math.Pow(k, model.Koef[index1+1])*sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Degree2DChartAverage(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 1;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];

            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther *= Math.Pow(XtempAll.Average(), model.Koef[i]);
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = Math.Pow(10, model.Koef[0]) * Math.Pow(k, model.Koef[index1 + 1]) * sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Degree2DChartMax(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 1;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];

            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther *= Math.Pow(XtempAll.Max(), model.Koef[i]);
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = Math.Pow(10, model.Koef[0]) * Math.Pow(k, model.Koef[index1 + 1]) * sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void GiperbolaChart(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[3, data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
                Xtemp[2, i] = data.X[index2 + 1, i];
            }

            RegressionData giperbolaTemp = RegressionModel.giperbolaRegression(Xtemp, data.Y);
            result.Text = Result.giperbolaRegressionResult(giperbolaTemp.Koef);

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = giperbolaTemp.Koef[0] + giperbolaTemp.Koef[1]/X1[j] + giperbolaTemp.Koef[2]/X2[i];
                }
            }
        }

        public void GiperbolaChartMin(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i]/XtempAll.Min();
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1+1] / X1[j] + model.Koef[index2+1] / X2[i] +sumOther;
                }
            }
        }

        public void GiperbolaChartAverage(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] / XtempAll.Average();
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1 + 1] / X1[j] + model.Koef[index2 + 1] / X2[i] + sumOther;
                }
            }
        }

        public void GiperbolaChartMax(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] / XtempAll.Max();
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1 + 1] / X1[j] + model.Koef[index2 + 1] / X2[i] + sumOther;
                }
            }
        }

        public void Giperbola2DChart(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            Xtemp = new double[2, data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
            }

            RegressionData giperbolaTemp = RegressionModel.giperbolaRegression(Xtemp, data.Y);
            result.Text = Result.giperbolaRegressionResult(giperbolaTemp.Koef);

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = giperbolaTemp.Koef[0] + giperbolaTemp.Koef[1] / k;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Giperbola2DChartMin(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] / XtempAll.Min();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] + model.Koef[index1 +1] / k + sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Giperbola2DChartAverage(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] / XtempAll.Average();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] + model.Koef[index1 + 1] / k + sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Giperbola2DChartMax(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] / XtempAll.Max();
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {

                Y[i] = model.Koef[0] + model.Koef[index1 + 1] / k + sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void SquareChart(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            Xtemp = new double[3, data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
                Xtemp[2, i] = data.X[index2 + 1, i];
            }

            RegressionData squareTemp = RegressionModel.sqrtRegression(Xtemp, data.Y);
            result.Text = Result.squareRegressionResult(squareTemp.Koef);

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = squareTemp.Koef[0] + squareTemp.Koef[1] *Math.Sqrt(X1[j]) + squareTemp.Koef[2]*Math.Sqrt(X2[i]);
                }
            }
        }

        public void SquareChartMin(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] *Math.Sqrt(XtempAll.Min());
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1+1] * Math.Sqrt(X1[j]) + model.Koef[index2+1] * Math.Sqrt(X2[i]) +sumOther;
                }
            }
        }

        public void SquareChartAverage(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * Math.Sqrt(XtempAll.Average());
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1 + 1] * Math.Sqrt(X1[j]) + model.Koef[index2 + 1] * Math.Sqrt(X2[i]) + sumOther;
                }
            }
        }

        public void SquareChartMax(RegressionData model, Data data, int index1, int index2, RichTextBox result)
        {
            X1temp = new double[data.Y.Length];
            X2temp = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X1 = new double[50];
            X2 = new double[50];
            Y = new double[X1.Length * X2.Length];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1temp[i] = data.X[index1 + 1, i];
                X2temp[i] = data.X[index2 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if ((i != index1 + 1) && (i != index2 + 1))
                {
                    for (int k = 0; k < data.Y.Length; k++)
                    {
                        XtempAll[k] = data.X[i, k];
                    }
                    sumOther += model.Koef[i] * Math.Sqrt(XtempAll.Max());
                }
            }

            double t1 = X1temp.Min();
            double t2 = X2temp.Min();
            for (int i = 0; i < X1.Length; i++)
            {
                X1[i] = t1;
                X2[i] = t2;
                t1 += (X1temp.Max() - X1temp.Min()) / X1.Length;
                t2 += (X2temp.Max() - X2temp.Min()) / X2.Length;
            }

            for (int i = 0; i < X1.Length; i++)
            {
                for (int j = 0; j < X2.Length; j++)
                {

                    Y[i * X1.Length + j] = model.Koef[0] + model.Koef[index1 + 1] * Math.Sqrt(X1[j]) + model.Koef[index2 + 1] * Math.Sqrt(X2[i]) + sumOther;
                }
            }
        }

        public void Square2DChart(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            Xtemp = new double[2, data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
                Xtemp[0, i] = data.X[0, i];
                Xtemp[1, i] = data.X[index1 + 1, i];
            }

            RegressionData squareTemp = RegressionModel.sqrtRegression(Xtemp, data.Y);
            result.Text = Result.squareRegressionResult(squareTemp.Koef);

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                Y[i] = squareTemp.Koef[0] + squareTemp.Koef[1]* Math.Sqrt(k);
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }
        public void Square2DChartMin(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * Math.Sqrt(XtempAll.Min());
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                Y[i] = model.Koef[0] + model.Koef[index1+1] * Math.Sqrt(k) +sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }
        public void Square2DChartAverage(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * Math.Sqrt(XtempAll.Average());
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                Y[i] = model.Koef[0] + model.Koef[index1 + 1] * Math.Sqrt(k)+ sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

        public void Square2DChartMax(RegressionData model, Data data, int index1, RichTextBox result)
        {
            X1 = new double[data.Y.Length];
            XtempAll = new double[data.Y.Length];
            X2 = new double[100];
            Y = new double[100];
            double sumOther = 0;
            for (int i = 0; i < model.Y.Length; i++)
            {
                X1[i] = data.X[index1 + 1, i];
            }

            for (int i = 1; i < data.X.Length / data.Y.Length; i++)
            {
                if (i != index1 + 1)
                {
                    for (int l = 0; l < data.Y.Length; l++)
                    {
                        XtempAll[l] = data.X[i, l];
                    }
                    sumOther += model.Koef[i] * Math.Sqrt(XtempAll.Max());
                }
            }

            double k = X1.Min();
            for (int i = 0; i < 100; i++)
            {
                Y[i] = model.Koef[0] + model.Koef[index1 + 1] * Math.Sqrt(k)+ sumOther;
                X2[i] = k;
                k += (X1.Max() - X1.Min()) / 100;
            }
        }

    }
}
