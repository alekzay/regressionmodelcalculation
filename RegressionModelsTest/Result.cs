﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RegressionModelsTest
{
    class Result
    {
       static public string linearRegressionResult(double[] Koef)
        {
            string result = Convert.ToString(Koef[0]);
            for (int i = 1; i < Koef.Length; i++)
            {
                if (Koef[i] > 0)
                {
                    result += " + " + Convert.ToString(Koef[i]) + "*x" + Convert.ToString(i);
                }
                else
                {
                    result += " " + Convert.ToString(Koef[i]) + "*x" + Convert.ToString(i);
                }
            } 
            return result;
        }

        static public string degreeRegressionResult(double[] Koef)
        {
            string result = Convert.ToString(Math.Pow(10,Koef[0]));
            for (int i = 1; i < Koef.Length; i++)
            {

                result += " *x" + Convert.ToString(i) + "^(" + Convert.ToString(Koef[i]) + ")";

            }
            return result;
        }

        static public string giperbolaRegressionResult(double[] Koef)
        {
            string result = Convert.ToString(Koef[0]);
            for (int i = 1; i < Koef.Length; i++)
            {
                if (Koef[i] > 0)
                {
                    result += " + " + Convert.ToString(Koef[i]) + "/x" + Convert.ToString(i);
                }
                else
                {
                    result += " " + Convert.ToString(Koef[i]) + "/x" + Convert.ToString(i);
                }
            }
            return result;
        }

        static public string exponentialRegressionResult(double[] Koef)
        {
            string result = "";
            for (int i = 1; i < Koef.Length; i++)
            {
                if (Koef[i] > 0)
                {
                    result += " + " + Convert.ToString(Koef[i]) + "*x" + Convert.ToString(i);
                }
                else
                {
                    result += " " + Convert.ToString(Koef[i]) + "*x" + Convert.ToString(i);
                }
            }
            result = Convert.ToString(Math.Exp(Koef[0])) + "*e^(" + result + ")";
            return result;
        }

        static public string squareRegressionResult(double[] Koef)
        {
            string result = Convert.ToString(Koef[0]);
            for (int i = 1; i < Koef.Length; i++)
            {
                if (Koef[i] > 0)
                {
                    result += " + " + Convert.ToString(Koef[i]) + "*sqrt(x" + Convert.ToString(i) + ")";
                }
                else
                {
                    result += " " + Convert.ToString(Koef[i]) + "*sqrt(x" + Convert.ToString(i) + ")";
                }
            }
            return result;
        }

        static public string polinomialRegressionResult(double[] Koef, int n)
        {
            string result = Koef[0].ToString();
            for (int i = 0; i < (Koef.Length - 1) / n; i++)
            {
                int k = 1;
                for (int j = i * n + 1; j < i * n + n + 1; j++)
                {
                    if (Koef[j] > 0)
                    {
                        result += " + " + Convert.ToString(Koef[j]) + "*x" + Convert.ToString(i + 1) + "^" + Convert.ToString(k);
                    }
                    else
                    {
                        result += " " + Convert.ToString(Koef[j]) + "*x" + Convert.ToString(i + 1) + "^" + Convert.ToString(k);
                    }
                    k++;
                }
            }
            return result;

        }

        static public void saveresult(string result)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            if (savefile.ShowDialog() == DialogResult.Cancel)
                return;

            string filename = savefile.FileName;
            System.IO.File.WriteAllText(filename, "y= " + result);
            MessageBox.Show("Файл сохранен");
        }
    }
}
