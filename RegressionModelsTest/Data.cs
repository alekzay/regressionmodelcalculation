﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.ComponentModel;
using System.Drawing;

namespace RegressionModelsTest
{
    class Data
    {
        public double[] Y{set;get;}
        public double[,] X;
        public double XColumns;
        public double XYRows;
        bool dataValidation = true; 

        public Data()
        {
        }
        public void setDataFromForm(DataGridView data)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            //data.Sort(data.Columns[0], ListSortDirection.Ascending);
            Y = new double[data.RowCount - 1];
            X = new double[data.ColumnCount , data.RowCount - 1];
            double k;
            for (int i = 0; i < data.RowCount - 1; i++)
            {
                X[0, i] = 1;
            }
            for (int j = 1; j < data.ColumnCount; j++)
                {
                    for (int i = 0; i < data.RowCount - 1; i++)
                    {
                        if (data[j, i].Value == null)
                        {
                            MessageBox.Show("null");
                            data[j, i].Style.BackColor = Color.Red;
                        }
                        else
                        {
                           // X[j, i] = Validate(data[j, i].Value.ToString());
                            if (Double.TryParse(data[j, i].Value.ToString(), out k) == true)
                            {
                                X[j, i] = k;
                            }
                            else
                            {
                                data[j, i].Style.BackColor = Color.Red;
                            }
                        }
                        //MessageBox.Show("I=" + i + " v =" + data[0, i].Value.ToString());
                    }
                }
            for (int i = 0; i < data.RowCount - 1; i++)
            {
                if (data[0, i].Value == null)
                {
                    MessageBox.Show("null");
                    data[0, i].Style.BackColor = Color.Red;
                }
                else
                {
                    //Y[i] = Validate(data[0, i].Value.ToString());
                    if (Double.TryParse(data[0, i].Value.ToString(), out k) == true)
                    {
                        Y[i] = k;
                    }
                    else
                    {
                        data[0, i].Style.BackColor = Color.Red;
                    }
                }
            }
            XColumns = data.ColumnCount - 1;
            XYRows = data.RowCount - 1;
        }
        public void setDataFromForm(string[] Y, string[] X1,string[] X2)
        {
            for (int i = 0; i <= Y.Length; i++)
            {
                this.Y[i] = Validate(Y[i]);
            }
        }

        public double Validate(string param)
        {
            double p;
            bool k = Double.TryParse(param, out p);
            if ((k) && (p >= 0))
            {
                return p;
            }
            else
            {
                return -1;
            };
        }
    }
}
