﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace RegressionModelsTest
{
    class RegressionModel
    {

        static public RegressionData baseRegression(double[,] X, double[] Y)
        {
            RegressionData data = new RegressionData(X, Y);
            double[] YRegressionData = new double[Y.Length];
            Matrix<double> Xvalue = DenseMatrix.OfArray(X);
            Vector<double> Yvalue = DenseVector.OfArray(Y);
            Matrix<double> XvalueTransparent;
            Matrix<double> result1;
            Vector<double> result2;
            Vector<double> result;
            XvalueTransparent = Xvalue.Transpose();
            result1 = Xvalue * XvalueTransparent;
            result2 = Xvalue * Yvalue;
            result = result1.Inverse() * result2;
            data.Y = YRegressionData;
            data.Koef = result.ToArray();
            return data;
        }

       static public RegressionData linearRegression(double[,] X, double[] Y)
       {
           RegressionData data = new RegressionData(X,Y);

           data = baseRegression(X,Y);
           for (int i = 0; i < data.Y.Length; i++)
           {
               data.Y[i] = data.Koef[0];
               for (int j = 1; j < data.Koef.Length; j++)
               {
                   data.Y[i] += X[j, i] * data.Koef[j];
               }
           }
           return data;
       }

       static public RegressionData degreeRegression(double[,] X, double[] Y)
       {
           RegressionData data = new RegressionData(X,Y);
           double[,] XData = new double[X.Length / Y.Length, Y.Length];
           double[] YData = new double[Y.Length];
           data.errorcount = 0;
           for (int j = 0; j < Y.Length; j++)
           {
               YData[j] = Math.Log10(Y[j]);
               XData[0, j] = X[0, j];
               for (int i = 1; i < X.Length/Y.Length; i++)
               {
                   XData[i,j] = Math.Log10(X[i,j]);
                   if (XData[i, j] == double.NaN && double.IsInfinity(XData[i, j]))
                   {
                       data.errorcount++;
                       XData[i, j] = 0;
                   }
               }
               if (YData[j] == double.NaN && double.IsInfinity(YData[j]))
               {
                   data.errorcount++;
                   YData[j] = 0;
               }

           }

           data = baseRegression(XData,YData);

           for (int i = 0; i < data.Y.Length; i++)
           {
               data.Y[i] = Math.Pow(10,data.Koef[0]);
               for (int j = 1; j < data.Koef.Length; j++)
               {
                   data.Y[i] *= Math.Pow(X[j,i],data.Koef[j]);
               }
           }
           return data;
       }


       static public RegressionData giperbolaRegression(double[,] X, double[] Y)
       {
           RegressionData data = new RegressionData(X, Y);
           data.errorcount = 0;
           double[,] XData = new double[X.Length/Y.Length, Y.Length];
           double[] YData = new double[Y.Length];
           for (int j = 0; j < Y.Length; j++)
           {
               XData[0, j] = X[0, j];
               YData[j] = Y[j];
               for (int i = 1; i < X.Length / Y.Length; i++)
               {
                   XData[i, j] = 1 / X[i, j];
                   if (XData[i, j] == double.NaN && double.IsInfinity(XData[i,j]))
                   {
                       data.errorcount++;
                       XData[i, j] = 0;
                   }
               }
           }

           data = baseRegression(XData, YData);

           for (int i = 0; i < data.Y.Length; i++)
           {
               data.Y[i] = data.Koef[0];
               for (int j = 1; j < data.Koef.Length; j++)
               {
                   data.Y[i] += data.Koef[j] * (XData[j, i]);
               }
           }
           return data;
       }

       static public RegressionData exponentialRegression(double[,] X, double[] Y)
       {
           RegressionData data = new RegressionData(X, Y);
           double[,] XData = new double[X.Length / Y.Length, Y.Length];
           double[] YData = new double[Y.Length];
           data.errorcount = 0;
           for (int j = 0; j < Y.Length; j++)
           {
               YData[j] = Math.Log(Y[j]);
               if (YData[j] == double.NaN && double.IsInfinity(YData[j]))
               {
                   data.errorcount++;
                   YData[j] = 0;
               }
           }

           data = baseRegression(X, YData);

           for (int i = 0; i < data.Y.Length; i++)
           {
               double sum = 0;
               for (int j = 1; j < data.Koef.Length; j++)
               {
                   sum += X[j,i]*data.Koef[j];
               }
               data.Y[i] = Math.Exp(data.Koef[0]) * Math.Exp(sum);
           }
           data.Koef[0] = Math.Exp(data.Koef[0]);
           return data;
       }

       static public RegressionData sqrtRegression(double[,] X, double[] Y)
       {
           RegressionData data = new RegressionData(X, Y);
           double[,] XData = new double[X.Length / Y.Length, Y.Length];
           double[] YData = new double[Y.Length];
           for (int j = 0; j < Y.Length; j++)
           {
               XData[0, j] = X[0, j];
               YData[j] = Y[j];
               for (int i = 1; i < X.Length / Y.Length; i++)
               {
                   XData[i, j] = Math.Sqrt(X[i, j]);
                   if (XData[i, j] == double.NaN && double.IsInfinity(XData[i,j]))
                   {
                       data.errorcount++;
                       XData[i, j] = 0;
                   }
               }
           }

           data = baseRegression(XData, YData);

           for (int i = 0; i < data.Y.Length; i++)
           {
               data.Y[i] = data.Koef[0];
               for (int j = 1; j < data.Koef.Length; j++)
               {
                   data.Y[i] += data.Koef[j] * (XData[j, i]);
               }
           }
           return data;

       }

       static public RegressionData polinomialAutoRegression(double[,] X, double[] Y)
       {
           RegressionData data = new RegressionData(X, Y);
           double[,] XData = new double[(X.Length / Y.Length)*3-2, Y.Length];
           double[] YData = new double[Y.Length];
           int k = 1;
           for (int j = 0; j < Y.Length; j++)
           {
               XData[0, j] = X[0, j];
               YData[j] = Y[j];
           }
           for (int i = 1; i < X.Length / Y.Length; i++)
           {
               for (int j = 0; j < Y.Length; j++)
               {
                   XData[k, j] = X[i, j];
                   XData[k + 1, j] = Math.Pow(X[i, j], 2);
                   XData[k + 2, j] = Math.Pow(X[i, j], 3);
               }
               k += 3;
           }

           data = baseRegression(XData, YData);
           int s = 1;
           for (int i = 0; i < data.Y.Length; i++)
           {
               data.Y[i] = data.Koef[0];

           }

           for (int i = 1; i < X.Length / Y.Length; i++)
           {
               for (int j = 0; j < Y.Length ; j++)
               {
                   data.Y[j] += data.Koef[s] * (X[i, j]) + data.Koef[s + 1] * Math.Pow((X[i, j]), 2) + data.Koef[s + 2] * Math.Pow((X[i, j]), 3); 
               }
               s += 3;
           }
               return data;

       }

       static public RegressionData polimonialRegression(double[,] X, double[] Y, int n)
       {
           RegressionData data = new RegressionData(X, Y);
           double[,] XData = new double[((X.Length / Y.Length) - 1)*n+1, Y.Length];
           double[] YData = new double[Y.Length];
           int k = 1;
           for (int j = 0; j < Y.Length; j++)
           {
               XData[0, j] = X[0, j];
               YData[j] = Y[j];
           }
           for (int i = 1; i < ((X.Length / Y.Length) - 1) * n+1; i += n)
           {
               for (int j = 0; j < Y.Length; j++)
               {
                   for (int l = 0; l <n; l++)
                   {
                       XData[i+l, j] = Math.Pow(X[k, j], l+1);

                   }
               }
               k++;
           }

           data = baseRegression(XData, YData);
           for (int i = 0; i < data.Y.Length; i++)
           {
               data.Y[i] = data.Koef[0];

           }
           k = 1;
           for (int i = 1; i < ((X.Length / Y.Length) - 1) * n + 1; i+=n)
           {
               for (int j = 0; j < Y.Length; j++)
               {
                   for (int l = 0; l < n; l++)
                   {
                       data.Y[j] += data.Koef[i+l]*Math.Pow(X[k, j], l + 1);
                   }
               }
               k++;
           }
           return data;
       }
    }
}
